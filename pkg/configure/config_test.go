package configure

import (
	"os"
	"testing"

	"github.com/BurntSushi/toml"
	"github.com/stretchr/testify/assert"
)

var fullOptions, prepOptions Options

func TestNewConfig(t *testing.T) {
	t.Run("Complete/valid auth config file", func(t *testing.T) {
		cfg, err := NewConfig("../../test/testdata/configs/valid_auth.toml")
		assert.Nil(t, err)
		assert.Equal(t, fullOptions, cfg.Options())
		assert.Equal(t, fullOptions.Auth, cfg.Auth())
		assert.Equal(t, fullOptions.General, cfg.General())
		assert.Equal(t, fullOptions.Batch, cfg.Batch())

		contents, err := cfg.PrepareState(true)
		assert.NoError(t, err, "PrepareState()")
		assert.NotNil(t, contents, "PrepareState()")

		_ = os.Setenv(EnvVariable, contents)
	})

	t.Run("Complete/valid auth config contents", func(t *testing.T) {
		got, err := NewConfig(EnvVariable)
		assert.NoError(t, err)
		assert.Equal(t, prepOptions, got.Options())
	})

	t.Run("Missing configuration file", func(t *testing.T) {
		_, err := NewConfig("/not/a/valid/file.toml")
		assert.Error(t, err)
	})

	t.Run("Invalid configuration", func(t *testing.T) {
		_ = os.Setenv(EnvVariable, "[]")
		_, err := NewConfig(EnvVariable)
		assert.Error(t, err)
	})
}

func Test_loadContents(t *testing.T) {
	type args struct {
		contents string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name:    "No contents",
			args:    args{contents: ""},
			wantErr: true,
		}, {
			name: "Invalid TOML",
			args: args{
				contents: `[general]
name = "Invalid"
executor = 42
`,
			},
			wantErr: true,
		}, {
			name: "Valid configuration",
			args: args{
				contents: `[general]
name = "Valid"
executor = "batch"

[batch]
arguments_variable = ["SCHEDULER_PARAMETERS"]
`,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := loadContents(tt.args.contents)
			assert.Equal(t, tt.wantErr, err != nil)
		})
	}
}

func Test_encode(t *testing.T) {
	opt := Options{
		General: General{
			Name:     "Valid",
			Executor: "batch",
		},
		Batch: Batch{
			ArgumentsVariable: []string{"PARAMETER"},
		},
	}
	t.Run("Encode valid options", func(t *testing.T) {
		_, err := encode(opt)
		assert.Nil(t, err)
	})
}

func TestConfig_PrepareNotification(t *testing.T) {
	tests := map[string]struct {
		opt       Options
		scriptDir string
		want      string
	}{
		"minimal config notification": {
			opt: Options{
				General: General{
					Executor: "shell",
				},
			},
			want: `Targeting shell executor
`,
		},
		"all cases for config encountered": {
			opt: Options{
				General: General{
					Executor:        "slurm",
					CustomBuildDir:  true,
					RetainLogs:      true,
					StaticMinDays:   7,
					StaticBuildsDir: true,
				},
				Auth: Auth{
					RunAs: RunAs{
						RunAsVariable: "EXAMPLE_RUN_AS",
					},
				},
			},
			scriptDir: "/data/dir/user/scripts",
			want: `Targeting slurm executor
Custom builds directory enabled (set with CUSTOM_CI_BUILDS_DIR variable)
Job and scheduler logs retained automatically (/data/dir/user/scripts)
RunAs service account process enabled (propose with EXAMPLE_RUN_AS variable)
Static builds directory in use, all jobs results kept. Please be aware of large disk space requirements when using this runner (minimum days: 7)
`,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			c := Config{
				opt: tt.opt,
			}
			got := c.PrepareNotification(tt.scriptDir)
			assert.Equal(t, tt.want, got)
		})
	}
}

func init() {
	fullOptions = Options{
		General: General{
			Name:          "Complete config",
			Executor:      "batch",
			RetainLogs:    true,
			DataDir:       "/ecp",
			KillTimeout:   "120s",
			TLSCAFile:     "/some/file.crt",
			StaticMinDays: defStaticMinDays,
		},
		Auth: Auth{
			Downscope:          "setuid",
			MaxEnvChars:        10000,
			UserAllowlist:      []string{"u1"},
			UserBlocklist:      []string{"g1", "g2"},
			GroupAllowlist:     []string{"u2"},
			GroupBlocklist:     []string{"g3"},
			ShellAllowlist:     []string{"/bin/bash"},
			ListsPreValidation: true,
			RootDirCreation:    true,
			JWTExpDelay:        "15m",
			RunAs: RunAs{
				ValidationScript: "valid.bash",
				RunAsVariable:    "RUNAS",
			},
			Logging: Logging{
				Enabled:  true,
				Level:    "debug",
				Location: "syslog",
			},
		},
		Batch: Batch{
			ArgumentsVariable: []string{"FOO", "BAR"},
			CommandDelay:      "30s",
			NFSTimeout:        "1m",
		},
	}

	prepOptions = Options{
		General: fullOptions.General,
		Batch:   fullOptions.Batch,
		Auth:    Auth{},
	}
}

func TestMetaOptions(t *testing.T) {
	tests := map[string]struct {
		file        string
		assertMeta  func(*testing.T, toml.MetaData)
		assertOpt   func(*testing.T, Options)
		assertError func(*testing.T, error)
	}{
		"missing file": {
			file: t.TempDir() + "/missing.toml",
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"valid config": {
			file: "../../test/testdata/configs/valid_auth.toml",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertOpt: func(t *testing.T, options Options) {
				assert.Equal(t, fullOptions, options)
			},
			assertMeta: func(t *testing.T, meta toml.MetaData) {
				assert.Equal(t, true, meta.IsDefined("general"))
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			meta, opt, err := MetaOptions(tt.file)

			if tt.assertMeta != nil {
				tt.assertMeta(t, meta)
			}
			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertOpt != nil {
				tt.assertOpt(t, opt)
			}
		})
	}
}
