#!/usr/bin/env bash

# Install appropriate runner version to the container

set -eo pipefail
set +o noclobber

echo "Create local user account..."

useradd -ms /bin/bash ${NIGHTLY_TEST_BOT_USER}
mkdir /ecp
chmod 777 /ecp

echo "Run verification on runner version..."

rpm --import /certs/runner-gitlab-runner-4C80FB51394521E9.pub.gpg
rpm -K gitlab_runner.rpm

echo "Verification OK"
echo "Installing runner version..."

cd /etc/gitlab-runner

rpm -i gitlab_runner.rpm

echo "Runner installed."
echo "Checking runner version..."

gitlab-runner --version

echo "Starting runner..."
echo "Press Ctrl+C to exit."

gitlab-runner run &> /etc/gitlab-runner/runner.log
