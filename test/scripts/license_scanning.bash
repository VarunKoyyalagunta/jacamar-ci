#!/bin/bash

# Override default behaviors of the /run.sh script found in
# security-products/analyzers/license-finder container image.

. /opt/gitlab/.bashrc || true

export GO111MODULE=on
export GOPATH=/opt/gitlab/.local

set -eo pipefail -x

# We currently license both cap and psx under the BSD clause three.
# This script allows us to bypass licenses management for the currently
# reviewed versions since they are not correctly identified.

cap_pkg="kernel.org/pub/linux/libs/security/libcap/cap"
egrep -o "${cap_pkg}" go.mod && license_management licenses add ${cap_pkg} BSD-3-Clause

psx_pkg="kernel.org/pub/linux/libs/security/libcap/psx"
egrep -o "${psx_pkg}" go.mod && license_management licenses add ${psx_pkg} BSD-3-Clause

# Unknown licenses

# https://pkg.go.dev/github.com/stretchr/objx?tab=licenses
objx_pkg="github.com/stretchr/objx"
egrep -o "${objx_pkg}" go.sum && license_management licenses add ${objx_pkg} MIT

# https://pkg.go.dev/golang.org/x/net?tab=licenses
net_pkg="golang.org/x/net"
egrep -o "${net_pkg}" go.sum && license_management licenses add ${net_pkg} BSD-3-Clause

# https://pkg.go.dev/golang.org/x/sync?tab=licenses
sync_pkg="golang.org/x/sync"
egrep -o "${sync_pkg}" go.sum && license_management licenses add ${sync_pkg} BSD-3-Clause

# https://pkg.go.dev/golang.org/x/tools?tab=licenses
tools_pkg="golang.org/x/tools"
egrep -o "${tools_pkg}" go.sum && license_management licenses add ${tools_pkg} BSD-3-Clause

# https://pkg.go.dev/golang.org/x/term?tab=licenses
terms_pkg="golang.org/x/term"
egrep -o "${terms_pkg}" go.sum && license_management licenses add ${terms_pkg} BSD-3-Clause

# https://pkg.go.dev/rsc.io/sampler?tab=licenses
sampler_pkg="rsc.io/sampler"
egrep -o "${sampler_pkg}" go.sum && license_management licenses add ${sampler_pkg} BSD-3-Clause

# https://pkg.go.dev/rsc.io/quote/v3?tab=licenses
quote_pkg="rsc.io/quote/v3"
egrep -o "${quote_pkg}" go.sum && license_management licenses add ${quote_pkg} BSD-3-Clause

license_management report --prepare-no-fail \
  --format=json --save=report.json \
  --no-recursive --no-debug
