_cfg-env:
  variables:
    configs: $CI_PROJECT_DIR/test/pavilion/share/configs
    scripts: $CI_PROJECT_DIR/test/pavilion/share/scripts
    plugins: $CI_PROJECT_DIR/test/pavilion/share/go-plugins
    responses: $CI_PROJECT_DIR/test/pavilion/share/responses
    jacamar: /opt/jacamar/bin/jacamar
  run:
    timeout: 60
    env:
      CUSTOM_ENV_CI_CONCURRENT_ID: 0
      CUSTOM_ENV_CI_JOB_ID: 123
      CUSTOM_ENV_CI_JOB_TOKEN: abc123efg456hij789kl
      CUSTOM_ENV_CI_RUNNER_SHORT_TOKEN: token123
      CUSTOM_ENV_CI_SERVER_URL: http://127.0.0.1:5000
      CUSTOM_ENV_CI_JOB_JWT: '{{jwt(["user_login=fluxuser"])}}'
      CUSTOM_ENV_CI_RUNNER_VERSION: '14.5.0-rc1'
      SYSTEM_FAILURE_EXIT_CODE: 2
      BUILD_FAILURE_EXIT_CODE: 1
      JOB_RESPONSE_FILE: '{{responses}}/valid_default.json'
      JACAMAR_CI_RUNNER_TIMEOUT: '0h3m0s'

_job-home-env:
  inherits_from: _cfg-env
  run:
    env:
      JACAMAR_CI_BASE_DIR: /home/fluxuser/.jacamar-ci
      JACAMAR_CI_BUILDS_DIR: /home/fluxuser/.jacamar-ci/builds
      JACAMAR_CI_CACHE_DIR: /home/fluxuser/.jacamar-ci/cache
      JACAMAR_CI_SCRIPT_DIR: /home/fluxuser/.jacamar-ci/scripts/group/project/123
      JACAMAR_CI_AUTH_USERNAME: fluxuser
      JACAMAR_CI_PROJECT_PATH: group/project
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/general/home_flux.toml")}}'

_job-dir-env:
  inherits_from: _cfg-env
  run:
    env:
      JACAMAR_CI_BASE_DIR: /var/tmp/fluxuser
      JACAMAR_CI_BUILDS_DIR: /var/tmp/fluxuser/builds
      JACAMAR_CI_CACHE_DIR: /var/tmp/fluxuser/cache
      JACAMAR_CI_SCRIPT_DIR: /var/tmp/fluxuser/scripts/group/project/123
      JACAMAR_CI_AUTH_USERNAME: fluxuser
      JACAMAR_CI_PROJECT_PATH: group/project
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/general/custom_flux.toml")}}'

###############################################################################
# general tests
###############################################################################

start-service:
  inherits_from: _cfg-env
  summary: Verify basic Flux deployment information.
  run:
    cmds:
      - "flux --version"
      - "env | grep FLUX_URI"
  result_evaluate:
    result: 'return_value == 0'

jacamar-version:
  inherits_from: _cfg-env
  summary: Verify jacamar application version.
  run:
    cmds:
      - "{{jacamar}} --version"
      - "{{jacamar}} --help"
  result_evaluate:
    result: 'return_value == 0'

###############################################################################
# prepare_exec tests
###############################################################################

prepare-home-dir:
  inherits_from: _job-home-env
  summary: User's home directory prepared for job.
  run:
    cmds:
      - '{{jacamar}} --no-auth prepare'
      - 'namei -l $JACAMAR_CI_BASE_DIR'
      - 'namei -l $JACAMAR_CI_BUILDS_DIR'
      - 'namei -l $JACAMAR_CI_CACHE_DIR'
      - 'namei -l $JACAMAR_CI_SCRIPT_DIR'
      # We don't need to confirm permissions, only the folder existence as we
      # rely on the $HOME directory permissions.

prepare-data-dir:
  inherits_from: _job-dir-env
  summary: Admin defined data directory prepared for job.
  run:
    cmds:
      - '{{jacamar}} --no-auth prepare'
      - 'namei -l $JACAMAR_CI_BASE_DIR'
      - 'namei -l $JACAMAR_CI_BUILDS_DIR'
      - 'namei -l $JACAMAR_CI_CACHE_DIR'
      - 'namei -l $JACAMAR_CI_SCRIPT_DIR'
  result_parse:
    regex:
      base_dir:
        regex: 'drwx------ fluxuser fluxuser fluxuser'
        action: store_true
  result_evaluate:
    result: 'base_dir and return_value == 0'

###############################################################################
# run_exec tests
###############################################################################

run-default-invalid-submission:
  inherits_from: _job-home-env
  summary: Invalid schedule parameters.
  run:
    env:
      CUSTOM_ENV_SCHEDULER_PARAMETERS: "--invalid"
    cmds:
      - 'cp {{scripts}}/jobs/job-env.bash /tmp/invalid-param.bash'
      - '{{jacamar}} --no-auth run /tmp/invalid-param.bash step_script'
  result_parse:
    regex:
      flux_option:
        regex: 'flux-mini: error: unrecognized arguments'
        action: store_true
  result_evaluate:
    result: 'flux_option and return_value == 1'

run-valid-short-submission:
  inherits_from: _job-home-env
  summary: Execute short run successful job.
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 456
      JACAMAR_CI_SCRIPT_DIR: /home/fluxuser/.jacamar-ci/scripts/group/project/456
      CUSTOM_ENV_SCHEDULER_PARAMETERS: "-N1"
    cmds:
      - 'mkdir -p ${JACAMAR_CI_SCRIPT_DIR}'
      - 'cp {{scripts}}/jobs/job-env.bash /tmp/pass-env.bash'
      - 'echo "echo "PASS"" >> /tmp/pass-env.bash'
      - '{{jacamar}} --no-auth run /tmp/pass-env.bash step_script'
      - 'flux jobs -a'
  result_parse:
    regex:
      flux_job:
        regex: 'ci-456_'
        action: store_true
  result_evaluate:
    result: 'flux_job and return_value == 0'

run-valid-sleep-submission:
  inherits_from: _job-home-env
  summary: Execute minute long successful job with custom env parameters.
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 789
      JACAMAR_CI_SCRIPT_DIR: /home/fluxuser/.jacamar-ci/scripts/group/project/789
      CUSTOM_ENV_FLUX_CI_PARAMETERS: "-N1"
    cmds:
      - 'mkdir -p ${JACAMAR_CI_SCRIPT_DIR}'
      - 'cp {{scripts}}/jobs/job-env.bash /tmp/sleep-env.bash'
      - 'echo "sleep 30" >> /tmp/sleep-env.bash'
      - '{{jacamar}} --no-auth run /tmp/sleep-env.bash step_script'
      - 'flux jobs -a'
  result_parse:
    regex:
      flux_job:
        regex: 'ci-789_.*3\d\.\d\ds'
        action: store_true
  result_evaluate:
    result: 'flux_job and return_value == 0'

run-failing-short-submission:
  inherits_from: _job-home-env
  summary: Execute minute long successful job.
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 1011
      JACAMAR_CI_SCRIPT_DIR: /home/fluxuser/.jacamar-ci/scripts/group/project/1011
      CUSTOM_ENV_SCHEDULER_PARAMETERS: "-N1"
    cmds:
      - 'mkdir -p ${JACAMAR_CI_SCRIPT_DIR}'
      - 'cp {{scripts}}/jobs/job-env.bash /tmp/fail-env.bash'
      - 'echo "exit 1" >> /tmp/fail-env.bash'
      - '{{jacamar}} --no-auth run /tmp/fail-env.bash step_script'
  result_evaluate:
    result: 'return_value == 1'

run-compute-login-env:
  inherits_from: _job-home-env
  summary: Verify new login environment provided for compute.
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 1213
      JACAMAR_CI_SCRIPT_DIR: /home/fluxuser/.jacamar-ci/scripts/group/project/1213
      CUSTOM_ENV_SCHEDULER_PARAMETERS: "-N1"
      SOME_TEST_VALUE: 42
    cmds:
      - 'mkdir -p ${JACAMAR_CI_SCRIPT_DIR}'
      - 'cp {{scripts}}/jobs/job-env.bash /tmp/login-env.bash'
      - '{{jacamar}} --no-auth run /tmp/login-env.bash step_script'
  result_parse:
    regex:
      flux_output:
        regex: 'SOME_TEST_VALUE=42'
        action: store_false
  result_evaluate:
    result: 'flux_output and return_value == 0'

run-precreate-output:
  inherits_from: _job-home-env
  summary: Wait in queue for existing job to pass.
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 1415
      JACAMAR_CI_SCRIPT_DIR: /home/fluxuser/.jacamar-ci/scripts/group/project/1415
      CUSTOM_ENV_SCHEDULER_PARAMETERS: "-N1"
    cmds:
      - 'mkdir -p ${JACAMAR_CI_SCRIPT_DIR}'
      - 'cp {{scripts}}/jobs/job-whoami.bash /tmp/sleep-whoami.bash'
      - 'echo "sleep 20" >> /tmp/sleep-whoami.bash'
      - 'sbatch -N5 /tmp/sleep-whoami.bash'
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/precreate-pass.bash'
      - '{{jacamar}} --no-auth run /tmp/precreate-pass.bash step_script'
  result_parse:
    regex:
      flux_output:
        regex: 'Purposely Passing'
        action: store_true
  result_evaluate:
    result: 'flux_output and return_value == 0'

run-invalid-parameters:
  inherits_from: _job-home-env
  summary: Invalid NFS caught and defaults used.
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 2002
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/group/project/2002
      CUSTOM_ENV_SCHEDULER_PARAMETERS: "--job-name=custom_job_name"
    cmds:
      - 'mkdir -p ${JACAMAR_CI_SCRIPT_DIR}'
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/nfs-job-pass.bash'
      - '{{jacamar}} --no-auth run /tmp/nfs-job-pass.bash step_script'
  result_parse:
    regex:
      warn_output:
        regex: 'Illegal argument detected\. Please remove\: --job-name'
        action: store_true
      error_output:
        regex: 'illegal arguments must be addressed before continuing'
        action: store_true
  result_evaluate:
    result: 'warn_output and error_output and return_value == 2'

###############################################################################
# timeout tests
###############################################################################

run-sigterm:
  inherits_from: _job-home-env
  summary: Verify logging for captured SIGTERM.
  variables:
    jobscript: /tmp/sigtermlogsrun.bash
  run:
    env:
      FLUX_CI_PARAMETERS: "-N1"
    cmds:
      - "cp {{scripts}}/jobs/job-background.bash {{jobscript}}"
      - "{{jacamar}} --no-auth prepare"
      - "{{jacamar}} --no-auth run {{jobscript}} step_script &"
      - "export SIGTERM_PID=$!"
      - "sleep 2"
      - "kill -s SIGTERM ${SIGTERM_PID}"
      # Wait long enough for all output from the job to be picked up. Changes
      # to file tailing may leave process open for additional time.
      - "sleep 30"
      - "ps -p $SIGTERM_PID > /dev/null && echo 'sigtermlogsrun process is still running'"
      - 'cat /tmp/background-${CUSTOM_ENV_CI_JOB_ID} || echo "SUCCESS-BACKGROUND"'
  result_parse:
    regex:
      ps:
        regex: 'sigtermlogs process is still running'
        action: store_false
      output:
        regex: 'SUCCESS-BACKGROUND'
        action: store_true
  result_evaluate:
    result: 'ps and output'

run-context-timeout:
  inherits_from: _job-home-env
  summary: Encounter response defined timeout and cancel job.
  variables:
    jobscript: /tmp/contexttimeout.bash
  run:
    env:
      CUSTOM_ENV_CI_JOB_ID: 5000
      JACAMAR_CI_SCRIPT_DIR: /home/fluxuser/.jacamar-ci/scripts/group/project/5000
      CUSTOM_ENV_SCHEDULER_PARAMETERS: "-N1"
      JACAMAR_CI_RUNNER_TIMEOUT: '4s'
    cmds:
      - "cp {{scripts}}/jobs/job-background.bash {{jobscript}}"
      - '{{jacamar}} --no-auth prepare'
      - '{{jacamar}} --no-auth run {{jobscript}} step_script'
      - 'cat /tmp/background-${CUSTOM_ENV_CI_JOB_ID} || echo "SUCCESS-CONTEXT-TIMEOUT"'
  result_parse:
    regex:
      output:
        regex: 'SUCCESS-CONTEXT-TIMEOUT'
        action: store_true
  result_evaluate:
    result: 'output'

run-sigterm-logging:
  inherits_from: _job-home-env
  summary: Verify (user) log file for captured SIGTERM.
  variables:
    jobscript: /tmp/sigterm_logfile.bash
  run:
    env:
      CUSTOM_ENV_SCHEDULER_PARAMETERS: "-N1"
      CUSTOM_ENV_CI_JOB_ID: 1002
      JACAMAR_CI_SCRIPT_DIR:  /home/fluxuser/.jacamar-ci/scripts/group/project/1002
    cmds:
      - "cp {{scripts}}/jobs/job-background.bash {{jobscript}}"
      - "{{jacamar}} --no-auth prepare"
      - "{{jacamar}} --no-auth run {{jobscript}} step_script &"
      - "export SIGTERM_PID=$!"
      - "sleep 2"
      - "kill -s SIGTERM ${SIGTERM_PID}"
      # Wait long enough for all output from the job to be picked up. Changes
      # to file tailing may leave process open for additional time.
      - "sleep 30"
      - "cat ${JACAMAR_CI_SCRIPT_DIR}/sigterm.log"
  result_parse:
    regex:
      output:
        regex: 'Signal captured, attempting to cancel job'
        action: store_true
      error_msg:
        regex: 'error\: <nil>'
        action: store_true
  result_evaluate:
    result: 'output and error_msg'
