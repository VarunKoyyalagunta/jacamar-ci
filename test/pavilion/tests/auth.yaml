_cfg-env:
  variables:
    configs: $CI_PROJECT_DIR/test/pavilion/share/configs
    scripts: $CI_PROJECT_DIR/test/pavilion/share/scripts
    plugins: $CI_PROJECT_DIR/test/pavilion/share/go-plugins
    responses: $CI_PROJECT_DIR/test/pavilion/share/responses
  run:
    env:
      CUSTOM_ENV_CI_CONCURRENT_ID: 0
      CUSTOM_ENV_CI_JOB_ID: 123
      CUSTOM_ENV_CI_JOB_TOKEN: abc123efg456hij789kl
      CUSTOM_ENV_CI_RUNNER_SHORT_TOKEN: token123
      CUSTOM_ENV_CI_SERVER_URL: http://127.0.0.1:5000
      CUSTOM_ENV_CI_JOB_JWT: '{{jwt("default")}}'
      CUSTOM_ENV_CI_RUNNER_VERSION: '14.5.0-rc1'
      SYSTEM_FAILURE_EXIT_CODE: 2
      BUILD_FAILURE_EXIT_CODE: 1
      JOB_RESPONSE_FILE: '{{responses}}/valid_default.json'
      JACAMAR_CI_RUNNER_TIMEOUT: '1h0m0s'

_user-home-dir:
  inherits_from: _cfg-env
  run:
    env:
      JACAMAR_CI_BASE_DIR: /home/user/.jacamar-ci
      JACAMAR_CI_BUILDS_DIR: /home/user/.jacamar-ci/builds
      JACAMAR_CI_CACHE_DIR: /home/user/.jacamar-ci/cache
      JACAMAR_CI_SCRIPT_DIR: /home/user/.jacamar-ci/scripts/group/project/123
      JACAMAR_CI_AUTH_USERNAME: user
      JACAMAR_CI_PROJECT_PATH: group/project

_user-authorized-setuid:
  inherits_from: _user-home-dir
  run:
    env:
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/auth/user_authorized_setuid.toml")}}'

_jacamar-authorized-setuid:
  inherits_from: _user-home-dir
  run:
    env:
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/auth/jacamar_authorized_setuid.toml")}}'

_root-dir-creation:
  inherits_from: _cfg-env
  run:
    env:
      JACAMAR_CI_BASE_DIR: /dir/test/user
      JACAMAR_CI_BUILDS_DIR: /dir/test/user/builds
      JACAMAR_CI_CACHE_DIR: /dir/test/user/cache
      JACAMAR_CI_SCRIPT_DIR: /dir/test/user/scripts/token123/000/group/project/123
      JACAMAR_CI_AUTH_USERNAME: user
      JACAMAR_CI_PROJECT_PATH: group/project
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/auth/root_dir_creation.toml")}}'

_job-dir-env:
  inherits_from: _cfg-env
  run:
    env:
      JACAMAR_CI_BASE_DIR: /var/tmp/user
      JACAMAR_CI_BUILDS_DIR: /var/tmp/user/builds
      JACAMAR_CI_CACHE_DIR: /var/tmp/user/cache
      JACAMAR_CI_SCRIPT_DIR: /var/tmp/user/scripts/group/project/123
      JACAMAR_CI_AUTH_USERNAME: user
      JACAMAR_CI_PROJECT_PATH: group/project
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/auth/dir_authorized_setuid.toml")}}'

_jacamar-source-setuid:
  inherits_from: _user-home-dir
  run:
    env:
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/auth/source_script_test.toml")}}'

_user-authorized-sigterm:
  inherits_from: _user-home-dir
  run:
    env:
      CUSTOM_ENV_CI_RUNNER_SHORT_TOKEN: sigtermlogs
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/capabilities/user_home_sigterm.toml")}}'

###############################################################################
#  misc/basic tests
###############################################################################

###############################################################################
# config_exec tests
###############################################################################

config-setuid-valid:
  inherits_from: _cfg-env
  summary: Load a basic valid configuration with a setuid run mechanism.
  run:
    cmds:
      - 'jacamar-auth -u config --configuration {{configs}}/auth/user_authorized_setuid.toml'
  result_parse:
    regex:
      username:
        regex: '"JACAMAR_CI_AUTH_USERNAME":"user"'
        action: store_true
      builds_dir:
        regex: '"JACAMAR_CI_BASE_DIR":"/home/user/.jacamar-ci"'
        action: store_true
  result_evaluate:
    result: 'username and builds_dir and return_value == 0'

config-setuid-valid-v1:
  inherits_from: _cfg-env
  summary: Load a basic valid configuration with a setuid run mechanism with CI_JOB_JWT_V1.
  run:
    env:
      CUSTOM_ENV_CI_JOB_JWT: '{{jwt("version1")}}'
    cmds:
      - 'jacamar-auth -u config --configuration {{configs}}/auth/user_authorized_setuid.toml'
  result_parse:
    regex:
      username:
        regex: '"JACAMAR_CI_AUTH_USERNAME":"user"'
        action: store_true
      builds_dir:
        regex: '"JACAMAR_CI_BASE_DIR":"/home/user/.jacamar-ci"'
        action: store_true
  result_evaluate:
    result: 'username and builds_dir and return_value == 0'

config-dir-valid:
  inherits_from: _cfg-env
  summary: Load a basic valid configuration with setuid and a data_dir defined, custom allowed.
  run:
    cmds:
      - 'jacamar-auth -u config --configuration {{configs}}/auth/dir_authorized_setuid.toml'
  result_parse:
    regex:
      username:
        regex: '"JACAMAR_CI_AUTH_USERNAME":"user"'
        action: store_true
      builds_dir:
        regex: '"JACAMAR_CI_BASE_DIR":"/var/tmp/user"'
        action: store_true
  result_evaluate:
    result: 'username and builds_dir and return_value == 0'

config-not-found-obfuscated:
  inherits_from: _cfg-env
  summary: Pass an invalid configuration file. Expected obfuscated error message
  run:
    cmds:
      - 'jacamar-auth config --configuration /invalid/file/defined/config.toml'
  result_parse:
    regex:
      error_msg:
        regex: 'Error encountered during job: unable to identify job context, please verify configuration/deployment'
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 2'

config-not-found-unobfuscated:
  inherits_from: _cfg-env
  summary: Pass an invalid configuration file. Expected unobfuscated error message.
  run:
    cmds:
      - 'jacamar-auth -u config --configuration /invalid/file/defined/config.toml'
  result_parse:
    regex:
      error_msg:
        regex: 'open /invalid/file/defined/config.toml: no such file or directory'
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 2'

config-user-blocklist:
  inherits_from: _cfg-env
  summary: CI user blocklisted by configuration. Expect restricted failure message.
  run:
    cmds:
      - 'jacamar-auth config --configuration {{configs}}/auth/user_blocklisted.toml'
  result_parse:
    regex:
      error_msg:
        regex: 'you are currently unauthorized to use this runner'
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 2'

config-group-blocklist:
  inherits_from: _cfg-env
  summary: CI group blocklisted by configuration. Expect restricted failure message.
  run:
    cmds:
      - 'jacamar-auth config --configuration {{configs}}/auth/user_group_blocklisted.toml'
  result_parse:
    regex:
      error_msg:
        regex: 'you are currently unauthorized to use this runner'
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 2'

config-pipeline-source-allowlist:
  inherits_from: _cfg-env
  summary: CI pipeline source not in configured allowlist. Expect restricted failure message.
  run:
    env:
      CUSTOM_ENV_CI_JOB_JWT: '{{jwt(["pipeline_source=web"])}}'
    cmds:
      - 'jacamar-auth -u config --configuration {{configs}}/auth/pipeline_source_allowlist.toml'
  result_parse:
    regex:
      error_msg:
        regex: 'invalid pipeline trigger source \(web\) detected'
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 2'

config-blocklist-unobfuscated:
  inherits_from: _cfg-env
  summary: CI user blocklisted by configuration, config_exec detailed warning. Expect expanded error message.
  run:
    cmds:
      - 'jacamar-auth -u config --configuration {{configs}}/auth/user_group_blocklisted.toml'
  result_parse:
    regex:
      error_msg:
        regex: 'user, is not on the user allowlist and is in the group blocklist'
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 2'

config-miss-downscope:
  inherits_from: _cfg-env
  summary: Missing downscope configuration leads to job failure.
  run:
    env:
      CUSTOM_ENV_CI_JOB_JWT: '{{jwt(["user_login=root"])}}'
    cmds:
      - 'jacamar-auth config --configuration {{configs}}/auth/job_home_no_downscope.toml'
  result_parse:
    regex:
      error_msg:
        regex: 'unable to initialize runner mechanism'
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 2'

config-user-root:
  inherits_from: _cfg-env
  summary: GitLab trigger user is root, with auth/setuid enabled. Expect restricted failure message.
  run:
    env:
      CUSTOM_ENV_CI_JOB_JWT: '{{jwt(["user_login=root"])}}'
    cmds:
      - 'jacamar-auth config --configuration {{configs}}/auth/user_authorized_setuid.toml'
  result_parse:
    regex:
      error_msg:
        regex: 'you are currently unauthorized to use this runner'
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 2'

config-valid-runas:
  inherits_from: _cfg-env
  summary: Validate runas user against script, using user defined target.
  run:
    env:
      CUSTOM_ENV_RUNAS_TARGET: passtest
    cmds:
      - 'jacamar-auth -u config --configuration {{configs}}/auth/runas_enabled.toml'
  result_parse:
    regex:
      username:
        regex: '"JACAMAR_CI_AUTH_USERNAME":"passtest"'
        action: store_true
  result_evaluate:
    result: 'username and return_value == 0'

config-multi-echo-runas:
  inherits_from: _cfg-env
  summary: Execute simple multi-line override via RunAs.
  run:
    cmds:
      - 'jacamar-auth config --configuration {{configs}}/auth/runas_multi.toml'
  result_parse:
    regex:
      username:
        regex: '"JACAMAR_CI_AUTH_USERNAME":"jacamar"'
        action: store_true
  result_evaluate:
    result: 'username and return_value == 0'

config-failed-jwt:
  inherits_from: _cfg-env
  summary: Failed to validated jwt (bad KeyID) during config.
  run:
    env:
      CUSTOM_ENV_CI_JOB_JWT: '{{jwt("fail_kid")}}'
    cmds:
      - 'jacamar-auth -u config --configuration {{configs}}/auth/user_authorized_setuid.toml'
  result_parse:
    regex:
      error_msg:
        regex: 'unable to parse supplied CI_JOB_JWT'
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 2'

config-root-dir-creation:
  inherits_from: _cfg-env
  summary: Load a basic valid configuration with a setuid run mechanism and root directory creation.
  run:
    cmds:
      - 'jacamar-auth config --configuration {{configs}}/auth/root_dir_creation.toml'
  result_evaluate:
    result: 'return_value == 0'

config-runas-datadir:
  inherits_from: _cfg-env
  summary: Validate runas user against script, override data_dir.
  run:
    env:
      CUSTOM_ENV_RUNAS_TARGET: datadir
    cmds:
      - 'jacamar-auth -u config --configuration {{configs}}/auth/runas_enabled.toml'
  result_parse:
    regex:
      username:
        regex: '"JACAMAR_CI_AUTH_USERNAME":"newuser"'
        action: store_true
      base_dir:
        regex: '"JACAMAR_CI_BASE_DIR":"/runas/example/newuser"'
        action: store_true
      builds_dir:
        regex: '"builds_dir":"/runas/example/newuser/builds/token123/000"'
        action: store_true
      cache_dir:
        regex: '"cache_dir":"/runas/example/newuser/cache"'
        action: store_true
  result_evaluate:
    result: 'username and base_dir and builds_dir and cache_dir and return_value == 0'

config-server-response-invalid:
  inherits_from: _cfg-env
  summary: Invalid server url encountered in environment
  run:
    env:
      CUSTOM_ENV_CI_SERVER_URL: https://gitlab.example.pizza
      JOB_RESPONSE_FILE: '{{responses}}/invalid_url.json'
    cmds:
      - 'jacamar-auth -u config --configuration {{configs}}/auth/user_authorized_setuid.toml'
  result_parse:
    regex:
      error_msg:
        regex: 'invalid ServerURL detected, do not attempt to influence CI_SERVER_URL'
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 2'

config-runner-version-error:
  inherits_from: _cfg-env
  summary: Encounter invalid runner version and raise error but continue.
  run:
    env:
      CUSTOM_ENV_CI_RUNNER_VERSION: '13.11.0.ecp'
    cmds:
      - 'jacamar-auth config --configuration {{configs}}/auth/user_authorized_setuid.toml'
  result_parse:
    regex:
      error_msg:
        regex: 'Invalid runner version detected, minimum supported: 14.1'
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 0'

config-custom-builds-vars:
  inherits_from: _cfg-env
  summary: Verify the CUSTOM_CI_BUILDS_DIR variables resolved.
  run:
    cmds:
      - "export CUSTOM_ENV_CUSTOM_CI_BUILDS_DIR='${HOME}/.runner'"
      - 'jacamar-auth config --configuration {{configs}}/auth/dir_authorized_setuid.toml'
  result_parse:
    regex:
      builds_dir:
        regex: '"builds_dir":"/home/user/.runner/user/builds/token123/000"'
        action: store_true
  result_evaluate:
    result: 'builds_dir'

config-invalid-jwt-claim:
  inherits_from: _cfg-env
  summary: Allowed to validated jwt (bad project_id) during debug.
  run:
    env:
      CUSTOM_ENV_CI_JOB_JWT: '{{jwt(["project_id=test"])}}'
    cmds:
      - 'jacamar-auth -u config --configuration {{configs}}/auth/dir_authorized_setuid.toml'
  result_parse:
    regex:
      error_msg:
        regex: 'unable to parse supplied CI_JOB_JWT'
        action: store_true
      error_details:
        regex: 'Error\:Field validation for .ProjectID. failed'
  result_evaluate:
    result: 'error_msg and error_details and return_value == 2'

config-runas-env:
  inherits_from: _cfg-env
  summary: Validate runas environment is containts expected keys.
  run:
    cmds:
      - 'jacamar-auth -u config --configuration {{configs}}/auth/runas_env.toml'
  result_parse:
    regex:
      username:
        regex: '"JACAMAR_CI_AUTH_USERNAME":"passtest"'
        action: store_true
  result_evaluate:
    result: 'username and return_value == 0'

config-failed-no-exp:
  inherits_from: _cfg-env
  summary: Failed to validated jwt (no exp) during config.
  run:
    env:
      CUSTOM_ENV_CI_JOB_JWT: '{{jwt("noexp")}}'
    cmds:
      - 'jacamar-auth -u config --configuration {{configs}}/auth/user_authorized_setuid.toml'
  result_parse:
    regex:
      error_msg:
        regex: 'unable to parse supplied CI_JOB_JWT: missing expiration'
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 2'

###############################################################################
# prepare_exec tests
###############################################################################

prepare-no-state:
  summary: Lack required stateful variables during config job.
  run:
    cmds:
      - 'jacamar-auth prepare'
  result_parse:
    regex:
      error_msg:
        regex: 'unable to identify job context'
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 2'

prepare-setuid-valid:
  inherits_from: _user-authorized-setuid
  summary: User's job environment prepared successfully.
  run:
    cmds:
      - 'jacamar-auth prepare'
  result_parse:
    regex:
      output:
        regex: 'uid=\d*\(user\)'
        action: store_true
      groups:
        regex: 'groups=\d*\(user\),\d*\(ci\)'
  result_evaluate:
    result: 'output and return_value == 0'

prepare-root-creation:
  inherits_from: _root-dir-creation
  summary: Verify root directory creation permission structure.
  run:
    cmds:
      - 'mkdir -p /dir/test && chmod -R 701 /dir'
      - 'jacamar-auth --unobfuscated prepare'
      - 'su - user -c "ls -l /dir/test/user"'
  result_parse:
    regex:
      output:
        regex: 'drwx------.*user.*user.*builds'
        action: store_true
  result_evaluate:
    result: 'output and return_value == 0'

prepare-verify-uid-gid:
  inherits_from: _user-authorized-setuid
  summary: User's job environment prepared successfully with effective, real, and saved ids.
  run:
    cmds:
      - 'jacamar-auth prepare'
  result_parse:
    regex:
      uid:
        regex: 'Uid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
      gid:
        regex: 'Gid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
  result_evaluate:
    result: 'uid and gid and return_value == 0'

prepare-failed-jwt:
  inherits_from: _user-authorized-setuid
  summary: Failed to validated (no JWT) jwt during preapre.
  run:
    env:
      CUSTOM_ENV_CI_JOB_JWT: ''
    cmds:
      - 'jacamar-auth -u prepare'
  result_parse:
    regex:
      error_msg:
        regex: "missing required environment variable CUSTOM_ENV_CI_JOB_JWT"
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 2'

prepare-incorrect-env-cfg:
  inherits_from: _job-dir-env
  summary: Downscope environment configureated but contains invalid chracter
  run:
    env:
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/auth/dir_authorized_invalid_env.toml")}}'
    cmds:
      - 'jacamar-auth -u prepare'
  result_parse:
    regex:
      output:
        regex: 'HELLO=WORLD'
        action: store_true
  result_evaluate:
    result: 'output and return_value == 0'

###############################################################################
# run_exec tests
###############################################################################

run-setuid-valid:
  inherits_from: _user-authorized-setuid
  summary: User's job environment run successfully.
  run:
    cmds:
      - 'mkdir -p $JACAMAR_CI_SCRIPT_DIR && chown -R user:user /home/user'
      - 'touch /tmp/test.bash && echo date >> /tmp/test.bash'
      - 'jacamar-auth run /tmp/test.bash prepare_script'
  result_parse:
    regex:
      output:
        regex: 'uid=\d*\(user\)'
        action: store_true
  result_evaluate:
    result: 'output and return_value == 0'

run-background-script:
  inherits_from: _jacamar-authorized-setuid
  summary: Run passing job that background sleep process.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-background.bash /tmp/job-301.bash'
      - 'jacamar-auth run /tmp/job-301.bash step_script'
      - 'pgrep sleep -u user || echo "SUCCESS-301"'
  result_parse:
    regex:
      output:
        regex: 'SUCCESS-301'
        action: store_true
  result_evaluate:
    result: 'output and return_value == 0'

run-root-creation:
  inherits_from: _root-dir-creation
  summary: Verify root directory creation permission structure.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/root-pass.bash'
      - 'jacamar-auth run /tmp/root-pass.bash after_script'
  result_parse:
    regex:
      output:
        regex: 'Purposely Passing'
        action: store_true
  result_evaluate:
    result: 'output and return_value == 0'

run-check-groups:
  inherits_from: _user-authorized-setuid
  summary: User's job environment run successfully.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/sup-groups.bash /tmp/sup-groups.bash'
      - 'jacamar-auth run /tmp/sup-groups.bash get_sources'
  result_parse:
    regex:
      output:
        regex: 'groups=\d*\(user\),\d*\(ci\)$'
        action: store_true
  result_evaluate:
    result: 'output and return_value == 0'

run-setgid-script:
  inherits_from: _root-dir-creation
  summary: User accessible script with setgid and corresponding executable bit set.
  variables:
    script: /tmp/setgid-bit-pass.bash
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-cap.bash {{script}}'
      - 'chmod g+s {{script}}'
      - 'jacamar-auth run {{script}} prepare_script'
  result_parse:
    regex:
      message:
        regex: 'Running as user UID: \d* GID: \d*$'
        action: store_true
      output:
        regex: 'groups=\d*\(user\),\d*\(ci\)$'
        action: store_true
  result_evaluate:
    result: 'message and output and return_value == 0'

run-setgid-directory:
  inherits_from: _root-dir-creation
  summary: Run script in accessible directory with setgid bit set.
  variables:
    gid_dir: /tmp/setgid-dir
    gid_script: setgid-bit-pass.bash
    script: job-execute-setgid.bash
  run:
    cmds:
      - 'mkdir {{gid_dir}}'
      - 'cp {{scripts}}/jobs/job-cap.bash {{gid_dir}}/{{gid_script}}'
      - 'cp {{scripts}}/jobs/{{script}} /tmp/{{script}}'
      - 'chown -R root:jacamar {{gid_dir}} && chmod -R 755 {{gid_dir}} && chmod g+s {{gid_dir}}'
      - 'ls -la {{gid_dir}}'
      - 'jacamar-auth run /tmp/{{script}} download_artifacts'
  result_parse:
    regex:
      folder:
        regex: 'drwxr-sr-x.*root\sjacamar.*\.$'
        action: store_true
      output:
        regex: 'groups=\d*\(user\),\d*\(ci\)$'
        action: store_true
  result_evaluate:
    result: 'folder and output and return_value == 0'

run-verify-egid-noseccomp:
  inherits_from: _root-dir-creation
  summary: Execute sgid copy of /usr/bin/id and verify egid output (seccomp disabled).
  variables:
    script: /tmp/job-env-egid.bash
    setgid: /tmp/id-setgid
  run:
    env:
      JACAMAR_CI_CONFIG_STR: '{{config_base64("/builds/ecp-ci/jacamar-ci/test/pavilion/share/configs/auth/root_dir_creation_noseccomp.toml")}}'
    cmds:
      - 'cp {{scripts}}/jobs/job-env.bash {{script}}'
      - 'echo /tmp/id-setgid >> {{script}}'
      - 'cp /usr/bin/id {{setgid}}'
      - 'chgrp jacamar {{setgid}}'
      - 'chmod 755 {{setgid}} && chmod g+s {{setgid}}'
      - 'jacamar-auth run {{script}} step_script'
  result_parse:
    regex:
      output:
        regex: 'egid=\d*\(jacamar\)'
        action: store_true
  result_evaluate:
    result: 'output and return_value == 0'

run-failed-jwt:
  inherits_from: _user-authorized-setuid
  summary: Failed to validated (no subject) jwt during run.
  run:
    env:
      CUSTOM_ENV_CI_JOB_JWT: '{{jwt(["sub="])}}'
    cmds:
      - 'jacamar-auth -u run /tmp/test.bash prepare_script'
  result_parse:
    regex:
      error_msg:
        regex: 'unable to parse supplied CI_JOB_JWT'
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 2'

###############################################################################
# cleanup_exec tests
###############################################################################

cleanup-blocklist-user:
  inherits_from: _cfg-env
  summary: CI user blocklisted by configuration, cleanup_exec detailed warning. Expect expanded error message.
  run:
    cmds:
      - 'jacamar-auth cleanup --configuration {{configs}}/auth/user_blocklisted.toml'
  result_parse:
    regex:
      error_msg:
        regex: 'user, is not in the user allowlist and is in the user blocklist'
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 0'

cleanup-blocklist-group:
  inherits_from: _cfg-env
  summary: CI user blocklisted by configuration, cleanup_exec detailed warning. Expect expanded error message.
  run:
    cmds:
      - 'jacamar-auth cleanup --configuration {{configs}}/auth/user_group_blocklisted.toml'
  result_parse:
    regex:
      error_msg:
        regex: 'user, is not on the user allowlist and is in the group blocklist'
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 0'

cleanup-user-root:
  inherits_from: _cfg-env
  summary: GitLab trigger user is root, with auth/setuid enabled. Expect expanded error message.
  run:
    env:
      JACAMAR_CI_CONFIG_STR:
      CUSTOM_ENV_CI_JOB_JWT: '{{jwt(["user_login=root"])}}'
    cmds:
      - 'jacamar-auth cleanup --configuration {{configs}}/auth/user_authorized_setuid.toml'
  result_parse:
    regex:
      error_msg:
        regex: 'cannot execute as root while authorization is enforced'
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 0'

cleanup-setuid-valid:
  inherits_from: _user-authorized-setuid
  summary: User's job environment cleanup successfully.
  run:
    cmds:
      - 'jacamar-auth cleanup --configuration {{configs}}/auth/user_authorized_setuid.toml'
  result_parse:
    regex:
      output:
        regex: 'uid=\d*\(user\)'
        action: store_true
  result_evaluate:
    result: 'output'

cleanup-always-exit0:
  # We do not want cleanup failures to be confused with configuration failure.
  summary: Cleanup should only fail if the cleanup itself fails.
  run:
    cmds:
      - 'jacamar-auth cleanup --configuration not_found.toml'
  result_parse:
    regex:
      error_msg:
        regex: "open not_found.toml: no such file or directory"
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 0'

cleanup-fail-runas:
  inherits_from: _cfg-env
  summary: Failed runas validation.
  run:
    env:
      CUSTOM_ENV_RUNAS_TARGET: failtest
    cmds:
      - 'jacamar-auth cleanup --configuration {{configs}}/auth/runas_enabled.toml'
  result_parse:
    regex:
      error_msg:
        regex: 'invalid authorization target user'
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 0'

cleanup-miss-downscope:
  inherits_from: _cfg-env
  summary: Missing downscope configuration leads to job failure.
  run:
    env:
      CUSTOM_ENV_CI_JOB_JWT: '{{jwt(["user_login=root"])}}'
      JACAMAR_CI_CONFIG_STR:
    cmds:
      - 'jacamar-auth cleanup --configuration {{configs}}/auth/job_home_no_downscope.toml'
  result_parse:
    regex:
      error_msg:
        regex: 'ensure supported downscope is defined'
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 0'

cleanup-bad-runas-checksum:
  inherits_from: _cfg-env
  summary: Validate script defined but invalid checksum identified.
  run:
    env:
      CUSTOM_ENV_CI_JOB_JWT: '{{jwt("default")}}'
    cmds:
      - 'jacamar-auth cleanup --configuration {{configs}}/auth/validate_invalid_checksum.toml'
  result_parse:
    regex:
      error_msg:
        regex: 'invalid checksum found for file'
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 0'

cleanup-failed-jwt:
  inherits_from: _cfg-env
  summary: Failed to validated jwt (HS256) during cleanup.
  run:
    env:
      CUSTOM_ENV_CI_JOB_JWT: '{{jwt("hs256")}}'
    cmds:
      - 'jacamar-auth -u cleanup --configuration {{configs}}/auth/user_authorized_setuid.toml'
  result_parse:
    regex:
      error_msg:
        regex: 'unable to parse supplied CI_JOB_JWT'
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 0'

cleanup-server-response-invalid:
  inherits_from: _cfg-env
  summary: Invalid server url encountered in environment.
  run:
    env:
      CUSTOM_ENV_CI_SERVER_URL: https://gitlab.example.pizza
      JOB_RESPONSE_FILE: '{{responses}}/invalid_url.json'
    cmds:
      - 'jacamar-auth -u cleanup --configuration {{configs}}/auth/user_authorized_setuid.toml'
  result_parse:
    regex:
      error_msg:
        regex: 'invalid ServerURL detected, do not attempt to influence CI_SERVER_URL'
        action: store_true
  result_evaluate:
    result: 'error_msg and return_value == 0'

###############################################################################
# scope basic checks
###############################################################################

scope-preapre:
  inherits_from: _user-authorized-setuid
  summary: Basic downscope verification prepare_exec.
  run:
    cmds:
      - 'jacamar-auth prepare'
  result_parse:
    regex:
      uid:
        regex: 'Uid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
      gid:
        regex: 'Gid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
  result_evaluate:
    result: 'uid and gid and return_value == 0'

scope-run-prepare_script:
  inherits_from: _user-authorized-setuid
  summary: Basic downscope verification run_exec + prepare_script.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/scope-run-prepare_script.bash'
      - 'jacamar-auth run /tmp/scope-run-prepare_script.bash prepare_script'
  result_parse:
    regex:
      uid:
        regex: 'Uid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
      gid:
        regex: 'Gid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
  result_evaluate:
    result: 'uid and gid and return_value == 0'

scope-run-get_sources:
  inherits_from: _user-authorized-setuid
  summary: Basic downscope verification run_exec + get_sources.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/scope-run-get_sources.bash'
      - 'jacamar-auth run /tmp/scope-run-get_sources.bash get_sources'
  result_parse:
    regex:
      uid:
        regex: 'Uid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
      gid:
        regex: 'Gid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
  result_evaluate:
    result: 'uid and gid and return_value == 0'

scope-run-restore_cache:
  inherits_from: _user-authorized-setuid
  summary: Basic downscope verification run_exec + restore_cache.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/scope-run-restore_cache.bash'
      - 'jacamar-auth run /tmp/scope-run-restore_cache.bash restore_cache'
  result_parse:
    regex:
      uid:
        regex: 'Uid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
      gid:
        regex: 'Gid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
  result_evaluate:
    result: 'uid and gid and return_value == 0'

scope-run-download_artifacts:
  inherits_from: _user-authorized-setuid
  summary: Basic downscope verification run_exec + download_artifacts.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/scope-run-download_artifacts.bash'
      - 'jacamar-auth run /tmp/scope-run-download_artifacts.bash download_artifacts'
  result_parse:
    regex:
      uid:
        regex: 'Uid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
      gid:
        regex: 'Gid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
  result_evaluate:
    result: 'uid and gid and return_value == 0'

scope-run-step_script:
  inherits_from: _user-authorized-setuid
  summary: Basic downscope verification run_exec + step_script.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/scope-run-step_script.bash'
      - 'jacamar-auth run /tmp/scope-run-step_script.bash step_script'
  result_parse:
    regex:
      uid:
        regex: 'Uid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
      gid:
        regex: 'Gid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
  result_evaluate:
    result: 'uid and gid and return_value == 0'

scope-run-after_script:
  inherits_from: _user-authorized-setuid
  summary: Basic downscope verification run_exec + after_script.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/scope-run-after_script.bash'
      - 'jacamar-auth run /tmp/scope-run-after_script.bash after_script'
  result_parse:
    regex:
      uid:
        regex: 'Uid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
      gid:
        regex: 'Gid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
  result_evaluate:
    result: 'uid and gid and return_value == 0'

scope-run-archive_cache:
  inherits_from: _user-authorized-setuid
  summary: Basic downscope verification run_exec + archive_cache.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/scope-run-archive_cache.bash'
      - 'jacamar-auth run /tmp/scope-run-archive_cache.bash archive_cache'
  result_parse:
    regex:
      uid:
        regex: 'Uid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
      gid:
        regex: 'Gid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
  result_evaluate:
    result: 'uid and gid and return_value == 0'

scope-run-upload_artifacts_on_success:
  inherits_from: _user-authorized-setuid
  summary: Basic downscope verification run_exec + upload_artifacts_on_success.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/scope-run-upload_artifacts_on_success.bash'
      - 'jacamar-auth run /tmp/scope-run-upload_artifacts_on_success.bash upload_artifacts_on_success'
  result_parse:
    regex:
      uid:
        regex: 'Uid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
      gid:
        regex: 'Gid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
  result_evaluate:
    result: 'uid and gid and return_value == 0'

scope-run-cleanup_file_variables:
  inherits_from: _user-authorized-setuid
  summary: Basic downscope verification run_exec + cleanup_file_variables.
  run:
    cmds:
      - 'cp {{scripts}}/jobs/job-pass.bash /tmp/scope-run-cleanup_file_variables.bash'
      - 'jacamar-auth run /tmp/scope-run-cleanup_file_variables.bash cleanup_file_variables'
  result_parse:
    regex:
      uid:
        regex: 'Uid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
      gid:
        regex: 'Gid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
  result_evaluate:
    result: 'uid and gid and return_value == 0'

scope-cleanup:
  inherits_from: _user-authorized-setuid
  summary: Basic downscope verification prepare_exec.
  run:
    cmds:
      - 'jacamar-auth cleanup --configuration {{configs}}/auth/user_authorized_setuid.toml'
  result_parse:
    regex:
      uid:
        regex: 'Uid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
      gid:
        regex: 'Gid:\s*1000\s*1000\s*1000\s*1000$'
        action: store_true
  result_evaluate:
    result: 'uid and gid and return_value == 0'

###############################################################################
# sigterm tests - separate to avoid potential collision with scripting
###############################################################################

run-sigterm:
  inherits_from: _user-authorized-sigterm
  summary: Verify logging for captured SIGTERM.
  variables:
    jobscript: /tmp/sigtermlogsrun.bash
  run:
    cmds:
      - "cp {{scripts}}/jobs/job-background.bash {{jobscript}}"
      - "jacamar-auth prepare"
      - "jacamar-auth run {{jobscript}} step_script &"
      - "export SIGTERM_PID=$!"
      - "sleep 2"
      - "kill -s SIGTERM ${SIGTERM_PID}"
      - "sleep 42"
      - "cat /tmp/sigterm.log"
      - "ps -p $SIGTERM_PID > /dev/null && echo 'sigtermlogsrun process is still running'"
      - 'cat /tmp/background-${CUSTOM_ENV_CI_JOB_ID} || echo "SUCCESS-BACKGROUND"'
  result_parse:
    regex:
      info:
        regex: 'SIGTERM captured, initiating job termination'
        action: store_true
      notify:
        regex: 'notifying process of SIGTERM'
        action: store_true
      failed:
        regex: 'failed to terminate process'
        action: store_false
      exited:
        regex: 'Error executing run_exec'
        action: store_true
      ps:
        regex: 'sigtermlogs process is still running'
        action: store_false
      output:
        regex: 'SUCCESS-BACKGROUND'
        action: store_true
  result_evaluate:
    result: 'info and notify and failed and exited and ps and output'
