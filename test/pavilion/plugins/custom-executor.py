import copy
import jwt as pyjwt
import os.path
from pathlib import Path
import pwd
import subprocess
import sys

from pavilion import commands
from pavilion import expression_functions

####################
# Custom Executors #
####################


class Home(expression_functions.CoreFunctionPlugin):
    """Obtain the target user's home directory."""

    def __init__(self):
        super().__init__(
            name="home",
            arg_specs=(str,))
    @staticmethod
    def home(user):
        if user == "_current":
            return Path.home()
        return pwd.getpwnam(user).pw_dir


class ConfigToBase64(expression_functions.CoreFunctionPlugin):
    """Load a config file an return it's base64 equivalent."""

    def __init__(self):
        """Setup plugin"""

        super().__init__(
            name="config_base64",
            arg_specs=(str,))

        self.go_file = os.path.expandvars("$CI_PROJECT_DIR/tools/config2base64/config2base64.go")

    def config_base64(self, config_file):
        name = subprocess.check_output([
            'go', 'run', self.go_file, os.path.expandvars(config_file)])
        return name.strip().decode('UTF-8')


class Base64ToConfig(expression_functions.CoreFunctionPlugin):
    """Decode an encoded configuration file to human readable."""

    def __init__(self):
        """Setup plugin"""

        super().__init__(
            name="base64_config",
            arg_specs=(str,))

        self.go_file = os.path.expandvars("$CI_PROJECT_DIR/tools/config2base64/config2base64.go")

    def base64_config(self, encoded_cfg):
        name = subprocess.check_output([
            'go', 'run', self.go_file, os.path.expandvars(encoded_cfg)])
        return name.strip().decode('UTF-8')

###################
# JSON Web Tokens #
###################


class MockAPI(commands.Command):
    def __init__(self):
        super().__init__(
            'mock-api',
            'Start mock GitLab (http) API background for testing.',
            short_help='Mock GitLab (http) API.',
            aliases=['api'])

        self.out = open(os.path.expandvars("$HOME/test.out"), 'w')
        self.script = os.path.expandvars("$CI_PROJECT_DIR/tools/mock-gl-api/gl-api.py")

    def run(self, pav_cfg, args):
        """Run the show command's chosen sub-command."""

        return self._start_server(pav_cfg)

    def _start_server(self, pav_cfg):
        subprocess.Popen(
            ['nohup', 'python3', '-u', self.script],
            stdout=self.out,
            stderr=self.out,
        )


class JWT(expression_functions.CoreFunctionPlugin):
    """Manage the creation of JWT using supplied payload."""

    def __init__(self):
        """Setup plugin"""

        super().__init__(
            name="jwt",
            arg_specs=None)

        self.private_key = b"""-----BEGIN PRIVATE KEY-----
MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQC7VJTUt9Us8cKj
MzEfYyjiWA4R4/M2bS1GB4t7NXp98C3SC6dVMvDuictGeurT8jNbvJZHtCSuYEvu
NMoSfm76oqFvAp8Gy0iz5sxjZmSnXyCdPEovGhLa0VzMaQ8s+CLOyS56YyCFGeJZ
qgtzJ6GR3eqoYSW9b9UMvkBpZODSctWSNGj3P7jRFDO5VoTwCQAWbFnOjDfH5Ulg
p2PKSQnSJP3AJLQNFNe7br1XbrhV//eO+t51mIpGSDCUv3E0DDFcWDTH9cXDTTlR
ZVEiR2BwpZOOkE/Z0/BVnhZYL71oZV34bKfWjQIt6V/isSMahdsAASACp4ZTGtwi
VuNd9tybAgMBAAECggEBAKTmjaS6tkK8BlPXClTQ2vpz/N6uxDeS35mXpqasqskV
laAidgg/sWqpjXDbXr93otIMLlWsM+X0CqMDgSXKejLS2jx4GDjI1ZTXg++0AMJ8
sJ74pWzVDOfmCEQ/7wXs3+cbnXhKriO8Z036q92Qc1+N87SI38nkGa0ABH9CN83H
mQqt4fB7UdHzuIRe/me2PGhIq5ZBzj6h3BpoPGzEP+x3l9YmK8t/1cN0pqI+dQwY
dgfGjackLu/2qH80MCF7IyQaseZUOJyKrCLtSD/Iixv/hzDEUPfOCjFDgTpzf3cw
ta8+oE4wHCo1iI1/4TlPkwmXx4qSXtmw4aQPz7IDQvECgYEA8KNThCO2gsC2I9PQ
DM/8Cw0O983WCDY+oi+7JPiNAJwv5DYBqEZB1QYdj06YD16XlC/HAZMsMku1na2T
N0driwenQQWzoev3g2S7gRDoS/FCJSI3jJ+kjgtaA7Qmzlgk1TxODN+G1H91HW7t
0l7VnL27IWyYo2qRRK3jzxqUiPUCgYEAx0oQs2reBQGMVZnApD1jeq7n4MvNLcPv
t8b/eU9iUv6Y4Mj0Suo/AU8lYZXm8ubbqAlwz2VSVunD2tOplHyMUrtCtObAfVDU
AhCndKaA9gApgfb3xw1IKbuQ1u4IF1FJl3VtumfQn//LiH1B3rXhcdyo3/vIttEk
48RakUKClU8CgYEAzV7W3COOlDDcQd935DdtKBFRAPRPAlspQUnzMi5eSHMD/ISL
DY5IiQHbIH83D4bvXq0X7qQoSBSNP7Dvv3HYuqMhf0DaegrlBuJllFVVq9qPVRnK
xt1Il2HgxOBvbhOT+9in1BzA+YJ99UzC85O0Qz06A+CmtHEy4aZ2kj5hHjECgYEA
mNS4+A8Fkss8Js1RieK2LniBxMgmYml3pfVLKGnzmng7H2+cwPLhPIzIuwytXywh
2bzbsYEfYx3EoEVgMEpPhoarQnYPukrJO4gwE2o5Te6T5mJSZGlQJQj9q4ZB2Dfz
et6INsK0oG8XVGXSpQvQh3RUYekCZQkBBFcpqWpbIEsCgYAnM3DQf3FJoSnXaMhr
VBIovic5l0xFkEHskAjFTevO86Fsz1C2aSeRKSqGFoOQ0tmJzBEs1R6KqnHInicD
TQrKhArgLXX4v3CddjfTRJkFWDbE/CkvKZNOrcf1nhaGCPspRJj2KUkj1Fhl9Cnc
dn/RsYEONbwQSjIfMPkvxF+8HQ==
-----END PRIVATE KEY-----"""

        self.default_payload = {
            "project_path": "group/project",
            "job_id": "123",
            "ref": "main",
            "ref_protected": "false",
            "ref_type": "branch",
            "sub": "project_path:group/project:ref_type:branch:ref:main",
            "aud": "http://127.0.0.1:5000",
            "iss": "http://127.0.0.1:5000",
            "namespace_id": "1001",
            "namespace_path": "group",
            "pipeline_id": "456",
            "pipeline_source": "web",
            "project_id": "2002",
            "user_login": "user",
            "user_id": "789",
            "user_email": "user@example.com",
            "jti": "jwtid1",
            "exp": 4102491661,
            "nbf": 1609462861,
        }
        self.signed_jwt = ""

    def _validate_arg(self, arg, spec):
        # TODO (paulbry): Issue supporting dictionaries, need to explore further.
        if not isinstance(arg, (list, str)):
            raise FunctionPluginError(
                "The len function only accepts lists and string. "
                "Got {} of type {}.".format(arg, type(arg).__name__)
            )
        return arg

    signature = "len(list|str)"

    def jwt(self, arg):
        if isinstance(arg, str):
            self._defined_payload(arg)
        elif isinstance(arg, list):
            self._list_payload(arg)

        return self.signed_jwt

    def _defined_payload(self, arg):
        """Generate signed JWT based upon pre-defined payloads."""
        payload = copy.copy(self.default_payload)

        if arg == "default":
            return self._sign(payload)
        elif arg == "fail_kid":
            return self._sign(payload, kid="fail")
        elif arg == "hs256":
            self.signed_jwt = pyjwt.encode(payload, "secretT0k3n", algorithm="HS256")
            return
        elif arg == "version1":
            payload.pop("aud", None)
            payload["sub"] = "job_123"
            return self._sign(payload)
        elif arg == "noexp":
            payload.pop("exp", None)
            return self._sign(payload)
        else:
            sys.exit("unsupported default proposed")

    def _list_payload(self, arg):
        """Generate signed JWT based upon pre-defined payload with overrides from list."""
        payload = copy.copy(self.default_payload)

        for x in arg:
            over = x.split('=', 1)
            payload[over[0]] = over[1]

        self._sign(payload)

    def _sign(self, payload, kid="test", alg="RS256"):
        self.signed_jwt = pyjwt.encode(
            payload,
            self.private_key,
            algorithm=alg,
            headers={"kid": kid}
        )
