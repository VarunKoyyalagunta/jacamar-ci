#!/usr/bin/env bash

set -eo pipefail
set +o noclobber

source /tmp/jacamar-ci/test/pavilion/run/functions.bash

cp /usr/local/sbin/* /usr/local/bin

migrate
mock_gl_api

vars=$(env | grep FLUX)
for v in $vars
do
  echo "export ${v}" >> /home/fluxuser/.bashrc
done

su fluxuser -c "source ${PAV_CI_DIR}/test/pavilion/run/functions.bash && pav_series" && rs=0 || rs=1
relocate

exit $rs
