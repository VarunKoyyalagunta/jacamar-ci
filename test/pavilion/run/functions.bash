export PAV_CI_DIR="/builds/ecp-ci/jacamar-ci"
export PAV_TMP_DIR="/tmp/jacamar-ci"
export PAV_CONFIG_DIR="${CI_PROJECT_DIR}/test/pavilion"

migrate() {
    mkdir -p ${PAV_CI_DIR}
    cp -R ${PAV_TMP_DIR} ${PAV_CI_DIR}/..

    # Ensure downscope user has access to view/create test directories.
    chmod -R 755 "${PAV_CI_DIR}"
}

mock_gl_api() {
    pav mock-api

    # Ensure permissions to support tests in userspace.
    chmod -R 777 "${PAV_CI_DIR}/test/pavilion/working_dir"
}

pav_series() {
  local result=1

  echo "Running ${PAV_TARGET} series"
  pushd ${PAV_CI_DIR}/test/pavilion > /dev/null
    pav series ${PAV_TARGET}
    sleep 5
    pav wait

    # Output organized results for easier visual review.
    printf "\n\n\n"
    pav status --failed --limit 1000
    printf "\n\n"
    pav status -s

    pav status --limit 1000 | grep -q "FAIL" || result=0
    if [ "${result}" -eq "0" ]; then
      pav status --limit 1000 | grep -q "RUN_TIMEOUT" && result=1
    fi
  popd > /dev/null

  echo "Test series exit status: ${result}"

  return $result
}

relocate() {
  echo "Relocating test results: test/pavilion"

  # Avoid issues copying builds files.
  rm -rf "${PAV_CI_DIR}/test/pavilion/working_dir/builds"

  cp -R "${PAV_CI_DIR}/test/pavilion/working_dir" \
    "${PAV_TMP_DIR}/test/pavilion" > /dev/null
}
