#!/usr/bin/env bash

set -eo pipefail
set +o noclobber

source /tmp/jacamar-ci/test/pavilion/run/functions.bash

cp /usr/local/sbin/* /usr/local/bin

sacctmgr -i create user user name=user || echo "Added user"
supervisorctl restart slurmctld > /dev/null

migrate
mock_gl_api

export HOME=/home/user
export PATH=$PATH:/usr/local/go/bin

su -m user -c "source ${PAV_CI_DIR}/test/pavilion/run/functions.bash && pav_series" && rs=0 || rs=1
relocate

exit $rs
