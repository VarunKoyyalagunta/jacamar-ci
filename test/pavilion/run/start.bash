#!/usr/bin/env bash

# Execute Pavilion tests within a containers.

set -eo pipefail
set +o noclobber

root_dir=$(pwd)
ci_dir="/builds/ecp-ci/jacamar-ci"
tmp_dir="/tmp/jacamar-ci"

#############################
# General testing functions #
#############################

build() {
  echo "Building Jacamar Binaries..."
  echo "Image: ${BUILD_IMG}"
  ${CONTAINER_RUNTIME} run \
    --rm \
    -v "${root_dir}:${ci_dir}:z" \
    -w ${ci_dir} \
    -t ${BUILD_IMG} \
    bash -c "make build"

  # Enforce permissions, issues can be caused by vbox mounts.
  chmod -R 755 binaries/
}

###########################
# Shell testing functions #
###########################

auth() {
  echo "Testing Jacamar Authorization..."
  echo "Image: ${PAVILION_IMG}"
  ${CONTAINER_RUNTIME} run \
    --rm \
    -v "${root_dir}:${tmp_dir}:z" \
    -v "${root_dir}/binaries:/usr/local/sbin" \
    -e "CI_PROJECT_DIR=${ci_dir}" \
    -e "PAV_TARGET=test-auth" \
    -t  ${PAVILION_IMG} \
    bash -c "bash ${tmp_dir}/test/pavilion/run/general.bash"
}

jacamar() {
  echo "Testing Jacamar + Shell Executor..."
  echo "Image: ${PAVILION_IMG}"
  ${CONTAINER_RUNTIME} run \
    --rm \
    -v "${root_dir}:${tmp_dir}:z" \
    -v "${root_dir}/binaries:/usr/local/sbin" \
    -e "CI_PROJECT_DIR=${ci_dir}" \
    -e "PAV_TARGET=test-jacamar" \
    -u "user" \
    -t ${PAVILION_IMG} \
    bash -c "bash ${tmp_dir}/test/pavilion/run/general.bash"
}

log() {
  echo "Testing Logging..."
  echo "Image: ${PAVILION_IMG}"
  ${CONTAINER_RUNTIME} run \
    --rm \
    -v "${root_dir}:${tmp_dir}:z" \
    -v "${root_dir}/binaries:/usr/local/sbin" \
    -e "CI_PROJECT_DIR=${ci_dir}" \
    -e "PAV_TARGET=test-log" \
    -t  ${PAVILION_IMG} \
    bash -c "bash ${tmp_dir}/test/pavilion/run/general.bash"
}

su_test() {
  echo "Testing Jacamar Authorization + Su Downscoping..."
  echo "Image: ${PAVILION_IMG}"
  ${CONTAINER_RUNTIME} run \
    --rm \
    -v "${root_dir}:${tmp_dir}:z" \
    -v "${root_dir}/binaries:/usr/local/sbin" \
    -e "CI_PROJECT_DIR=${ci_dir}" \
    -e "PAV_TARGET=test-su" \
    -t ${PAVILION_IMG} \
    bash -c "${tmp_dir}/test/pavilion/run/general.bash"
}

seccomp() {
  echo "Testing Jacamar Seccomp..."
  echo "Image: ${PAVILION_IMG}"
  ${CONTAINER_RUNTIME} run \
    --rm \
    -v "${root_dir}:${tmp_dir}:z" \
    -e "CI_PROJECT_DIR=${ci_dir}" \
    -e "PAV_TARGET=test-seccomp" \
    -t  ${PAVILION_IMG} \
    bash -c "${tmp_dir}/test/pavilion/run/general.bash"
}

sudo_test() {
  echo "Testing Sudo Downscoping..."
  echo "Image: ${PAVILION_IMG}"
  ${CONTAINER_RUNTIME} run \
    --rm \
    -v "${root_dir}:${tmp_dir}:z" \
    -v "${root_dir}/binaries:/usr/local/sbin" \
    -e "CI_PROJECT_DIR=${ci_dir}" \
    -e "PAV_TARGET=test-sudo" \
    -t ${PAVILION_IMG} \
    bash -c "${tmp_dir}/test/pavilion/run/sudo.bash"
}

capabilities() {
  echo "Testing Capabilities + Setuid Downscoping..."
  echo "Image: ${PAVILION_IMG}"
  ${CONTAINER_RUNTIME} run \
    --rm \
    -v "${root_dir}:${tmp_dir}:z" \
    -v "${root_dir}/binaries:/usr/local/sbin" \
    -e "CI_PROJECT_DIR=${ci_dir}" \
    -e "PAV_TARGET=test-capabilities" \
    -t ${PAVILION_IMG} \
    bash -c "${tmp_dir}/test/pavilion/run/capabilities.bash"
}

strace() {
  echo "Testing Strace Authorization..."
  echo "Image: ${PAVILION_IMG}"
  ${CONTAINER_RUNTIME} run \
    --rm \
    --cap-add=SYS_PTRACE \
    -v "${root_dir}:${tmp_dir}:z" \
    -v "${root_dir}/binaries:/usr/local/sbin" \
    -e "CI_PROJECT_DIR=${ci_dir}" \
    -e "PAV_TARGET=test-strace" \
    -t  ${PAVILION_IMG} \
    bash -c "bash ${tmp_dir}/test/pavilion/run/general.bash"

    pushd "${root_dir}" > /dev/null
        mkdir -p "strace-results"
        for i in `find "test/pavilion/working_dir" -name "*.strace" -type f`; do
          echo "$i"
          cp "$i" "strace-results/"
        done
    popd > /dev/null
}

###########################
# Slurm testing functions #
###########################

slurm() {
  echo "Testing Jacamar + Slurm (CLI) Executor..."
  echo "Image: ${SLURM_IMG}"
  ${CONTAINER_RUNTIME} run \
    -h slurmctl \
    --rm \
    --cap-add=SYS_ADMIN \
    -v "${root_dir}:${tmp_dir}:z" \
    -v "${root_dir}/binaries:/usr/local/sbin" \
    -e "CI_PROJECT_DIR=${ci_dir}" \
    -e "PAV_TARGET=test-slurm" \
    -t "${SLURM_IMG}" \
    bash -c "${tmp_dir}/test/pavilion/run/slurm.bash"
}

###########################
# Flux testing functions #
###########################

flux() {
  echo "Testing Jacamar + Flux (CLI) Executor..."
  echo "Image: ${FLUX_IMG}"
  ${CONTAINER_RUNTIME} run \
    --rm \
    -v "${root_dir}:${tmp_dir}:Z" \
    -v "${root_dir}/binaries:/opt/jacamar/bin" \
    -e "CI_PROJECT_DIR=${ci_dir}" \
    -e "PAV_TARGET=test-flux" \
    -u "fluxuser" \
    -t "${FLUX_IMG}" \
    flux start /bin/bash -c "sudo -E -u root ${tmp_dir}/test/pavilion/run/flux.bash"
}

###########################
# PBS testing functions #
###########################

pbs() {
  if [ "${CONTAINER_RUNTIME}" == "podman" ] ; then
    echo "*****************************************************"
    echo "Warning: 'setrlimit NPROC' may be encountered using"
    echo "Podman, preventing test completion."
    echo "See: https://github.com/containers/podman/issues/6389"
    echo "*****************************************************"
  fi

  echo "Testing Jacamar + PBS (CLI) Executor..."
  echo "Image: ${PBS_IMG}"
  ${CONTAINER_RUNTIME} run \
    -h buildkitsandbox \
    --rm \
    --cap-add=SYS_PTRACE \
    --cap-add=CAP_SYS_RESOURCE \
    -v "${root_dir}:${tmp_dir}:z" \
    -v "${root_dir}/binaries:/usr/local/sbin" \
    -e "CI_PROJECT_DIR=${ci_dir}" \
    -e "PAV_TARGET=test-pbs" \
    -t "${PBS_IMG}" \
    bash -c "${tmp_dir}/test/pavilion/run/pbs.bash"
}

#################
# Function case #
#################

case "${1}" in
  auth)
    auth
    ;;
  build)
    build
    ;;
  capabilities)
    capabilities
    ;;
  capabilities_user)
    capabilities_user
    ;;
  flux)
    flux
    ;;
  jacamar)
    jacamar
    ;;
  log)
    log
    ;;
  migrate)
    migrate
    ;;
  pbs)
    pbs
    ;;
  sacctmgr)
    sacctmgr_update
    ;;
  slurm)
    slurm
    ;;
  strace)
    strace
    ;;
  su)
    su_test
    ;;
  seccomp)
    seccomp
    ;;
  sudo)
    sudo_test
    ;;
  run)
    run
    ;;
  *)
    echo "Invalid argument: ${0}"
    exit 1
    ;;
esac
