#!/bin/bash

# Enforce that all expected RunAs variables exists.
required=(
  # jacamar-ci
  "RUNAS_CURRENT_USER"
  "RUNAS_TARGET_USER"
  # gljobctx-go
  "JWT_NAMESPACE_ID"
  "JWT_NAMESPACE_PATH"
  "JWT_PROJECT_ID"
  "JWT_PROJECT_PATH"
  "JWT_USER_ID"
  "JWT_USER_LOGIN"
  "JWT_USER_EMAIL"
  "JWT_PIPELINE_ID"
  "JWT_PIPELINE_SOURCE"
  "JWT_JOB_ID"
  "JWT_REF"
  "JWT_REF_TYPE"
  "JWT_REF_PROTECTED"
  "JWT_ISS"
  "JWT_AUD"
  "JWT_SUB"
)

for e in ${required[@]}; do
  if [[ ! $(env | grep "${e}") ]]; then
    echo "Missing variable ${e}"
    exit 1
  fi
done

echo '{"username": "passtest"}'
exit 0
