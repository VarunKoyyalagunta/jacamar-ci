# Mock GitLab API

Script for mocking API against Jacamar binaries where
traditional methods are not possible.

Requirements:

* `python3`
* `flask`

Recommend launch via `nohop`:

```console
$ nohup python3 -u ./tools/mock-gl-api/gl-api.py > test.out &
nohup: ignoring input and redirecting stderr to stdout
```

Verify that the `TRUSTED_CI_SERVER_URL` environment variable is
to `http://127.0.0.1:5000` for Jacamar to properly interact
with the endpoint.

## /-/jwks

Replicate the functionality of a `gitlab.com/-/jwks` endpoint.

## /api/v4/jobs

Return jobs API based upon `JOB-TOKEN` supplied in header.
