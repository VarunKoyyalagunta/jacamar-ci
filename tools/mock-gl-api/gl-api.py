#!/usr/bin/env python3

import flask
from flask import request, jsonify, Response

app = flask.Flask(__name__)
app.config["DEBUG"] = True

# /-/jwks
#   test = https://jwt.io/ public key
#   old = Outdated jwt.io example key
jwks = {
    "keys": [
        {
            "kty":"RSA",
            "kid":"test",
            "e":"AQAB",
            "n":"u1SU1LfVLPHCozMxH2Mo4lgOEePzNm0tRgeLezV6ffAt0gunVTLw7onLRnrq0_IzW7yWR7QkrmBL7jTKEn5u-qKhbwKfBstIs-bMY2Zkp18gnTxKLxoS2tFczGkPLPgizskuemMghRniWaoLcyehkd3qqGElvW_VDL5AaWTg0nLVkjRo9z-40RQzuVaE8AkAFmxZzow3x-VJYKdjykkJ0iT9wCS0DRTXu269V264Vf_3jvredZiKRkgwlL9xNAwxXFg0x_XFw005UWVRIkdgcKWTjpBP2dPwVZ4WWC-9aGVd-Gyn1o0CLelf4rEjGoXbAAEgAqeGUxrcIlbjXfbcmw",
            "use":"sig",
            "alg":"RS256"
        }, {
            "kty":"RSA",
            "kid":"old",
            "e":"AQAB",
            "n":"nzyis1ZjfNB0bBgKFMSvvkTtwlvBsaJq7S5wA-kzeVOVpVWwkWdVha4s38XM_pa_yr47av7-z3VTmvDRyAHcaT92whREFpLv9cj5lTeJSibyr_Mrm_YtjCZVWgaOYIhwrXwKLqPr_11inWsAkfIytvHWTxZYEcXLgAXFuUuaS3uF9gEiNQwzGTU1v0FqkqTBr4B8nW3HCN47XUu0t8Y0e-lf4s4OxQawWD79J9_5d3Ry0vbV3Am1FtGJiJvOwRsIfVChDpYStTcHTCMqtvWbV6L11BWkpzGXSW4Hv43qa-GSYOD2QU68Mb59oSk2OB-BtOLpJofmbGEGgvmwyCI9Mw",
            "use":"sig",
            "alg":"RS256",
        }
    ]
}


@app.route('/-/jwks', methods=['GET'])
def api_all():
    return jsonify(jwks)


@app.route('/api/v4/job', methods=['GET'])
def api_job():
    token = request.headers.get('JOB-TOKEN')
    if token is None:
        return Response("404 Not Found", 404)

    jobs = {
        "user": {
            "bot": False
        }
    }

    if token.startswith('bot'):
        jobs["user"]["bot"] = True

    return jsonify(jobs)


app.run()
