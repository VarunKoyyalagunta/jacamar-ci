#!/usr/bin/env bash

set -eo pipefail
set +o noclobber

if [ ${CUSTOM_ENV_CI_JOB_NAME} == "prep" ]; then 
    bash $1
    exit 0
fi

mkdir -p /var/tmp/${CUSTOM_ENV_CI_RUNNER_VERSION}
mv $1 /var/tmp/${CUSTOM_ENV_CI_RUNNER_VERSION}/$2

pushd /var/tmp/${CUSTOM_ENV_CI_RUNNER_VERSION}
    sed -i "s/${CUSTOM_ENV_CI_RUNNER_SHORT_TOKEN}/runnerShortToken/g" $2
    sed -i "s/${CUSTOM_ENV_CI_JOB_TOKEN}/ciJobToken/g" $2
    sed -i "s/${CUSTOM_ENV_CI_JOB_JWT_V1}/ciJobJWT_V1/g" $2
    sed -i "s/${CUSTOM_ENV_CI_JOB_JWT_V2}/ciJobJWT_V2/g" $2
    sed -i "s/${CUSTOM_ENV_CI_JOB_JWT}/ciJobJWT/g" $2
    sed -i "s/${CUSTOM_ENV_CI_SERVER_HOST}/gitlab.example.com/g" $2
    sed -i "s/${CUSTOM_ENV_GITLAB_USER_NAME}/user/g" $2
    sed -i "s/${CUSTOM_ENV_GITLAB_USER_EMAIL}/user@example.com/g" $2
    sed -i "s/${CUSTOM_ENV_GITLAB_USER_LOGIN}/user/g" $2
    
    # Removing this makes the file easier to read and we don't need to account for it.
    sed -i 's/-----BEGIN CERTIFICATE-----.*-----END CERTIFICATE-----//' $2
popd

exit 0
