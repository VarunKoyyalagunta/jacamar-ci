//go:build linux
// +build linux

package main

import (
	"bytes"
	"flag"
	"io"
	"log"
	"os"
	"os/exec"
	"strconv"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/seccomp"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
)

var (
	noNewPriv   bool
	cfgFile     string
	commandName string
	commandArgs []string
)

func main() {
	flag.StringVar(&cfgFile, "cfg", "", "Jacamar CI configuration file (required).")
	flag.BoolVar(&noNewPriv, "no_new_priv", false, "Enable no_new_priv (optional).")
	flag.Parse()

	// Establish all configuration, seccomp, and prctl calls.
	estCommand(flag.Args())
	opt := estOptions()
	estSeccomp(opt)
	noNewPrivs()

	cmd := exec.Command(commandName, commandArgs...)

	var stdoutBuf, stderrBuf bytes.Buffer
	cmd.Stdout = io.MultiWriter(os.Stdout, &stdoutBuf)
	cmd.Stderr = io.MultiWriter(os.Stderr, &stderrBuf)
	cmd.Stdin = nil

	runCmd(cmd)
}

func estCommand(args []string) {
	if len(args) == 0 {
		log.Fatal("Command required but not found.")
	}

	commandName = args[0]
	if len(args) > 1 {
		commandArgs = args[1:]
	}
}

func estOptions() configure.Options {
	cfg, err := configure.NewConfig(cfgFile)
	if err != nil {
		log.Fatal(err)
	}

	return cfg.Options()
}

func estSeccomp(opt configure.Options) {
	usr, _ := authuser.CurrentUser()
	uid, _ := strconv.Atoi(usr.Uid)
	gid, _ := strconv.Atoi(usr.Gid)

	err := seccomp.Factory{
		Stage: "testing",
		Opt:   opt,
		Usr: authuser.UserContext{
			Username: usr.Username,
			HomeDir:  usr.HomeDir,
			UID:      uid,
			GID:      gid,
		},
	}.Establish()
	if err != nil {
		log.Fatal(err)
	}
}

func noNewPrivs() {
	if !noNewPriv {
		return
	}

	err := seccomp.SetNoNewPrivs()
	if err != nil {
		log.Fatal(err)
	}
}

func runCmd(cmd *exec.Cmd) {
	err := cmd.Start()
	if err == nil {
		err = cmd.Wait()
	}

	if err != nil {
		log.Fatal(err)
	}
}
