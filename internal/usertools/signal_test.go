package usertools

import (
	"os/exec"
	"strconv"
	"syscall"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

func TestManageSignal(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	cmd := exec.Command("sleep", "20&")
	_ = cmd.Start()

	tests := map[string]struct {
		msg  func(ctrl *gomock.Controller) logging.Messenger
		sCmd func() arguments.SignalCmd
	}{
		"failed to identify signal": {
			msg: func(ctrl *gomock.Controller) logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Error("invalid signal test defined (SIGKILL|SIGTERM)")
				return m
			},
			sCmd: func() arguments.SignalCmd {
				return arguments.SignalCmd{
					Signal: "test",
					PID:    "1234",
				}
			},
		},
		"failed to translate PID": {
			msg: func(ctrl *gomock.Controller) logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Error("strconv.Atoi: parsing \"abc\": invalid syntax")
				return m
			},
			sCmd: func() arguments.SignalCmd {
				return arguments.SignalCmd{
					Signal: "SIGTERM",
					PID:    "abc",
				}
			},
		},
		"SIGKILL no such process": {
			msg: func(ctrl *gomock.Controller) logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Error("no such process")
				return m
			},
			sCmd: func() arguments.SignalCmd {
				return arguments.SignalCmd{
					Signal: "SIGKILL",
					PID:    "999999",
				}
			},
		},
		"SIGTERM sent": {
			msg: func(ctrl *gomock.Controller) logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				return m
			},
			sCmd: func() arguments.SignalCmd {
				return arguments.SignalCmd{
					Signal: "SIGTERM",
					PID:    strconv.Itoa(cmd.Process.Pid),
				}
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			ManageSignal(tt.msg(ctrl), tt.sCmd(), func() {}, func() {})
		})
	}
}

func Test_processSignal(t *testing.T) {
	t.Run("test timeout encountered", func(t *testing.T) {
		err := processSignal(
			arguments.SignalCmd{
				Signal: "SIGTERM",
				PID:    "123",
			},
			func(i int, signal syscall.Signal) error {
				time.Sleep(20 * time.Second)
				return nil
			},
		)
		assert.EqualError(t, err, "command timeout encountered")
	})
}
