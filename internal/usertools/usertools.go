// Package usertools is a generic catch-all for Jacamar CI that is meant to support any range
// of functionality required by CI jobs within userspace. This primarily entails the creation
// and management of the user's directories/files and the strict adherence to permission
// requirements. All actions are designed for the downscoped target user and do not strictly
// support use cases involving a privileged user via the jacamar-auth application.
package usertools

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

const (
	// OwnerPermissions for any file/directory created where only the Owner has rwx permission.
	OwnerPermissions = 0700
	// GroupPermissions for any file/directory created where Owner has rwx permission and Group has r-x permission.
	GroupPermissions = 0750

	// OwnerUmask is the Umask user for directory creation where only the Owner has rwx permission.
	OwnerUmask = 077
	// GroupUmask is the Umask user for directory creation where Owner has rwx permission and Group has r-x permission.
	GroupUmask = 027

	rwPermissions = 0600
	gitAsk        = "#!/usr/bin/env bash\n\nbuiltin echo %s\n"
)

// PreCleanupVerification is targeted to support ensuring the minimum possible user
// directories and CI environment is available before any executor/configuration
// specific cleanup actions can be taken.
func PreCleanupVerification(state envparser.StatefulEnv, cfg configure.General) error {
	directories := []string{
		state.BaseDir,
		state.BuildsDir,
		state.CacheDir,
		state.ScriptDir,
	}

	for _, path := range directories {
		dir, err := identifyDirectory(path, cfg)
		if err != nil {
			return err
		}

		if err = dir.preCleanupWF(); err != nil {
			return err
		}
	}

	return nil
}

func (d dirInfo) preCleanupWF() error {
	if err := d.verifyBasePermissions(); err != nil {
		return err
	}

	// The verifyBasePermissions() will return a nil if a directory doesn't
	// exist. In our case we will want to generate an error.
	if d.notFound {
		return fmt.Errorf("%w: %s", os.ErrNotExist, d.path)
	}

	return nil
}

// CreateScript generates a script with the specified contents and 700 permissions.
// Lack of contents or invalid paths will result in an error.
func CreateScript(path, contents string) error {
	return generateFile(filepath.Clean(path), contents, OwnerPermissions)
}

// CreateFile generates a file with the specified contents and 600 permissions.
// Lack of contents or invalid paths will result in an error.
func CreateFile(path, contents string) error {
	return generateFile(filepath.Clean(path), contents, rwPermissions)
}

func generateFile(filepath, contents string, perm os.FileMode) error {
	file, err := os.OpenFile(filepath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, perm)
	if err != nil {
		return err
	}

	err = writeContents(file, contents)
	if err != nil {
		_ = file.Close()
		return err
	}

	// OpenFile observes umask, we should attempt to modify just in case.
	_ = os.Chmod(filepath, perm)

	return file.Close()
}

func writeContents(f *os.File, contents string) error {
	if contents == "" {
		return errors.New("no contents detected for file creation")
	}
	_, err := f.WriteString(contents)

	return err
}

// GitPassCreation maintains the folder and script creation for a CI job's
// GIT_ASKPASS script. A valid CI_JOB_TOKEN and the full path to the script
// must be provided.
func GitPassCreation(path, token string) (err error) {
	if token == "" {
		return errors.New("missing job token for GIT_ASKPASS")
	}

	path = filepath.Clean(path)

	i := strings.LastIndex(path, "/")
	if i == -1 {
		// Unlikely, just avoid unexpected issues causes in manipulation.
		return errors.New("invalid file path provided for script")
	}

	err = os.MkdirAll(path[:i], OwnerPermissions)
	if err != nil {
		return
	}

	return CreateScript(path, fmt.Sprintf(gitAsk, token))
}
