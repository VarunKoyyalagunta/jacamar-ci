package usertools

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

func Test_ConfirmHelperConst(t *testing.T) {
	// At no point should we change this value!
	assert.Equal(t, 0700, OwnerPermissions, "invalid change to OwnerPermissions constant")
	assert.Equal(t, 0750, GroupPermissions, "invalid change to GroupPermissions constant")
	assert.Equal(t, 077, OwnerUmask, "invalid change to OwnerUmask constant")
	assert.Equal(t, 027, GroupUmask, "invalid change to GroupUmask constant")
}

func mockMissing() envparser.StatefulEnv {
	return envparser.StatefulEnv{
		BaseDir:   "/does/not/exist",
		BuildsDir: "/does/not/exist/builds",
		CacheDir:  "/does/not/exist/cache",
		ScriptDir: "/does/not/exist/script",
	}
}

func mockPermissions() envparser.StatefulEnv {
	return envparser.StatefulEnv{
		BaseDir:   "/var/tmp",
		BuildsDir: "/var/tmp/builds",
		CacheDir:  "/var/tmp/cache",
		ScriptDir: "/var/tmp/script",
	}
}

func Test_PreCleanupVerification(t *testing.T) {
	// create and verify functional directory structure
	baseDir := t.TempDir() + "/ci-dir"
	functional := mocksFunctional(baseDir)
	permissions := configure.General{}
	err := CreateDirectories(functional, permissions)
	assert.DirExists(t, functional.BaseDir)
	assert.NoError(t, err, "create functional test directories")

	tests := map[string]dirTests{
		"base directory does not exist": {
			state: mockMissing(),
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"no base directory defined": {
			state: mocksEmptyBase(),
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "no base directory defined, verify runner configuration")
			},
		},
		"permissions issues on base directory": {
			state: mockPermissions(),
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"valid directories encountered": {
			state: functional,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := PreCleanupVerification(tt.state, tt.opt)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_FileGeneration(t *testing.T) {
	t.Run("create script", func(t *testing.T) {
		file := t.TempDir() + "/CreateScript"
		err := CreateScript(file, `hello world`)
		assert.NoError(t, err, "CreateScript()")

		f, err := os.Stat(file)
		assert.NoError(t, err, "Stat(file)")
		assert.Equal(t, "-rwx------", f.Mode().String())
	})

	t.Run("create file", func(t *testing.T) {
		file := t.TempDir() + "/CreateFile"
		err := CreateFile(file, `hello world`)
		assert.NoError(t, err, "CreateFile()")

		f, err := os.Stat(file)
		assert.NoError(t, err, "Stat(file)")
		assert.Equal(t, "-rw-------", f.Mode().String())
	})

	t.Run("openfile error observed", func(t *testing.T) {
		err := generateFile("", "test", OwnerPermissions)
		assert.Error(t, err)
	})

	t.Run("no contents provided", func(t *testing.T) {
		err := generateFile(t.TempDir()+"/contents", "", OwnerPermissions)
		assert.EqualError(t, err, "no contents detected for file creation")
	})
}

func TestGitPassCreation(t *testing.T) {
	tests := map[string]struct {
		path       string
		token      string
		assertErr  func(*testing.T, error)
		assertFile func(*testing.T, string)
	}{
		"invalid path": {
			path:  "no-forward-slash",
			token: "abc123",
			assertErr: func(t *testing.T, err error) {
				assert.EqualError(t, err, "invalid file path provided for script")
			},
		},
		"root file path": {
			path:  "/",
			token: "abc123",
			assertErr: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"valid file created": {
			path:  t.TempDir() + "/.creds/pass",
			token: "abc123",
			assertErr: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertFile: func(t *testing.T, s string) {
				info, _ := os.Stat(s)
				assert.Equal(t, "-rwx------", info.Mode().String())
				b, _ := os.ReadFile(s)
				assert.Equal(t, fmt.Sprintf(gitAsk, "abc123"), string(b))
			},
		},
		"missing token": {
			token: "",
			assertErr: func(t *testing.T, err error) {
				assert.EqualError(t, err, "missing job token for GIT_ASKPASS")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := GitPassCreation(tt.path, tt.token)

			if tt.assertErr != nil {
				tt.assertErr(t, err)
			}
			if tt.assertFile != nil {
				tt.assertFile(t, tt.path)
			}
		})
	}
}
