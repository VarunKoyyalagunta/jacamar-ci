package bash

import (
	"context"
	"io"
	"os/exec"
	"os/user"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_authuser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

type bashTests struct {
	script    string
	targetEnv map[string]string

	s       *shell
	absCmdr *command.AbstractCommander
	auth    *mock_authuser.MockAuthorized

	assertError  func(*testing.T, error)
	assertShell  func(*testing.T, *shell)
	assertString func(*testing.T, string)
}

// testSysLog is an empty logger, syslog tested via Pavilion2 primarily.
var testSysLog *logrus.Entry

func init() {
	ll := logrus.New()
	ll.Out = io.Discard
	testSysLog = logrus.NewEntry(ll)
}

func mockGenConfig(ctrl *gomock.Controller) *mock_configure.MockConfigurer {
	m := mock_configure.NewMockConfigurer(ctrl)
	m.EXPECT().General().Return(configure.General{
		KillTimeout: "5s",
	}).AnyTimes()
	return m
}

func Test_shell_PipeOutput(t *testing.T) {
	ctx := context.TODO() // Not utilized in tests.
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	cfg := mockGenConfig(ctrl)

	cur, _ := user.Current()

	bash := (Factory{
		AbsCmdr: command.NewAbsCmdr(ctx, cfg.General(), m, envparser.ExecutorEnv{}, testSysLog),
		Cfg:     cfg,
	}).CreateBaseShell(cur.HomeDir)

	tests := map[string]bashTests{
		"exit 0 command, successfully executed": {
			s:      bash,
			script: `echo "hello" && exit 0`,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "hello")
			},
		},
		"exit 1 command, successfully executed": {
			s:      bash,
			script: `echo "hello" && exit 1`,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "hello")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := tt.s.PipeOutput(tt.script)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertString != nil {
				tt.assertString(t, tt.script)
			}
		})
	}
}

func Test_shell_ReturnOutput(t *testing.T) {
	ctx := context.TODO() // Not utilized in tests.
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	cfg := mockGenConfig(ctrl)

	cur, _ := user.Current()

	bash := (Factory{
		AbsCmdr: command.NewAbsCmdr(ctx, cfg.General(), m, envparser.ExecutorEnv{}, testSysLog),
		Cfg:     cfg,
	}).CreateBaseShell(cur.HomeDir)

	tests := map[string]bashTests{
		"exit 0 command, successfully executed": {
			s:      bash,
			script: `echo "hello" && exit 0`,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "hello")
			},
		},
		"exit 1 command, successfully executed": {
			s:      bash,
			script: `echo "hello" && exit 1`,
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
			assertString: func(t *testing.T, s string) {
				assert.Contains(t, s, "hello")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := tt.s.ReturnOutput(tt.script)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertString != nil {
				tt.assertString(t, got)
			}
		})
	}
}

func TestFactory_CreateBaseShell(t *testing.T) {
	ctx := context.TODO() // Not utilized in tests.
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().General().Return(configure.General{
		Shell:       "/bin/bash",
		KillTimeout: "5s",
	}).AnyTimes()
	cfg.EXPECT().Auth().Return(configure.Auth{}).AnyTimes()

	tests := map[string]bashTests{
		"valid base bash command generated": {
			absCmdr: command.NewAbsCmdr(ctx, cfg.General(), m, envparser.ExecutorEnv{}, testSysLog),
			assertShell: func(t *testing.T, s *shell) {
				assert.NotNil(t, s.cmd)
				ep, _ := exec.LookPath("env")
				assert.Equal(t, ep, s.cmd.Path)
			},
		},
		"base command generated, no Bash profiled enabled": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + "JACAMAR_NO_BASH_PROFILE": "true",
			},
			absCmdr: command.NewAbsCmdr(ctx, cfg.General(), m, envparser.ExecutorEnv{}, testSysLog),
			assertShell: func(t *testing.T, s *shell) {
				assert.NotNil(t, s.cmd)
				// Specifying --noprofile will result in no PATH.
				out, err := s.ReturnOutput("env | grep PATH | echo pass")
				assert.NoError(t, err)
				assert.Equal(t, "pass\n", out)
			},
		},
		"base command generated, source /etc/profile only": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + "JACAMAR_ETC_PROFILE_ONLY": "true",
			},
			absCmdr: command.NewAbsCmdr(ctx, cfg.General(), m, envparser.ExecutorEnv{}, testSysLog),
			assertShell: func(t *testing.T, s *shell) {
				assert.NotNil(t, s.cmd)
				// Source /etc/project will establish a PATH.
				out, err := s.ReturnOutput("env | grep PATH")
				assert.NoError(t, err)
				assert.NotEmpty(t, out)
			},
		},
		"bash shell with Jacamar shell variable": {
			absCmdr: command.NewAbsCmdr(ctx, cfg.General(), m, envparser.ExecutorEnv{}, testSysLog),
			assertShell: func(t *testing.T, s *shell) {
				assert.NotNil(t, s.cmd)
				assert.Contains(t, s.cmd.Args, envkeys.JacamarShell+"=1")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for key, value := range tt.targetEnv {
				t.Setenv(key, value)
			}

			f := Factory{
				AbsCmdr: tt.absCmdr,
				Cfg:     cfg,
			}
			s := f.CreateBaseShell("/home/user")

			if tt.assertShell != nil {
				tt.assertShell(t, s)
			}
		})
	}
}

func Test_MiscInterface(t *testing.T) {
	ctx := context.TODO() // Not utilized in tests.
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	cfg := mockGenConfig(ctrl)

	s := &shell{
		abs: command.NewAbsCmdr(ctx, cfg.General(), m, envparser.ExecutorEnv{}, testSysLog),
		cmd: nil,
	}
	s.build("/usr/bin/env", "-i", "--login")

	t.Run("updated command directory", func(t *testing.T) {
		s.CommandDir("/new")
		assert.Equal(t, s.cmd.Dir, "/new")
	})
	t.Run("append env", func(t *testing.T) {
		s.AppendEnv([]string{"HELLO=WORLD"})
		assert.Contains(t, s.cmd.Env, "HELLO=WORLD")
	})
	t.Run("check sigterm received", func(t *testing.T) {
		got := s.SigtermReceived()
		assert.False(t, got)
	})
	t.Run("request context", func(t *testing.T) {
		got := s.RequestContext()
		assert.Nil(t, got.Err())
	})
	t.Run("request copied command", func(t *testing.T) {
		got := s.CopiedCmd()
		assert.NotNil(t, got)
		assert.Equal(t, "/usr/bin/env", got.Path)
	})
	t.Run("modify command", func(t *testing.T) {
		s.ModifyCmd("bash")
		assert.NotNil(t, s.cmd)
		assert.Equal(t, "/bin/bash", s.cmd.Path)
	})
}
