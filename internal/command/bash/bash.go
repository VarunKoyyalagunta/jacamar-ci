package bash

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"sync"
	"syscall"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
)

type shell struct {
	abs   *command.AbstractCommander
	sig   command.Signaler
	cmd   *exec.Cmd
	mutex sync.Mutex
}

func (s *shell) PipeOutput(stdin string) error {
	cmd := &exec.Cmd{}
	command.CloneCmd(s.cmd, cmd)

	var stdoutBuf, stderrBuf bytes.Buffer
	cmd.Stdout = io.MultiWriter(os.Stdout, &stdoutBuf)
	cmd.Stderr = io.MultiWriter(os.Stderr, &stderrBuf)
	cmd.Stdin = bytes.NewBufferString(stdin)

	return s.abs.RunCmd(cmd, s.sig)
}

func (s *shell) ReturnOutput(stdin string) (string, error) {
	cmd := &exec.Cmd{}
	command.CloneCmd(s.cmd, cmd)

	var b bytes.Buffer
	cmd.Stdout = &b
	cmd.Stderr = &b
	cmd.Stdin = bytes.NewBufferString(stdin)

	err := s.abs.RunCmd(cmd, s.sig)

	var sb strings.Builder
	sb.Write(b.Bytes())

	return sb.String(), err
}

func (s *shell) CommandDir(dir string) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	s.cmd.Dir = dir
}

func (s *shell) RequestContext() context.Context {
	return s.abs.SignalContext()
}

func (s *shell) SigtermReceived() bool {
	return s.abs.SigtermReceived()
}

func (s *shell) AppendEnv(e []string) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	s.cmd.Env = append(s.cmd.Env, e...)
}

func (s *shell) CopiedCmd() *exec.Cmd {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	cmd := &exec.Cmd{}
	command.CloneCmd(s.cmd, cmd)

	return cmd
}

func (s *shell) ModifyCmd(name string, arg ...string) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	s.cmd.Path = name
	s.cmd.Args = append([]string{name}, arg...)
	if filepath.Base(name) == name {
		if lp, err := exec.LookPath(name); err == nil {
			s.cmd.Path = lp
		}
	}
}

func (s *shell) build(name string, arg ...string) {
	/* #nosec */
	// launching subprocess with variables required
	cmd := exec.Command(name, arg...)
	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
	s.cmd = cmd
}

// Factory is used to defined requirements and interface to all related methods.
type Factory struct {
	AbsCmdr *command.AbstractCommander
	Cfg     configure.Configurer
}

// CreateBaseShell generates a shell that is targeted at job level command/script execution.
// Upon successful creation a command ($ env -i HOME=? bash ?) will be defined for future
// runs, via stdin. Runs are always presented with a completely clean bash login shell.
// It is expected that this is invoked as part of the Jacamar application.
func (f Factory) CreateBaseShell(auHome string) *shell {
	s := &shell{
		abs: f.AbsCmdr,
		sig: command.SystemSignals{},
		cmd: nil,
	}

	// Override and enforce default behaviors.
	s.abs.KillTimeout = 0 * time.Second

	s.build(
		"/usr/bin/env",
		"-i",
		fmt.Sprintf("HOME=%s", auHome),
		fmt.Sprintf("%s=1", envkeys.JacamarShell),
		command.IdentifyShell(f.Cfg.General()),
		"--login",
	)

	return s
}
