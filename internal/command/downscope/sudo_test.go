package downscope

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

func TestFactory_CreateSudo(t *testing.T) {
	ctx := context.TODO() // Not utilized in tests.
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().General().Return(configure.General{
		Shell:       "/bin/bash",
		KillTimeout: "5s",
	}).AnyTimes()
	cfg.EXPECT().Auth().Return(configure.Auth{
		JacamarPath: "/opt/jacamar/bin/jacamar",
	}).AnyTimes()

	f := Factory{
		AbsCmdr:  command.NewAbsCmdr(ctx, cfg.General(), m, envparser.ExecutorEnv{}, testSysLog),
		SysLog:   testSysLog,
		Cfg:      cfg,
		AuthUser: mockAuthWorkingID(ctrl),
		Concrete: arguments.ConcreteArgs{
			Prepare: &arguments.PrepareCmd{},
		},
	}

	t.Run("sudo shell created", func(t *testing.T) {
		s := f.CreateSudoShell()
		assert.NotNil(t, s.cmd)
		assert.Equal(t, s.cmd.Args, []string{
			"sudo", "-E", "-u", "user", "--",
			command.DefaultJacamarBin + "/jacamar", "--no-auth", "prepare",
		})
		assert.Equal(t, command.DefaultJacamarBin, s.cmd.Dir)
	})
}
