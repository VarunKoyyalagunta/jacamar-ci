package downscope

import (
	"os"

	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
)

// CreateStdShell generated a simply command interface that executes the jacamar application
// (e.g., /opt/jacamar/bin/jacamar --no-auth ...) within the namespace of the current user.
// It is strictly expected that this is only utilized  as part of a jacamar-auth application
// where the "none" downscope is specified.
func (f Factory) CreateStdShell() *shell {
	s := &shell{
		abs: f.AbsCmdr,
		sig: command.SystemSignals{
			OptionalLog: f.SysLog,
		},
		cmd: nil,
	}

	name, args := command.JacamarCmd(f.Cfg.Auth(), f.Concrete)
	s.build(name, args...)
	s.cmd.Env = os.Environ()
	s.CommandDir(f.identifyWorkDir())

	return s
}
