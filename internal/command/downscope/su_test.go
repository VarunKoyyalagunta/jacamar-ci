package downscope

import (
	"context"
	"runtime"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

func TestFactory_CreateSu_Sudo(t *testing.T) {
	if runtime.GOOS != "linux" {
		t.Skip("tests only for Linux")
	}

	ctx := context.TODO() // Not utilized in tests.
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().General().Return(configure.General{
		Shell:       "/bin/bash",
		KillTimeout: "5s",
	}).AnyTimes()
	cfg.EXPECT().Auth().Return(configure.Auth{
		JacamarPath: "/bin",
	}).AnyTimes()

	f := Factory{
		AbsCmdr:  command.NewAbsCmdr(ctx, cfg.General(), m, envparser.ExecutorEnv{}, testSysLog),
		SysLog:   testSysLog,
		Cfg:      cfg,
		AuthUser: mockAuthWorkingID(ctrl),
		Concrete: arguments.ConcreteArgs{
			Prepare: &arguments.PrepareCmd{},
		},
	}

	t.Run("su shell created", func(t *testing.T) {
		s := f.CreateSuShell()
		assert.NotNil(t, s.cmd)
		assert.Equal(t, []string{
			"su", "user", "-m", "-s", "/bin/bash", "--pty",
			"-c", "/bin/jacamar --no-auth prepare",
		}, s.cmd.Args)
		assert.Equal(t, "/bin", s.cmd.Dir)
	})
}
