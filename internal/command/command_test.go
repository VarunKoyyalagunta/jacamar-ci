package command

import (
	"context"
	"io"
	"os"
	"os/exec"
	"reflect"
	"syscall"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

var (
	orgPath string
	// defSysLog is a default logrus logger to avoid panic.
	defSysLog *logrus.Entry
)

func init() {
	orgPath = os.Getenv("PATH")

	ll := logrus.New()
	ll.Out = io.Discard
	defSysLog = logrus.NewEntry(ll)
}

func TestNewAbsCmdr(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Warn(gomock.Eq(
		"Invalid time duration in configuration (KillTimeout), defaults to 30s.",
	))

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().General().Return(configure.General{
		KillTimeout: "bad-time",
	})

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	ll := logrus.New()
	ll.Out = io.Discard
	sysLog := logrus.NewEntry(ll)

	t.Run("enforce new abstract commander default behavior", func(t *testing.T) {
		a := NewAbsCmdr(ctx, cfg.General(), m, envparser.ExecutorEnv{}, sysLog)

		assert.False(t, a.NotifyTerm, "NotifyTerm expected default is false")
		assert.False(t, a.TermCaptured, "TermCaptured expected default is false")

		a.EnableNotifyTerm()
		assert.True(t, a.NotifyTerm, "NotifyTerm expected to be changed")

		cancel()
	})
}

func TestCloneCmd(t *testing.T) {
	cmd := exec.Command("bash", "test")
	cmd.Env = append(cmd.Env, "foo=bar")
	cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}
	cmd.SysProcAttr.Credential = &syscall.Credential{
		Uid: 1,
		Gid: 1,
	}

	newCmd := &exec.Cmd{}

	tests := []struct {
		name string
		src  *exec.Cmd
		dest *exec.Cmd
	}{
		{
			name: "nil src pointer provided",
			src:  nil,
			dest: newCmd,
		}, {
			name: "copying standard command",
			src:  cmd,
			dest: newCmd,
		}, {
			name: "nil dest pointer provided",
			src:  cmd,
			dest: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			CloneCmd(tt.src, tt.dest)

			if tt.dest != nil && tt.src != nil {
				assert.True(t, reflect.DeepEqual(tt.src, tt.dest))
			}
		})
	}
}

func TestAbstractCommander_RunCmd(t *testing.T) {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	tests := map[string]signalTests{
		"run successful command": {
			a: &AbstractCommander{
				sigCtx:    ctx,
				cancelCtx: cancel,
				sysLog:    defSysLog,
			},
			cmd: exec.Command("date"),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"run unsuccessful command": {
			a: &AbstractCommander{
				sigCtx:    ctx,
				cancelCtx: cancel,
				sysLog:    defSysLog,
			},
			cmd: exec.Command("exit", "1"),
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			sig := generateSig(tt.mockSignaler)

			err := tt.a.RunCmd(tt.cmd, sig)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}

	cancel() // cleanup
}

func Test_IdentifyShell(t *testing.T) {
	t.Run("verify default Bash shell", func(t *testing.T) {
		// Remove PATH to ensure no bash found
		_ = os.Unsetenv("PATH")
		got := IdentifyShell(configure.General{})
		assert.Equal(t, defaultShell, got)
	})
	t.Run("configured shell", func(t *testing.T) {
		// Remove PATH to ensure no bash found
		got := IdentifyShell(configure.General{
			Shell: "/custom/bin/bash",
		})
		assert.Equal(t, "/custom/bin/bash", got)
	})
}

func TestJacamarCmd(t *testing.T) {
	tmpPath := t.TempDir()
	defer func() { _ = os.Setenv("PATH", orgPath) }()

	tests := map[string]struct {
		auth     configure.Auth
		c        arguments.ConcreteArgs
		mockPath func()
		name     string
		args     []string
	}{
		"no configuration, run": {
			mockPath: func() {
				_ = os.Setenv("PATH", orgPath)
			},
			auth: configure.Auth{},
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{
					Script: "/file.bash",
					Stage:  "step_script",
				},
			},
			// Defaults back executable location which will be a temporary location.
			name: "/jacamar",
			args: []string{"--no-auth", "run", "env-script", "step_script"},
		},
		"jacamar path defined, cleanup": {
			auth: configure.Auth{
				JacamarPath: "/bin",
			},
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{
					Configuration: "/dir/config.toml",
				},
			},
			name: "/bin/jacamar",
			args: []string{"--no-auth", "cleanup", "--configuration", "/dir/config.toml"},
		},
		"jacamar on path, prepare": {
			mockPath: func() {
				_ = os.Mkdir(tmpPath+"/bin", 0700)
				file, _ := os.Create(tmpPath + "/bin/jacamar")
				_ = os.Chmod(file.Name(), 0700)
				_ = os.Setenv("PATH", orgPath+":"+tmpPath+"/bin")
			},
			auth: configure.Auth{},
			c: arguments.ConcreteArgs{
				Prepare: &arguments.PrepareCmd{},
			},
			name: "/jacamar",
			args: []string{"--no-auth", "prepare"},
		},
		"binary defined, cleanup": {
			auth: configure.Auth{
				// In this test case we don't care if the binary is accurate, only
				// that we can identify it is not a directory.
				JacamarPath: "/bin/date",
			},
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{
					Configuration: "/dir/config.toml",
				},
			},
			name: "/bin/date",
			args: []string{"--no-auth", "cleanup", "--configuration", "/dir/config.toml"},
		},
		"non-existent binary defined, cleanup": {
			auth: configure.Auth{
				// In this test case we don't care if the binary is accurate, only
				// that we can identify it is not a directory.
				JacamarPath: "/dir/doesnt/exist/bin/jacamar",
			},
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{
					Configuration: "/dir/config.toml",
				},
			},
			name: "/dir/doesnt/exist/bin/jacamar",
			args: []string{"--no-auth", "cleanup", "--configuration", "/dir/config.toml"},
		},
		"jacamar path defined, signal": {
			auth: configure.Auth{
				JacamarPath: "/bin",
			},
			c: arguments.ConcreteArgs{
				Signal: &arguments.SignalCmd{
					Signal: "SIGTERM",
					PID:    "1234",
				},
			},
			name: "/bin/jacamar",
			args: []string{"--no-auth", "signal", "SIGTERM", "1234"},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.mockPath != nil {
				tt.mockPath()
			}

			got, got1 := JacamarCmd(tt.auth, tt.c)

			assert.Contains(t, got, tt.name)

			if !reflect.DeepEqual(got1, tt.args) {
				t.Errorf("JacamarCmd() args = %v, want %v", got1, tt.args)
			}
		})
	}
}

func TestNoOutputCmd(t *testing.T) {
	tmpFile := t.TempDir() + "/test"

	tests := map[string]struct {
		command      string
		arg          []string
		assertResult func(*testing.T)
	}{
		"failed command": {
			command: "exit",
			arg: []string{
				"2",
			},
		},
		"valid command": {
			command: "touch",
			arg: []string{
				tmpFile,
			},
			assertResult: func(*testing.T) {
				_, err := os.Stat(tmpFile)
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			NoOutputCmd(tt.command, tt.arg...)

			if tt.assertResult != nil {
				tt.assertResult(t)
			}
		})
	}
}

func TestAbstractCommander_SignalContext(t *testing.T) {
	t.Run("expected signal context returned", func(t *testing.T) {
		ctx := context.Background()
		ctx, cancel := context.WithCancel(ctx)
		defer cancel()

		a := &AbstractCommander{
			sigCtx: ctx,
			sysLog: defSysLog,
		}
		got := a.SignalContext()

		assert.NotNil(t, got)
		assert.Equal(t, got, ctx)
	})
}

func TestAbstractCommander_SigtermReceived(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	t.Run("expected sigterm returned", func(t *testing.T) {
		a := &AbstractCommander{
			TermCaptured: false,
			sysLog:       defSysLog,
		}
		got := a.SigtermReceived()
		assert.False(t, got)

		a.CaptureSigterm()

		got = a.SigtermReceived()
		assert.True(t, got)
	})
	t.Run("SIGTERM optionally logged", func(t *testing.T) {
		err := SystemSignals{
			OptionalLog: defSysLog,
		}.SIGTERM(999999)
		assert.Error(t, err)
	})
	t.Run("SIGKILL optionally logged", func(t *testing.T) {
		err := SystemSignals{
			OptionalLog: defSysLog,
		}.SIGKILL(999999)
		assert.Error(t, err)
	})
}
