package command

import (
	"os"
	"os/exec"
	"os/signal"
	"syscall"
	"time"

	"github.com/sirupsen/logrus"
)

// Signaler interface provides system level interaction with any sub-process spawned
// by jacamar-auth / jacamar based upon the configuration.
type Signaler interface {
	SIGTERM(pid int) error
	SIGKILL(pid int) error
}

// MonitorSignal continuously monitors for SIGTERM until either it is
// encountered of the channel closed.
func (a *AbstractCommander) MonitorSignal() {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGTERM)

	// We should only attempt to cancel context in cases where SIGTERM is encountered
	// to avoid attempting to clean up an already finished task.
	select {
	case <-sig:
		a.CaptureSigterm()
		a.cancelCtx()
	}
}

// MonitorTermination associates a command with the process of awaiting an identified
// termination signal. If encountered the commanded with be properly terminated.
func (a *AbstractCommander) MonitorTermination(cmd *exec.Cmd, cmdErr chan error, sig Signaler) {
	if cmd == nil {
		return
	}

	select {
	case <-cmdErr:
	case <-a.sigCtx.Done():
		// We treat timeout context similar to capturing SIGTERM and expect
		// related shutdown to occur. Enforcing TermCapture is true will do this.
		a.CaptureSigterm()
		a.manageTermination(cmd.Process, sig)
	}
}

func (a *AbstractCommander) manageTermination(proc *os.Process, sig Signaler) {
	if proc == nil {
		return
	}

	a.sysLog.Info("SIGTERM captured, initiating job termination")

	if a.NotifyTerm {
		notifyProcess(proc, a.sysLog, sig)
		time.Sleep(a.KillTimeout) // Wait before killing process.
	}
	killProcess(proc, a.sysLog, sig)
}

func notifyProcess(proc *os.Process, sysLog *logrus.Entry, sig Signaler) {
	if proc == nil {
		sysLog.Error("attempted SIGTERM but no PID could be identified")
		return
	}

	sysLog.Debugf("notifying process of SIGTERM: %d", proc.Pid)
	if err := sig.SIGTERM(proc.Pid); err != nil {
		sysLog.Errorf("failed to notify process %d: %v", proc.Pid, err)
		return
	}
}

func killProcess(proc *os.Process, sysLog *logrus.Entry, sig Signaler) {
	if proc == nil {
		sysLog.Error("attempted SIGKILL but no PID could be identified")
		return
	}

	sysLog.Debugf("stopping process: %d", proc.Pid)
	if err := sig.SIGKILL(proc.Pid); err != nil {
		sysLog.Errorf("failed to terminate process %d: %v", proc.Pid, err)
	}
}

type SystemSignals struct {
	OptionalLog *logrus.Entry
}

func (sys SystemSignals) SIGTERM(pid int) error {
	if sys.OptionalLog != nil {
		sys.OptionalLog.Debugf("prepared kill(2) signal: %v", syscall.SIGTERM)
	}

	return syscall.Kill(pid, syscall.SIGTERM)
}

func (sys SystemSignals) SIGKILL(pid int) error {
	if sys.OptionalLog != nil {
		sys.OptionalLog.Debugf("prepared kill(2) signal: %v", syscall.SIGKILL)
	}

	return syscall.Kill(-pid, syscall.SIGKILL)
}
