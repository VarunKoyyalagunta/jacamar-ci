package auth

import (
	"context"
	"io"
	"os"
	"os/user"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/cmd/preparations"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/internal/errorhandling"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_authuser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

type authTests struct {
	ae        *abstracts.Executor
	c         arguments.ConcreteArgs
	opt       configure.Auth
	targetEnv map[string]string

	auth *mock_authuser.MockAuthorized
	msg  *mock_logging.MockMessenger

	assertError    func(*testing.T, error)
	assertAbstract func(*testing.T, *abstracts.Executor)
	assertDir      func(*testing.T, string)
}

var (
	workingCfgEnv map[string]string
)

func init() {
	// override initialized exit behaviors
	sysExit = func() {}
	buildExit = func() {}

	workingCfgEnv = map[string]string{
		"TRUSTED_CI_CONCURRENT_PROJECT_ID":       "1",
		"TRUSTED_CI_JOB_ID":                      "123",
		"TRUSTED_CI_JOB_TOKEN":                   "JoBt0k3n",
		"TRUSTED_CI_RUNNER_SHORT_TOKEN":          "abc",
		"TRUSTED_CI_BUILDS_DIR":                  "/gitlab/builds",
		"TRUSTED_CI_CACHE_DIR":                   "/gitlab/cache",
		envkeys.UserEnvPrefix + envkeys.CIJobJWT: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJoZWxsbyI6IndvbHJkIn0.sm0r7FOR-HZe3FhWcTnYq-ZiMMNRXY03fKH8w298WeE",
		"TRUSTED_CI_SERVER_URL":                  "gitlab.url",
	}
}

func Test_establishAbstract(t *testing.T) {
	var tarErr errorhandling.ObfuscatedErr

	ctx := context.Background()
	cancelCtx, cancelFun := context.WithCancel(ctx)
	defer cancelFun()

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	m := mock_logging.NewMockMessenger(ctrl)

	tests := map[string]authTests{
		"config_exec - failed to establish job context": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{
					Configuration: "/missing/file/config.toml",
				},
			},
			msg: m,
			assertError: func(t *testing.T, err error) {
				assert.True(t, errorhandling.IsObfuscatedErr(err, &tarErr), "expect obfuscated error type")
			},
		},
		"config_exec - failed to authorize user": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{
					Configuration: "../../../test/testdata/auth_test_config.toml",
				},
			},
			msg:       m,
			targetEnv: workingCfgEnv,
			assertError: func(t *testing.T, err error) {
				assert.True(t, errorhandling.IsObfuscatedErr(err, &tarErr), "expect obfuscated error type")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			_, prep, err := establishAbstract(cancelCtx, tt.c, tt.msg)

			if tt.assertAbstract != nil {
				tt.assertAbstract(t, prep.AbsExec)
			}
			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_rootDirManagement(t *testing.T) {
	cur, _ := user.Current()
	if cur.Uid != "0" {
		t.Skip("rootDirManagement tests must be run as root")
	}

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	// create new directory
	newDir := t.TempDir()
	create := mock_authuser.NewMockAuthorized(ctrl)
	create.EXPECT().CIUser().Return(authuser.UserContext{
		BaseDir: newDir,
		UID:     0,
		GID:     0,
	}).AnyTimes()

	// chmod on existing directory
	chmodDir := t.TempDir()
	_ = os.Chmod(chmodDir, 0770)
	existing := mock_authuser.NewMockAuthorized(ctrl)
	existing.EXPECT().CIUser().Return(authuser.UserContext{
		BaseDir: chmodDir,
		UID:     0,
		GID:     0,
	}).AnyTimes()

	tests := map[string]authTests{
		"create new directory": {
			auth: create,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertDir: func(t *testing.T, dir string) {
				info, err := os.Stat(dir)
				assert.NoError(t, err, "unable to stat directory")
				assert.Equal(t, "drwx------", info.Mode().String())
			},
		},
		"chmod on existing directory": {
			auth: existing,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertDir: func(t *testing.T, dir string) {
				info, err := os.Stat(dir)
				assert.NoError(t, err, "unable to stat directory")
				assert.Equal(t, "drwx------", info.Mode().String())
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := rootDirManagement(tt.auth)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertDir != nil {
				tt.assertDir(t, tt.auth.CIUser().BaseDir)
			}
		})
	}
}

func Test_exitPanic(t *testing.T) {
	sysExit = func() { return }

	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	configMsg := mock_logging.NewMockMessenger(ctrl)
	configMsg.EXPECT().Stderr(
		gomock.Eq("Error encountered during job: %s"),
		gomock.Eq("unexpected fatal error (refer to system logs for additional details)"),
	)

	runMsg := mock_logging.NewMockMessenger(ctrl)
	runMsg.EXPECT().Warn(
		gomock.Eq("Error encountered during job: %s"),
		gomock.Eq("unexpected fatal error (refer to system logs for additional details)"),
	)

	tests := map[string]struct {
		c   arguments.ConcreteArgs
		msg logging.Messenger
	}{
		"panic encountered during config_exec": {
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			msg: configMsg,
		},
		"panic encountered during run_exec": {
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{},
			},
			msg: runMsg,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			defer exitPanic(tt.c, tt.msg)
			panic(1)
		})
	}
}

func Test_stagePrep(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	msg := mock_logging.NewMockMessenger(ctrl)
	msg.EXPECT().Stderr("Invalid runner version detected, minimum supported: 14.1").Times(2)

	tests := map[string]struct {
		c         arguments.ConcreteArgs
		msg       logging.Messenger
		targetEnv map[string]string
	}{
		"valid runner version during config": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.CIRunnerVer: "14.1.0",
			},
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			msg: msg,
		},
		"invalid runner version during config": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.CIRunnerVer: "13.8.0",
			},
			c: arguments.ConcreteArgs{
				Config: &arguments.ConfigCmd{},
			},
			msg: msg,
		},
		"cleanup stage, invalid runner versions": {
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + envkeys.CIRunnerVer: "13.8.0",
			},
			c: arguments.ConcreteArgs{
				Cleanup: &arguments.CleanupCmd{},
			},
			msg: msg,
		},
		"run stage, do nothing": {
			c: arguments.ConcreteArgs{
				Run: &arguments.RunCmd{},
			},
			msg: msg,
		},
		"prepare stage, do nothing": {
			c: arguments.ConcreteArgs{
				Prepare: &arguments.PrepareCmd{},
			},
			msg: msg,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for v, k := range tt.targetEnv {
				t.Setenv(k, v)
			}

			sysExit = func() { return }

			stagePrep(tt.c, tt.msg)
		})
	}
}

func Test_cleanupCfgCheck(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	ll := logrus.New()
	ll.Out = io.Discard
	sysLog := logrus.NewEntry(ll)

	missKey := mock_logging.NewMockMessenger(ctrl)
	missKey.EXPECT().Stderr("unrecognized key(s) detected in configuration ([general.custom_data_dirs])")

	tests := map[string]struct {
		msg  logging.Messenger
		file string
	}{
		"undecoded keys encountered": {
			file: "../../../test/testdata/configs/invalid_decoder.toml",
			msg:  missKey,
		},
		"valid auth": {
			file: "../../test/testdata/configs/valid_auth.toml",
			msg:  mock_logging.NewMockMessenger(ctrl),
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			cleanupCfgCheck(sysLog, tt.msg, tt.file)
		})
	}
}

func Test_mngExec(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	buildExit = func() {}
	sysExit = func() {}

	ll := logrus.New()
	ll.Out = io.Discard

	t.Run("unsupported concrete arguments", func(t *testing.T) {
		prep := preparations.Factory{
			Args: arguments.ConcreteArgs{},
			AbsExec: &abstracts.Executor{
				Msg: func() *mock_logging.MockMessenger {
					m := mock_logging.NewMockMessenger(ctrl)
					m.EXPECT().Stderr(gomock.Any(), gomock.Eq("Invalid subcommand - See $ jacamar-auth --help")).Times(1)
					return m
				}(),
			},
			SysLog: logrus.NewEntry(ll),
		}

		mngExec(nil, prep)
	})
}
