// Package preparations realizes universal actions taken by different applications / sub-commands
// to prepare for an upcoming CI job. Stage specific triggers to the appropriate execution layer
// are maintained here, any further distinctions in workflows should be realized in the appropriate
// command package.
package preparations

import (
	"context"
	"errors"
	"fmt"
	"os"

	"github.com/sirupsen/logrus"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser/fullauth"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser/noauth"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command/bash"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command/downscope"
	"gitlab.com/ecp-ci/jacamar-ci/internal/verifycaps"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

var (
	estValid   func(string, configure.Options, envparser.ExecutorEnv) (authuser.Validators, error)
	isTerminal func() bool
)

func init() {
	estValid = authuser.EstablishValidators
	isTerminal = verifycaps.IsTerminal
}

type Factory struct {
	AbsExec *abstracts.Executor
	SysLog  *logrus.Entry
	Args    arguments.ConcreteArgs
}

// StdError output provided error message using mechanism appropriate to the current stage.
func StdError(c arguments.ConcreteArgs, message string, msg logging.Messenger, exit func()) {
	prefix := "Error encountered during job: %s"
	if c.Run != nil || c.Prepare != nil {
		msg.Warn(prefix, message)
	} else {
		msg.Stderr(prefix, message)
	}
	exit()
}

func JobContext(
	c arguments.ConcreteArgs,
	exit func(),
) (*abstracts.Executor, *logrus.Entry, error) {
	cfg, err := newConfigurer(c)
	if err != nil {
		return nil, nil, fmt.Errorf("unable to establish Configurer: %w", err)
	}

	env, err := newExecutorEnv(stateReq(c), c, cfg.Options())
	if err != nil {
		return nil, nil, fmt.Errorf("unable to identify ExecutorEnv: %w", err)
	}

	ae := &abstracts.Executor{
		Cfg: cfg,
		Env: env,
	}

	stageConfig(ae, c)
	execOptions(ae, c, exit)

	var sysLog *logrus.Entry
	sysLog, err = logging.EstablishLogger(ae.Stage, ae.Cfg.Options(), ae.Env.RequiredEnv)

	if c.Config != nil && sysLog != nil {
		sysLog.Info(fmt.Sprintf("configuration file (%s) opened", c.Config.Configuration))
	}

	return ae, sysLog, err
}

// NewAuthorizedUser identifies required user context. It is required that the JobContext has already
// been successfully ran.
func (f Factory) NewAuthorizedUser() (auth authuser.Authorized, err error) {
	valid, err := estValid(f.AbsExec.Stage, f.AbsExec.Cfg.Options(), f.AbsExec.Env)
	if err != nil {
		return
	}

	if f.Args.NoAuth {
		auth, err = noauth.Factory{
			Env:   f.AbsExec.Env,
			Opt:   f.AbsExec.Cfg.Options(),
			Valid: valid,
			Msg:   f.AbsExec.Msg,
		}.EstablishUser()
	} else {
		auth, err = fullauth.Factory{
			SysLog: f.SysLog,
			Env:    f.AbsExec.Env,
			Opt:    f.AbsExec.Cfg.Options(),
			Valid:  valid,
			Msg:    f.AbsExec.Msg,
		}.EstablishUser()
	}

	return
}

func newConfigurer(c arguments.ConcreteArgs) (configure.Configurer, error) {
	if c.Config != nil {
		return configure.NewConfig(c.Config.Configuration)
	} else if len(os.Getenv(configure.EnvVariable)) == 0 && c.Cleanup != nil {
		return configure.NewConfig(c.Cleanup.Configuration)
	}
	return configure.NewConfig(configure.EnvVariable)
}

func newExecutorEnv(
	stateful bool,
	c arguments.ConcreteArgs,
	opt configure.Options,
) (env envparser.ExecutorEnv, err error) {
	env, err = envparser.Fetcher(stateful, c, opt)
	if err != nil {
		return envparser.ExecutorEnv{}, fmt.Errorf(
			"unable to parse runner environment: %w", err,
		)
	}
	return
}

// stateReq determines based upon stage if job (stateful) variables should be relied upon.
func stateReq(c arguments.ConcreteArgs) bool {
	_, found := os.LookupEnv("JACAMAR_CI_BASE_DIR")
	if c.Config != nil {
		return false
	} else if c.Cleanup != nil && !found {
		return false
	}
	return true
}

// stageConfig updates the abstract Executor with details based upon stage specific arguments.
func stageConfig(ae *abstracts.Executor, c arguments.ConcreteArgs) {
	switch {
	case c.Config != nil:
		ae.Stage = "config_exec"
	case c.Prepare != nil:
		ae.Stage = "prepare_exec"
	case c.Run != nil:
		ae.Stage = c.Run.Stage
		ae.ScriptPath = c.Run.Script
	case c.Cleanup != nil:
		ae.Stage = "cleanup_exec"
	}
}

// execOptions realizes the establishment of the configuration environment variable for sharing
// the contents of a --configuration amongst stages.
func execOptions(ae *abstracts.Executor, c arguments.ConcreteArgs, exit func()) {
	req := true
	if c.Config != nil {
		req = false
	}

	ec, err := ae.Cfg.PrepareState(req)
	if err != nil {
		StdError(c,
			"unable to PrepareState for configuration (--configuration), please verify encoding",
			ae.Msg,
			exit,
		)
	}
	_ = os.Setenv(configure.EnvVariable, ec)
	ae.ExecOptions = ec
}

func (f Factory) NewCommander(ctx context.Context, auth authuser.Authorized) (command.Commander, error) {
	absCmdr := command.NewAbsCmdr(
		ctx,
		f.AbsExec.Cfg.General(),
		f.AbsExec.Msg,
		f.AbsExec.Env,
		f.SysLog,
	)

	if f.Args.NoAuth {
		return f.nonAuthLevel(absCmdr, auth), nil
	}

	return f.authorizationLevel(absCmdr, auth)
}

func (f Factory) nonAuthLevel(absCmdr *command.AbstractCommander, auth authuser.Authorized) command.Commander {
	factory := bash.Factory{
		AbsCmdr: absCmdr,
		Cfg:     f.AbsExec.Cfg,
	}

	return factory.CreateBaseShell(auth.CIUser().HomeDir)
}

func (f Factory) authorizationLevel(
	absCmdr *command.AbstractCommander,
	auth authuser.Authorized,
) (command.Commander, error) {
	absCmdr.EnableNotifyTerm()

	var cmdr command.Commander
	var err error

	factory := downscope.Factory{
		AbsCmdr:  absCmdr,
		SysLog:   f.SysLog,
		Cfg:      f.AbsExec.Cfg,
		AuthUser: auth,
		Concrete: f.Args,
	}

	target := f.AbsExec.Cfg.Auth().Downscope
	switch target {
	case "setuid":
		if isTerminal() && !factory.Cfg.Auth().DevMode {
			err = errors.New("setuid downscoping via active terminal (TTY) is not supported at this time")
		} else {
			cmdr = factory.CreateSetuidShell()
		}
	case "sudo":
		cmdr = factory.CreateSudoShell()
	case "su":
		cmdr = factory.CreateSuShell()
	case "none":
		cmdr = factory.CreateStdShell()
	default:
		err = errors.New("invalid runner configuration, ensure supported downscope is defined")
	}

	return cmdr, err
}
