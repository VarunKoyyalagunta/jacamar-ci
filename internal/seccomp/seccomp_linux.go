// Package seccomp manages bindings for libseccomp.
package seccomp

import (
	"errors"
	"fmt"
	"path/filepath"
	"plugin"

	libseccomp "github.com/seccomp/libseccomp-golang"
	"github.com/sirupsen/logrus"
	"golang.org/x/sys/unix"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/errorhandling"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
)

type Factory struct {
	Stage  string
	Opt    configure.Options
	Usr    authuser.UserContext
	SysLog *logrus.Entry
}

var pluginErr error

// Establish generates and enforces all rules for seccomp filters based upon the administratively
// controlled configuration and currently identified job context.
func (f Factory) Establish() (err error) {
	if f.Opt.Auth.Seccomp.Disabled {
		return
	}

	c := f.generate()

	if len(c.rules) == 0 && c.defaultAction != defaultBlockAct && c.pluginFile == "" {
		if !f.Opt.Auth.Seccomp.LogAllowedActions {
			// In cases where we are going ot log allowed actions the seccomp
			// filter must be put in place regardless of other rules.
			return
		}
	}

	c.logLibrary(f.SysLog)
	if err = c.enable(f.SysLog); err != nil {
		err = errorhandling.NewSeccompError(err)
	}

	return
}

func SetNoNewPrivs() (err error) {
	v, _ := unix.PrctlRetInt(
		unix.PR_GET_NO_NEW_PRIVS,
		uintptr(0),
		uintptr(0),
		uintptr(0),
		uintptr(0),
	)

	if v == 0 {
		// A return of 0 indicates no_new_privs has not been set.
		err = unix.Prctl(
			unix.PR_SET_NO_NEW_PRIVS,
			uintptr(1),
			uintptr(0),
			uintptr(0),
			uintptr(0),
		)
	}

	return
}

func (c config) logLibrary(sysLog *logrus.Entry) {
	major, minor, micro := libseccomp.GetLibraryVersion()

	sysLog.Debug(fmt.Sprintf(
		"libseccomp version: %v.%v.%v (api level: %v)",
		major,
		minor,
		micro,
		c.apiLevel,
	))
}

// enable establishes and loads appropriate filters based upon the set of rules.
// Errors generated during this should be observed and result in a system failure.
func (c config) enable(sysLog *logrus.Entry) (err error) {
	filter, err := libseccomp.NewFilter(c.defaultAction)
	if err != nil {
		return
	}

	if err = c.applyRules(filter); err != nil {
		return
	}

	if c.disableNoNewPrivs {
		if err = filter.SetNoNewPrivsBit(false); err != nil {
			return
		}
	}

	err = c.applyPlugin(filter)
	if err != nil {
		return fmt.Errorf("plugin error: %w", err)
	} else if pluginErr != nil {
		return pluginErr
	}

	c.loggingBit(filter, sysLog)

	return filter.Load()
}

func (c config) applyRules(filter *libseccomp.ScmpFilter) error {
	for _, tarRule := range c.rules {
		if tarRule.action == c.defaultAction {
			// Avoid conflicting with default actions.
			continue
		}

		callID, err := libseccomp.GetSyscallFromName(tarRule.callName)
		if err != nil {
			return err
		}

		if len(tarRule.conditions) > 0 {
			var scmpCond []libseccomp.ScmpCondition
			scmpCond, err = establishConditions(tarRule.conditions)
			if err == nil {
				err = filter.AddRuleConditional(callID, tarRule.action, scmpCond)
			}
		} else {
			err = filter.AddRule(callID, tarRule.action)
		}

		if err != nil {
			return err
		}
	}

	return nil
}

func establishConditions(conditions []condition) ([]libseccomp.ScmpCondition, error) {
	var scmpCond []libseccomp.ScmpCondition

	for _, cond := range conditions {
		sc, err := libseccomp.MakeCondition(cond.arg, cond.compare, cond.val)
		if err != nil {
			return scmpCond, err
		}
		scmpCond = append(scmpCond, sc)
	}

	return scmpCond, nil
}

func (c config) applyPlugin(filter *libseccomp.ScmpFilter) error {
	if c.pluginFile == "" {
		return nil
	}

	defer handlePanic()

	p, err := plugin.Open(filepath.Clean(c.pluginFile))
	if err != nil {
		return err
	}

	sym, err := p.Lookup("SeccompExpansion")
	if err != nil {
		return err
	}

	expFunc, ok := sym.(func(*libseccomp.ScmpFilter, string) error)
	if !ok {
		return errors.New(
			"unable to locate 'SeccompExpansion(*libseccomp.ScmpFilter, string) error'",
		)
	}

	return expFunc(filter, c.stage)
}

// handlePanic attempts to gracefully handle a panic caused by an administrative plugin
// by assigning the associated error to the pluginErr variable.
func handlePanic() {
	if r := recover(); r != nil {
		pluginErr = fmt.Errorf("panic encountered in plugin: %v", r)
	}
}

// loggingBit identifies/sets the logging bit for the current filter. Failure to do
// so, either due to error or current API level, will simply be logged as it will
// not prevent the filters from being enforced.
func (c config) loggingBit(filter *libseccomp.ScmpFilter, sysLog *logrus.Entry) {
	logBit, _ := filter.GetLogBit()
	if logBit {
		sysLog.Debug("seccomp actions being logged to audit")
	} else if c.apiLevel >= uint(3) {
		err := filter.SetLogBit(true)
		if err != nil {
			sysLog.Warn(fmt.Sprintf("failed to establish seccomp logging: %s", err.Error()))
		}
	}
}
