// Package errorhandling maintains all interfaces by which the Jacamar CI
// applications should handle specialized errors specifically formatted to
// align with the custom executor model.
//
// Example code:
//
//	err = NewAuthError(err)
//	// cleanup steps
//	MessageError(c, err, msg, sysExit)
package errorhandling

import (
	"errors"

	"gitlab.com/ecp-ci/jacamar-ci/internal/arguments"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

const (
	stderrPrefix     = "Error encountered during job: %s"
	usrPostfix       = " (refer to system logs for additional details)"
	usrJobCtx        = "unable to identify job context, please verify configuration/deployment"
	usrAuthorization = "you are currently unauthorized to use this runner"
	usrRunMechanism  = "unable to initialize runner mechanism, verify configuration/permissions"
	usrSeccomp       = "unexpected error establishing seccomp filters"
	usrPanic         = "unexpected fatal error"
	cleanCtx         = "\nPotentially caused by missing/incomplete job context, " +
		"review errors in previous stage(s) to identify accurate cause."
)

// ObfuscatedErr implements an error interface for messages that may
// need hidden (obfuscated) from the user's CI job log.
type ObfuscatedErr interface {
	error
	obfuscate()
	cleanupContext(arguments.ConcreteArgs)
	Unwrap() error
}

// AuthError and other error types contain metadata about the errors and a reference to inner errors.
type AuthError struct {
	previous error  // A reference to any wrapped errors.
	s        string // The error string.
	hidden   string // Replaces the above string if the error requires obfuscation.
	cleanCtx bool   // Adds context to errors in the cleanup stage.
}

type SeccompError AuthError
type JobCtxError AuthError
type RunMechanismError AuthError
type PanicError AuthError

func (a *AuthError) Error() string {
	return a.s
}

func (a *AuthError) obfuscate() {
	a.s = a.hidden
}

func (a *AuthError) Unwrap() error {
	return a.previous
}

func (a *AuthError) cleanupContext(c arguments.ConcreteArgs) {
	a.s += cleanupMsgCtx(c, a.cleanCtx)
}

func (a *SeccompError) Error() string {
	return a.s
}

func (a *SeccompError) obfuscate() {
	a.s = a.hidden
}

func (a *SeccompError) Unwrap() error {
	return a.previous
}

func (a *SeccompError) cleanupContext(c arguments.ConcreteArgs) {
	a.s += cleanupMsgCtx(c, a.cleanCtx)
}

func (a *JobCtxError) Error() string {
	return a.s
}

func (a *JobCtxError) obfuscate() {
	a.s = a.hidden
}

func (a *JobCtxError) Unwrap() error {
	return a.previous
}

func (a *JobCtxError) cleanupContext(c arguments.ConcreteArgs) {
	a.s += cleanupMsgCtx(c, a.cleanCtx)
}

func (a *RunMechanismError) Error() string {
	return a.s
}

func (a *RunMechanismError) obfuscate() {
	a.s = a.hidden
}

func (a *RunMechanismError) Unwrap() error {
	return a.previous
}

func (a *RunMechanismError) cleanupContext(c arguments.ConcreteArgs) {
	a.s += cleanupMsgCtx(c, a.cleanCtx)
}

func (a *PanicError) Error() string {
	return a.s
}

func (a *PanicError) obfuscate() {
	a.s = a.hidden
}

func (a *PanicError) Unwrap() error {
	return a.previous
}

func (a *PanicError) cleanupContext(c arguments.ConcreteArgs) {
	a.s += cleanupMsgCtx(c, a.cleanCtx)
}

func requireObfuscation(c arguments.ConcreteArgs) bool {
	return !c.UnobfuscatedError && (c.Cleanup == nil)
}

func cleanupMsgCtx(c arguments.ConcreteArgs, reqCtx bool) string {
	if (c.Cleanup != nil) && reqCtx {
		return cleanCtx
	} else {
		return ""
	}
}

type UserSafeErr interface {
	error
	Unwrap() error
}

type UserError struct {
	msg      string
	previous error
}

func (u *UserError) Error() string {
	return u.msg
}

func (u *UserError) Unwrap() error {
	return u.previous
}

// NewUserError represents an error that is safe to present to users and can be wrapped inside other errors.
func NewUserError(e error) *UserError {
	return &UserError{
		msg:      e.Error(),
		previous: e,
	}
}

// NewJobCtxError wraps an existing error generated during while identifying
// the provided job context (configuration or environment) with the goal
// of obfuscating information from the CI user.
func NewJobCtxError(e error) *JobCtxError {
	return &JobCtxError{
		s:        e.Error(),
		previous: e,
		hidden:   usrJobCtx + usrPostfix,
		cleanCtx: true,
	}
}

// NewAuthError wraps an existing error generated during the authorization
// process with the goal of obfuscating information from the CI user.
func NewAuthError(e error) *AuthError {
	return &AuthError{
		s:        e.Error(),
		previous: e,
		hidden:   usrAuthorization + usrPostfix,
		cleanCtx: true,
	}
}

// NewRunMechanismError wraps an existing error generated during the
// establishment of a RunMechanism with the goal of obfuscating
// information from the CI user.
func NewRunMechanismError(e error) *RunMechanismError {
	return &RunMechanismError{
		s:        e.Error(),
		previous: e,
		hidden:   usrRunMechanism + usrPostfix,
		cleanCtx: false,
	}
}

func NewSeccompError(e error) *SeccompError {
	return &SeccompError{
		s:        e.Error(),
		previous: e,
		hidden:   usrSeccomp + usrPostfix,
		cleanCtx: false,
	}
}

// NewPanicError wraps an existing error generated by recovering
// from a panicking routine.
func NewPanicError(e error) *PanicError {
	return &PanicError{
		s:        e.Error(),
		previous: e,
		hidden:   usrPanic + usrPostfix,
		cleanCtx: false,
	}
}

// MessageError produces a valid message formatted based upon the current stage and any
// command line arguments, prior to conveying a failure back to the runner.
// If the error requires obfuscation and contains a user-safe error, the user safe error will be presented.
// Otherwise, the obfuscated error will be presented.
func MessageError(
	c arguments.ConcreteArgs,
	msg logging.Messenger,
	e error,
) {
	var tmpErr ObfuscatedErr
	errPrinted := false

	if IsObfuscatedErr(e, &tmpErr) {
		if requireObfuscation(c) {
			var usrErr *UserError

			ok := errors.As(e, &usrErr)

			if ok {
				stdErr(c, usrErr.Error(), msg)
				errPrinted = true
			}

			tmpErr.obfuscate()
		}

		tmpErr.cleanupContext(c)
	}

	if !errPrinted {
		stdErr(c, e.Error(), msg)
	}
}

func stdErr(c arguments.ConcreteArgs, s string, msg logging.Messenger) {
	if c.Run != nil || c.Prepare != nil {
		msg.Warn(stderrPrefix, s)
	} else {
		msg.Stderr(stderrPrefix, s)
	}
}

func IsObfuscatedErr(err error, tarErr *ObfuscatedErr) bool {
	ok := errors.As(err, tarErr)
	return ok
}

func IsUserSafeErr(err error) bool {
	var usrErr *UserError
	ok := errors.As(err, &usrErr)
	return ok
}
