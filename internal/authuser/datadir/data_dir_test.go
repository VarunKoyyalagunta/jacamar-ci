package datadir

import (
	"fmt"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/gljobctx-go"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
)

type dirTest struct {
	buildEnv string
	cfg      configure.Options
	req      envparser.RequiredEnv
	user     *authuser.UserContext
	claims   gljobctx.Claims

	mockMsg func(*gomock.Controller) *mock_logging.MockMessenger

	assertError       func(*testing.T, error)
	assertUserContext func(*testing.T, *authuser.UserContext)
}

var testRedEnv envparser.RequiredEnv
var testRedClaims gljobctx.Claims

func init() {
	testRedEnv = envparser.RequiredEnv{
		ConcurrentID: "0",
		JobID:        "123",
		RunnerShort:  "short",
	}
	testRedClaims = gljobctx.Claims{
		ProjectPath: "group/project",
		UserLogin:   "tester",
	}
}

func qualifiedWarning(ctrl *gomock.Controller) *mock_logging.MockMessenger {
	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Warn(fmt.Sprintf(
		"Invalid %s, verify fully qualified path declared with supported keys.",
		envkeys.CustomBuildsDir,
	))
	return m
}

func noWarning(ctrl *gomock.Controller) *mock_logging.MockMessenger {
	m := mock_logging.NewMockMessenger(ctrl)
	return m
}

func TestUserContext_identifyDirectories(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	testContext := &authuser.UserContext{
		Username: "user",
		HomeDir:  "/home/user",
	}

	tests := map[string]dirTest{
		"undefined username in UserContext": {
			user:    &authuser.UserContext{},
			mockMsg: noWarning,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"incomplete user context detected",
				)
			},
		},
		"undefined homedir in UserContext": {
			user: &authuser.UserContext{
				Username: "user",
				HomeDir:  "",
			},
			mockMsg: noWarning,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"incomplete user context detected",
				)
			},
		},
		"DataDir defined for user's home, no custom builds": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir: "$HOME",
				},
			},
			req:     testRedEnv,
			claims:  testRedClaims,
			mockMsg: noWarning,
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.Equal(
					t, "/home/user/.jacamar-ci", u.BaseDir,
					"UserContext - baseDir",
				)
				assert.Equal(
					t, "/home/user/.jacamar-ci/builds/short/000", u.BuildsDir,
					"UserContext - buildsDir",
				)
				assert.Equal(
					t, "/home/user/.jacamar-ci/cache", u.CacheDir,
					"UserContext - cacheDir",
				)
				assert.Equal(
					t, "/home/user/.jacamar-ci/scripts/short/000/group/project/123", u.ScriptDir,
					"UserContext - scriptDir",
				)
			},
		},
		"DataDir defined for variable home, no custom builds": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir: "$HOME/.gitlab-runner",
				},
			},
			req:     testRedEnv,
			claims:  testRedClaims,
			mockMsg: noWarning,
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.Equal(
					t, "/home/user/.gitlab-runner", u.BaseDir,
					"UserContext - baseDir",
				)
			},
		},
		"DataDir defined for user's home, custom builds": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir:        "$HOME",
					CustomBuildDir: true,
				},
			},
			req:      testRedEnv,
			claims:   testRedClaims,
			mockMsg:  noWarning,
			buildEnv: "/custom/dir",
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.Equal(
					t, "/custom/dir/user/builds/short/000", u.BuildsDir,
					"UserContext - buildsDir",
				)
			},
		},
		"DataDir defined for target directory, no custom builds": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir: "/ci",
				},
				Auth: configure.Auth{
					RootDirCreation: true,
				},
			},
			req:     testRedEnv,
			claims:  testRedClaims,
			mockMsg: noWarning,
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.Equal(
					t, "/ci/user", u.BaseDir,
					"UserContext - baseDir",
				)
				assert.Equal(
					t, "/ci/user/builds/short/000", u.BuildsDir,
					"UserContext - buildsDir",
				)
				assert.Equal(
					t, "/ci/user/cache", u.CacheDir, "UserContext - cacheDir")
				assert.Equal(
					t, "/ci/user/scripts/short/000/group/project/123", u.ScriptDir,
					"UserContext - scriptDir",
				)
			},
		},
		"DataDir defined for target directory, custom builds": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir:        "/ci",
					CustomBuildDir: true,
				},
			},
			req:      testRedEnv,
			claims:   testRedClaims,
			mockMsg:  noWarning,
			buildEnv: "/custom/dir",
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.Equal(
					t, "/custom/dir/user/builds/short/000", u.BuildsDir,
					"UserContext - buildsDir")
			},
		},
		"DataDir not defined in configuration": {
			user:    testContext,
			mockMsg: noWarning,
			cfg: configure.Options{
				General: configure.General{},
				Auth:    configure.Auth{},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"unsupported configuration, data_dir must be defined",
				)
			},
		},
		"Custom build directory enabled but not used": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir:        "/ci",
					CustomBuildDir: true,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			req:     testRedEnv,
			claims:  testRedClaims,
			mockMsg: noWarning,
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.Equal(
					t, "/ci/user/builds/short/000", u.BuildsDir,
					"UserContext - buildsDir",
				)
			},
		},
		"Shared cache directory enabled and dataDir correctly defined": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir: "/ci",
				},
				Auth: configure.Auth{
					RootDirCreation: true,
				},
			},
			req:     testRedEnv,
			claims:  testRedClaims,
			mockMsg: noWarning,
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.Equal(
					t, "/ci/user", u.BaseDir,
					"UserContext - baseDir",
				)
				assert.Equal(
					t, "/ci/user/builds/short/000", u.BuildsDir,
					"UserContext - buildsDir",
				)
				assert.Equal(
					t, "/ci/user/cache", u.CacheDir,
					"UserContext - cacheDir",
				)
				assert.Equal(
					t, "/ci/user/scripts/short/000/group/project/123", u.ScriptDir,
					"UserContext - scriptDir",
				)
			},
		},
		"variable data directory enabled": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir:         "/test/something",
					VariableDataDir: true,
				},
				Auth: configure.Auth{},
			},
			req:     testRedEnv,
			claims:  testRedClaims,
			mockMsg: noWarning,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unsupported configuration, variable_data_dir no longer supported")
			},
		},
		"unsupported variable data directory ": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir: "/${TEST}/something",
				},
				Auth: configure.Auth{},
			},
			req:     testRedEnv,
			claims:  testRedClaims,
			mockMsg: noWarning,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unsupported key (TEST) detected in data_dir")
			},
		},
		"custom build directory with root creation": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir:        "/ci",
					CustomBuildDir: true,
				},
				Auth: configure.Auth{
					RootDirCreation: true,
				},
			},
			req:     testRedEnv,
			claims:  testRedClaims,
			mockMsg: noWarning,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unsupported configuration, root_dir_creation cannot be used with custom builds")
			},
		},
		"DataDir overridden in RunAs, no custom builds": {
			user: &authuser.UserContext{
				Username:        "user",
				HomeDir:         "/home/user",
				DataDirOverride: "/runas",
			},
			cfg: configure.Options{
				General: configure.General{
					DataDir: "$HOME",
				},
			},
			req:     testRedEnv,
			claims:  testRedClaims,
			mockMsg: noWarning,
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.Equal(
					t, "/runas/user", u.BaseDir,
					"UserContext - baseDir",
				)
				assert.Equal(
					t, "/runas/user/builds/short/000", u.BuildsDir,
					"UserContext - buildsDir",
				)
				assert.Equal(
					t, "/runas/user/cache", u.CacheDir,
					"UserContext - cacheDir",
				)
				assert.Equal(
					t, "/runas/user/scripts/short/000/group/project/123", u.ScriptDir,
					"UserContext - scriptDir",
				)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"DataDir defined for user's home, invalid custom builds": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir:        "$HOME",
					CustomBuildDir: true,
				},
			},
			req:      testRedEnv,
			claims:   testRedClaims,
			mockMsg:  qualifiedWarning,
			buildEnv: "$INVALID/custom/dir",
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.Equal(
					t, "/home/user/.jacamar-ci/builds/short/000", u.BuildsDir,
					"fallback to $HOME builds directory",
				)
			},
		},
		"custom data dir, feature flag enabled": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir:         "$HOME",
					CustomBuildDir:  true,
					FFCustomDataDir: true,
				},
			},
			req:      testRedEnv,
			claims:   testRedClaims,
			mockMsg:  noWarning,
			buildEnv: "/custom/dir",
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.Equal(
					t, "/custom/dir/user/builds/short/000", u.BuildsDir,
					"UserContext - buildsDir",
				)
				assert.Equal(
					t, "/custom/dir/user", u.BaseDir,
					"UserContext - baseDir",
				)
				assert.Equal(
					t, "/custom/dir/user/cache", u.CacheDir,
					"UserContext - cacheDir",
				)
				assert.Equal(
					t, "/custom/dir/user/scripts/short/000/group/project/123", u.ScriptDir,
					"UserContext - scriptDir",
				)
			},
		},
		"custom data dir, unexpanded variables": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir:        "$HOME",
					CustomBuildDir: true,
				},
			},
			req:      testRedEnv,
			claims:   testRedClaims,
			mockMsg:  qualifiedWarning,
			buildEnv: "/custom/dir/${TEST}",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.Equal(
					t, "/home/user/.jacamar-ci", u.BaseDir,
					"UserContext - baseDir",
				)
			},
		},
		"custom data dir, valid user var": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir:        "$HOME",
					CustomBuildDir: true,
				},
			},
			req:      testRedEnv,
			claims:   testRedClaims,
			mockMsg:  noWarning,
			buildEnv: "/project/space/$USER",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.Equal(
					t, "/project/space/user/user/builds/short/000", u.BuildsDir,
					"UserContext - buildsDir",
				)
			},
		},
		"custom data dir, valid home var": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir:        "$HOME",
					CustomBuildDir: true,
				},
			},
			req:      testRedEnv,
			claims:   testRedClaims,
			mockMsg:  noWarning,
			buildEnv: "$HOME/.runner",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.Equal(
					t, "/home/user/.runner/user/builds/short/000", u.BuildsDir,
					"UserContext - buildsDir",
				)
			},
		},
		"custom data dir, unresolved": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir:        "$HOME",
					CustomBuildDir: true,
				},
			},
			req:      testRedEnv,
			claims:   testRedClaims,
			mockMsg:  qualifiedWarning,
			buildEnv: "$HOME/.runner/..",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.Equal(
					t, "/home/user/.jacamar-ci", u.BaseDir,
					"UserContext - baseDir",
				)
			},
		},
		"custom data dir, invalid characters": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir:        "$HOME",
					CustomBuildDir: true,
				},
			},
			req:      testRedEnv,
			claims:   testRedClaims,
			mockMsg:  qualifiedWarning,
			buildEnv: "/$(USER)/.runner",
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.Equal(
					t, "/home/user/.jacamar-ci", u.BaseDir,
					"UserContext - baseDir",
				)
			},
		},
		"custom data dir, root_dir_creation": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir:        "$HOME",
					CustomBuildDir: true,
				},
				Auth: configure.Auth{
					RootDirCreation: true,
				},
			},
			req:     testRedEnv,
			claims:  testRedClaims,
			mockMsg: noWarning,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unsupported configuration, root_dir_creation cannot be used with custom builds")
			},
		},
		"DataDir defined for user's home,static": {
			user: testContext,
			cfg: configure.Options{
				General: configure.General{
					DataDir:         "$HOME",
					StaticBuildsDir: true,
				},
			},
			req: envparser.RequiredEnv{
				ConcurrentID: "0",
				JobID:        "123",
				RunnerShort:  "short",
			},
			claims:  testRedClaims,
			mockMsg: noWarning,
			assertUserContext: func(t *testing.T, u *authuser.UserContext) {
				assert.Equal(
					t, "/home/user/.jacamar-ci", u.BaseDir,
					"UserContext - baseDir",
				)
				assert.Equal(
					t, "/home/user/.jacamar-ci/static/00000123", u.BuildsDir,
					"UserContext - buildsDir",
				)
				assert.Equal(
					t, "/home/user/.jacamar-ci/cache", u.CacheDir,
					"UserContext - cacheDir",
				)
				assert.Equal(
					t, "/home/user/.jacamar-ci/scripts/short/000/group/project/123", u.ScriptDir,
					"UserContext - scriptDir",
				)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.buildEnv != "" {
				t.Setenv("CUSTOM_ENV_CUSTOM_CI_BUILDS_DIR", tt.buildEnv)
			}

			err := IdentifyDirectories(
				tt.user,
				tt.cfg,
				tt.req,
				tt.claims,
				tt.mockMsg(ctrl),
			)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertUserContext != nil {
				tt.assertUserContext(t, tt.user)
			}
		})
	}
}

func Test_resolveDir(t *testing.T) {
	t.Run("resolve all potential variables", func(t *testing.T) {
		u := &authuser.UserContext{
			Username: "name",
			HomeDir:  "/home/name",
		}

		got, err := resolveDir(u, "$HOME/$USER")
		assert.NoError(t, err)
		assert.Equal(t, "/home/name/name", got)
	})
}

func Test_customBuildsDir(t *testing.T) {
	t.Run("disallow root directory creation", func(t *testing.T) {
		err := customBuildsDir(nil,
			configure.Options{
				Auth: configure.Auth{
					RootDirCreation: true,
				},
			},
			envparser.RequiredEnv{},
			gljobctx.Claims{},
			nil,
		)

		assert.EqualError(t, err, "unsupported configuration, root_dir_creation cannot be used with custom builds")
	})
}
