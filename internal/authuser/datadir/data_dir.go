// Package datadir maintains procedures for establishing core directory structures
// the meet CI job requirements. No folders will be created or permissions verified
// during this process.
package datadir

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/ecp-ci/gljobctx-go"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
)

const (
	hiddenHomeDir = "/.jacamar-ci"
)

// IdentifyDirectories updates the appropriate directories in the UserContext (BaseDir,
// BuildsDir, CacheDir, and ScriptDir), returning an error if encountered. A fully
// validated local user context (Unix username and home directory) is required before
// invoking this method. No directories are created as part of this process and variables
// (with the exclusion of $HOME) will not be resolved and will result in errors.
func IdentifyDirectories(
	u *authuser.UserContext,
	opt configure.Options,
	req envparser.RequiredEnv,
	jwt gljobctx.Claims,
	msg logging.Messenger,
) (err error) {
	if u.HomeDir == "" || u.Username == "" {
		return fmt.Errorf("incomplete user context detected")
	}

	dataDir := opt.General.DataDir
	if u.DataDirOverride != "" {
		// Observe override value if supplied by admin earlier in process.
		dataDir = u.DataDirOverride
	}

	if err = directoryRules(opt); err != nil {
		return err
	}

	if dataDir == "$HOME" {
		u.BaseDir = homeDir(u)
	} else if strings.Contains(dataDir, "$") {
		u.BaseDir, err = resolveDir(u, dataDir)
		if err != nil {
			return err
		}
		u.BuildsDir = filepath.Clean(u.BaseDir)
	} else if dataDir != "" {
		u.BaseDir = stdDir(u, dataDir)
	} else {
		// Missing upstream context for the defined (config.toml) build/cache directory.
		return errors.New("unsupported configuration, data_dir must be defined")
	}

	u.BuildsDir = buildDir(u.BaseDir, req, opt)
	u.CacheDir = cacheDir(u.BaseDir)
	u.ScriptDir = scriptDir(u.BaseDir, req, jwt)

	if opt.General.CustomBuildDir || opt.General.FFCustomDataDir {
		err = customBuildsDir(u, opt, req, jwt, msg)
	}

	return err
}

func directoryRules(opt configure.Options) error {
	if opt.General.VariableDataDir {
		return errors.New("unsupported configuration, variable_data_dir no longer supported")
	}

	if opt.General.CustomBuildDir {
		// Root directory creation + custom directory not supported.
		if opt.Auth.RootDirCreation {
			return errors.New("unsupported configuration, root_dir_creation cannot be used with custom builds")
		}
	}

	return nil
}

func homeDir(u *authuser.UserContext) string {
	return filepath.Clean(u.HomeDir + hiddenHomeDir)
}

func resolveDir(u *authuser.UserContext, dataDir string) (string, error) {
	var err error
	mapper := func(key string) string {
		switch key {
		case "USER":
			return u.Username
		case "HOME":
			return u.HomeDir
		}

		err = fmt.Errorf("unsupported key (%s) detected in data_dir", key)
		return ""
	}

	return os.Expand(dataDir, mapper), err
}

func stdDir(u *authuser.UserContext, dataDir string) string {
	return filepath.Clean(strings.TrimSpace(dataDir) + "/" + u.Username)
}

// buildDir returns target for CI job's DefaultBuildDir:
// <base_dir>/builds/<runner-short>/<concurrent-id>.
func buildDir(
	base string,
	req envparser.RequiredEnv,
	opt configure.Options,
) string {
	if opt.General.StaticBuildsDir {
		return strings.Join([]string{base,
			"/static/", zeroPadJobID(req),
		}, "")
	}

	return strings.Join([]string{base,
		"/builds/", req.RunnerShort,
		"/", zeroPadConcurrent(req),
	}, "")
}

// cacheDir returns target for CI job's DefaultCacheDir:
// <base_dir>/cache.
func cacheDir(base string) string {
	return strings.Join([]string{base, "/cache"}, "")
}

// scriptDir returns target for CI job's DefaultCacheDir:
// <base_dir>/scripts/<project-path>/<CI_JOB_ID>.
func scriptDir(base string, req envparser.RequiredEnv, jwtClaims gljobctx.Claims) string {
	return strings.Join([]string{base,
		"/scripts/", req.RunnerShort,
		"/", zeroPadConcurrent(req),
		"/", jwtClaims.ProjectPath,
		"/", req.JobID,
	}, "")
}

// customBuildsDir updates the BuildsDir if the appropriate environment variable identified.
// If an error is encountered no changes to the UserContext are made.
func customBuildsDir(
	u *authuser.UserContext,
	opt configure.Options,
	req envparser.RequiredEnv,
	jwt gljobctx.Claims,
	msg logging.Messenger,
) error {
	// Root directory creation + custom directory not supported.
	if opt.Auth.RootDirCreation {
		return errors.New("unsupported configuration, root_dir_creation cannot be used with custom builds")
	}

	if err := realizeCustomDir(u, req, jwt, opt); err != nil {
		// Errors here just result in a warning to the user. We fall back
		// to the data_dir defined by the admin.
		msg.Warn(fmt.Sprintf(
			"Invalid %s, verify fully qualified path declared with supported keys.",
			envkeys.CustomBuildsDir,
		))
	}

	return nil
}

func realizeCustomDir(
	u *authuser.UserContext,
	req envparser.RequiredEnv,
	jwt gljobctx.Claims,
	opt configure.Options,
) error {
	dir, found, err := envparser.CustomBuildsDir()
	if !found {
		// Skip process, we fall back to configured data_dir.
		return nil
	} else if err != nil {
		// Invalid directory does not constitute a failed job and user should
		// be notified so steps can be taken in the future.
		return err
	}

	dir, err = resolveDir(u, dir)
	if err != nil {
		return err
	}

	if err = envparser.ValidateDirectory(dir); err != nil {
		// enforces fully qualified directory
		return err
	}
	dir = filepath.Clean(dir)

	base := strings.Join([]string{filepath.Clean(strings.TrimSpace(dir)), "/", u.Username}, "")
	u.BuildsDir = buildDir(base, req, opt)

	if opt.General.FFCustomDataDir {
		// observer feature flag
		u.BaseDir = base
		u.CacheDir = cacheDir(u.BaseDir)
		u.ScriptDir = scriptDir(u.BaseDir, req, jwt)
	}

	return nil
}

func zeroPadConcurrent(req envparser.RequiredEnv) string {
	cID := req.ConcurrentID
	for len(cID) < 3 {
		cID = "0" + cID
	}
	return cID
}

func zeroPadJobID(req envparser.RequiredEnv) string {
	jobID := req.JobID
	for len(jobID) < 8 {
		jobID = "0" + jobID
	}
	return jobID
}
