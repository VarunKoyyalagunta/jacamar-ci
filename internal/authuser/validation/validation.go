// Package validation maintains Jacamar's interactions with an admin defined validation
// script. These tools can be used not sure to convey pass/fail status but also
// directly influence ongoing user context leveraged in the remainder of the authorization flow.
package validation

import (
	"crypto/sha256"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/sirupsen/logrus"
	"gitlab.com/ecp-ci/gljobctx-go"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
)

// RunAsOverride represents potential values that can be returned to Jacamar to override or
// influence authorization flow behaviors beyond a simple pass/fail result.
type RunAsOverride struct {
	// Username is the valid local account that will be the target henceforth
	// for the authorization process.
	Username string `json:"username" validate:"username"`
	// DataDir overrides the target data_dir configured by the admin with a new target.
	DataDir string `json:"data_dir" validate:"directory"`
}

// RunAsValidator implements an interface for interacting with validation scripts.
type RunAsValidator interface {
	// Execute leverages an admin defined script in conjunction with the currently
	// identified job context to provide a complete set of verified override values. All
	// override values returned must be observed, barring any error encountered.
	Execute(pay gljobctx.Claims, username string, sysLog *logrus.Entry) (RunAsOverride, error)
}

// NewRunAs establishing a RunAs Validator based upon job context and admin configuration.
// If no RunAs is established by the configuration a nil Validator will be returned.
func NewRunAs(auth configure.Auth) (RunAsValidator, error) {
	if auth.RunAs.ValidationPlugin != "" {
		return nil, errors.New(
			"plugin support has been removed from Jacamar, please migrate to validation_script",
		)
	}

	if auth.RunAs.ValidationScript == "" {
		return nil, nil
	}

	target, err := getRunAsUser(
		envkeys.UserEnvPrefix + auth.RunAs.RunAsVariable,
	)
	if err != nil {
		return nil, fmt.Errorf(
			"invalid user provided account (defined by %s variable): %w",
			auth.RunAs.RunAsVariable,
			err,
		)
	}

	rc := runAsCfg{
		sha256:     auth.RunAs.SHA256,
		targetUser: target,
		env:        auth.RunAs.ValidationEnv,
	}

	rc.file = auth.RunAs.ValidationScript
	return runAsScript{
		runAsCfg: rc,
	}, nil
}

// fileStatus verifies the presence of the file, permissions, and it's checksum in advance.
func fileStatus(file, sha string) (err error) {
	file, _ = filepath.Abs(file)

	/* #nosec */
	// variable file path required
	f, err := os.Open(file)
	if err != nil {
		return fmt.Errorf("unable to open file: %w", err)
	}

	// no write to file, ignore potential error
	defer func() { _ = f.Close() }()

	if sha != "" {
		err = checksum(f, sha)
	}
	return
}

// checksum compares the sha256 of the provided file against a target.
func checksum(f *os.File, sha string) error {
	h := sha256.New()
	if _, err := io.Copy(h, f); err != nil {
		return err
	}

	check := fmt.Sprintf("%x", h.Sum(nil))
	if sha != check {
		return fmt.Errorf("invalid checksum found for file: %s", f.Name())
	}

	return nil
}

// runCommand runs the supplied command, captures the output returning and a boolean,
// false if exit status != 0 in addition to any output ([]byte).
func runCommand(cmd *exec.Cmd) (bool, []byte) {
	ret, err := cmd.CombinedOutput()
	if err != nil {
		return false, ret
	}
	return true, ret
}

func parseScriptStdout(output []byte, over interface{}) (err error) {
	if len(output) == 0 { // No stdout, skip parsing
		return
	}

	// No JSON found in stdout, assume error.
	err = json.Unmarshal(output, over)

	return
}
