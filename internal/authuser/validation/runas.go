package validation

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/sirupsen/logrus"
	"gitlab.com/ecp-ci/gljobctx-go"

	"gitlab.com/ecp-ci/jacamar-ci/internal/rules"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

type runAsCfg struct {
	file   string
	sha256 string
	// targetUser is a user proposed login (e.g. service account)
	targetUser string
	env        []string
}

type runAsScript struct {
	runAsCfg
}

// RunAsInit values are used to established user context for the upcoming authorization.
type RunAsInit struct {
	// TargetUser is a user proposed account (via the CI environment) meant to be the
	// replaced for the CurrentUser if the process is approved.
	TargetUser string
	// CurrentUser is the currently identified local user account of the CI trigger user.
	// This can differ from the JWT UserLogin depending on configuration of the authorization,
	// and should be observed when attempting to approve a local user account.
	CurrentUser string
	JobJWT      gljobctx.Claims
}

func (rs runAsScript) Execute(
	pay gljobctx.Claims,
	username string,
	sysLog *logrus.Entry,
) (over RunAsOverride, err error) {
	initializer, err := rs.runAsCfg.runAsPrep(pay, username)
	if err != nil {
		return over, err
	}

	ok, output := rs.runAsCfg.invokeRunAsScript(rs.file, initializer)
	if !ok {
		return over, fmt.Errorf("script failed - raw output: %s", output)
	}

	err = parseScriptStdout(output, &over)
	if err != nil {
		sysLog.Error(fmt.Sprintf("RunAs script raw output: %s", output))
		return over, err
	}

	sysLog.Debug(fmt.Sprintf("RunAs script raw output: %s", output))

	return finalizeOverride(initializer, over)
}

func finalizeOverride(initializer RunAsInit, over RunAsOverride) (RunAsOverride, error) {
	if over.Username == "" {
		if initializer.TargetUser != "" {
			over.Username = initializer.TargetUser
		} else {
			over.Username = initializer.CurrentUser
		}
	}

	if err := over.validator(); err != nil {
		return RunAsOverride{}, err
	}

	return over, nil
}

func (rc runAsCfg) runAsPrep(pay gljobctx.Claims, username string) (RunAsInit, error) {
	if username == "" {
		return RunAsInit{},
			errors.New("unexpected system error encountered, no current user can be identified")
	}

	err := fileStatus(rc.file, rc.sha256)
	return rc.create(pay, username), err
}

func (rc runAsCfg) create(pay gljobctx.Claims, username string) RunAsInit {
	ra := RunAsInit{
		TargetUser:  rc.targetUser,
		CurrentUser: username,
		JobJWT:      pay,
	}

	return ra
}

// getRunAsUser attempts to find a user provided name in the custom executor
// provided environment. Nonexistent key/values will not results in an error.
func getRunAsUser(key string) (username string, err error) {
	if key != "" {
		username = strings.TrimSpace(os.Getenv(key))
	}

	v := validator.New()
	_ = v.RegisterValidation("username", rules.CheckUsername)
	err = v.Var(username, "username")

	return
}

func runasEnv(initializer RunAsInit) []string {
	env := initializer.JobJWT.Environment()

	env = append(env, []string{
		"RUNAS_TARGET_USER=" + initializer.TargetUser,
		"RUNAS_CURRENT_USER=" + initializer.CurrentUser,
	}...)

	return env
}

// invokeRunAsScript executes the provided script passing it the verifier and username
// as arguments ($ script verifier username), returning if true (exit status = 0)
// and  false (!=0) along with any stdout.
func (rc runAsCfg) invokeRunAsScript(script string, initializer RunAsInit) (bool, []byte) {
	var cmd *exec.Cmd
	/* #nosec */
	// launching subprocess with variables required
	if initializer.TargetUser != "" {
		cmd = exec.Command(script, initializer.TargetUser, initializer.CurrentUser)
	} else {
		cmd = exec.Command(script, initializer.CurrentUser)
	}

	tarEnv := rc.env
	tarEnv = append(tarEnv, runasEnv(initializer)...)
	for _, val := range os.Environ() {
		// We want to avoid passing potential tokens to more process than necessary.
		if !envparser.SupportedPrefix(val) {
			tarEnv = append(tarEnv, val)
		}
	}
	cmd.Env = tarEnv

	return runCommand(cmd)
}

// validator is used to ensure that all payload values confirm to Jacamar expectations.
func (ro RunAsOverride) validator() error {
	v := validator.New()
	_ = v.RegisterValidation("username", rules.CheckUsername)

	// We only need to validate if a DataDir override is proposed.
	if ro.DataDir != "" {
		_ = v.RegisterValidation("directory", rules.CheckDirectory)
	} else {
		_ = v.RegisterValidation("directory", func(fl validator.FieldLevel) bool {
			return true
		})
	}

	return v.Struct(ro)
}
