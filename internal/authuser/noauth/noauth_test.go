package noauth

import (
	"errors"
	"os/user"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/gljobctx-go"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_gljobctx"
	tst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

func TestFactory_EstablishUser(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	jwtTest := mock_gljobctx.NewMockValidater(ctrl)
	jwtTest.EXPECT().ValidateJWT(
		gomock.Eq("jwt.failed.example"),
		gomock.Eq("failed.example"),
	).Return(gljobctx.Claims{}, errors.New("error message")).AnyTimes()
	jwtTest.EXPECT().ValidateJWT(
		gomock.Eq("jwt.success.example"),
		gomock.Eq("success.example"),
	).Return(gljobctx.Claims{
		JobID: "12345",
	}, nil).AnyTimes()

	cur, _ := authuser.CurrentUser()

	tests := map[string]struct {
		f           Factory
		mockUser    func()
		assertAuth  func(*testing.T, authuser.Authorized)
		assertError func(*testing.T, error)
	}{
		"issue identifying user": {
			mockUser: func() {
				curUsr = func() (*user.User, error) {
					return nil, errors.New("error message")
				}
			},
			assertError: tst.AssertError,
		},
		"process current user from state": {
			f: Factory{
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						Username: cur.Username,
					},
				},
			},
			mockUser: func() {
				curUsr = authuser.CurrentUser
			},
			assertError: tst.AssertNoError,
			assertAuth: func(t *testing.T, auth authuser.Authorized) {
				assert.Equal(t, auth.CIUser().Username, cur.Username)
			},
		},
		"force process current user from state error": {
			f: Factory{
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						Username: cur.Username,
					},
				},
			},
			mockUser: func() {
				curUsr = func() (*user.User, error) {
					return &user.User{
						Uid: "0",
						Gid: "gid",
					}, nil
				}
			},
			assertError: tst.AssertError,
		},
		"process current user, jwt error": {
			f: Factory{
				Valid: authuser.Validators{
					JobJWT: jwtTest,
				},
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.failed.example",
						ServerURL: "failed.example",
					},
				},
			},
			mockUser: func() {
				curUsr = authuser.CurrentUser
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unable to parse supplied CI_JOB_JWT: unable to parse supplied CI_JOB_JWT: error message")
			},
		},
		"process current user, data_dir error": {
			f: Factory{
				Valid: authuser.Validators{
					JobJWT: jwtTest,
				},
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						ServerURL: "success.example",
					},
				},
			},
			mockUser: func() {
				curUsr = authuser.CurrentUser
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unable to identify target directories: unsupported configuration, data_dir must be defined")
			},
		},
		"process current user, success": {
			f: Factory{
				Opt: configure.Options{
					General: configure.General{
						DataDir: "/ci",
					},
				},
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						ServerURL: "success.example",
					},
				},
				Valid: authuser.Validators{
					JobJWT: jwtTest,
				},
			},
			mockUser: func() {
				curUsr = authuser.CurrentUser
			},
			assertError: tst.AssertNoError,
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.mockUser != nil {
				tt.mockUser()
			}

			got, err := tt.f.EstablishUser()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertAuth != nil {
				tt.assertAuth(t, got)
			}
		})
	}
}

func Test_Authorized_Interface(t *testing.T) {
	working := targetCtx{
		usrCtx: authuser.UserContext{
			Username:  "username",
			HomeDir:   "/home/username",
			UID:       1000,
			GID:       2000,
			Groups:    []uint32{},
			BaseDir:   "/base",
			BuildsDir: "/builds",
			CacheDir:  "/cache",
			ScriptDir: "/script",
		},
		jwt: gljobctx.Claims{
			JobID: "123",
		},
	}

	t.Run("verify basic interface functionality implemented", func(tt *testing.T) {
		working.BuildState()
		assert.Equal(tt, "Running as username UID: 1000 GID: 2000\n", working.PrepareNotification())
		assert.Equal(tt, gljobctx.Claims{
			JobID: "123",
		}, working.JobJWT())
	})
}
