package fullauth

import (
	"errors"
	"os/user"
	"runtime"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/gljobctx-go"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	tst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

func Test_groupIDs(t *testing.T) {
	badUser := &user.User{}
	usr, _ := user.Current()

	tests := map[string]struct {
		usr         *user.User
		assertError func(*testing.T, error)
	}{
		"invalid user provided": {
			usr: badUser,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, `user: list groups for : invalid gid ""`)
			},
		},
		"current user (correct)": {
			usr: usr,
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			_, err := groupIDs(tt.usr)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_checkUserContext(t *testing.T) {
	tester := func() (*user.User, error) {
		return &user.User{
			Username: "tester",
		}, nil
	}

	tests := map[string]authTests{
		"no downscoping": {
			u:           &authuser.UserContext{},
			assertError: tst.AssertNoError,
		},
		"error encountered during user lookup": {
			auth: configure.Auth{
				Downscope: "setuid",
			},
			u: &authuser.UserContext{},
			mockCurUsr: func() (*user.User, error) {
				return nil, errors.New("error message")
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unable to identify service user: error message")
			},
		},
		"same username identified": {
			auth: configure.Auth{
				Downscope: "setuid",
			},
			u: &authuser.UserContext{
				Username: "tester",
				UID:      1000,
				GID:      1000,
			},
			mockCurUsr: tester,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "cannot execute as tester while authorization is enforced")
			},
		},
		"root username identified": {
			auth: configure.Auth{
				Downscope: "setuid",
			},
			u: &authuser.UserContext{
				Username: "root",
			},
			mockCurUsr: curUsr,
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "cannot execute as root while authorization is enforced")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.mockCurUsr != nil {
				curUsr = tt.mockCurUsr
			} else {
				curUsr = authuser.CurrentUser
			}

			err := checkUserContext(*tt.u, tt.auth)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestUserContext_checkLists(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	opt := buildMockFullOpt()

	t.Run("list error encountered", func(t *testing.T) {
		u := &authuser.UserContext{
			Username: currentUsr.Username,
		}

		err := Factory{
			Opt: opt,
		}.checkLists(u, currentUsr, gljobctx.Claims{})
		assert.Error(t, err)
	})

	t.Run("list error encountered", func(t *testing.T) {
		if runtime.GOOS != "linux" {
			t.Skip()
		}

		u := &authuser.UserContext{
			Username: currentUsr.Username,
		}
		opt.Auth.ShellAllowlist = []string{"/example/shell"}

		err := Factory{
			Opt: opt,
		}.checkLists(u, currentUsr, gljobctx.Claims{})
		assert.Error(t, err)
	})

	t.Run("list error encountered", func(t *testing.T) {
		jwt := gljobctx.Claims{
			PipelineSource: "unexpected",
		}

		u := &authuser.UserContext{
			Username: currentUsr.Username,
		}
		opt.Auth.PipelineSourceAllowlist = []string{"web", "push"}
		opt.Auth.ShellAllowlist = []string{}

		err := Factory{
			Opt: opt,
			Msg: func() logging.Messenger {
				m := mock_logging.NewMockMessenger(ctrl)
				m.EXPECT().Error(gomock.Any())
				return m
			}(),
		}.checkLists(u, currentUsr, jwt)
		assert.EqualError(t, err, "invalid pipeline trigger source (unexpected) detected")
	})

	t.Run("list error encountered", func(t *testing.T) {
		u := &authuser.UserContext{
			Username: currentUsr.Username,
		}

		invalidUser := &user.User{
			Uid:      "12345",
			Gid:      "54321",
			Username: "invalid",
		}

		err := Factory{
			Opt: opt,
		}.checkLists(u, invalidUser, gljobctx.Claims{})
		assert.Error(t, err)
	})
}

func TestFactory_checkBotAccount(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	errMsg := mock_logging.NewMockMessenger(ctrl)
	errMsg.EXPECT().Error("bot account token used for job, unsupported by runner").AnyTimes()

	tests := map[string]authTests{
		"none downscope type defined": {
			factory: Factory{
				Opt: configure.Options{
					Auth: configure.Auth{
						Downscope: "none",
					},
				},
			},
			jwt: gljobctx.Claims{
				UserLogin: "project_1_bot",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"allow bot account configured": {
			factory: Factory{
				Opt: configure.Options{
					Auth: configure.Auth{
						AllowBotAccounts: true,
					},
				},
			},
			jwt: gljobctx.Claims{
				UserLogin: "project_1_bot",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"standard username encountered": {
			jwt: gljobctx.Claims{
				UserLogin: "project",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"project bot account encountered": {
			factory: Factory{
				Msg: errMsg,
			},
			jwt: gljobctx.Claims{
				UserLogin: "project_123_bot",
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"group bot account encountered": {
			factory: Factory{
				Msg: errMsg,
			},
			jwt: gljobctx.Claims{
				UserLogin: "group_123_bot1",
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"standard project bot account": {
			jwt: gljobctx.Claims{
				UserLogin: "project_bot",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := tt.factory.checkBotAccount(tt.jwt)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestFactory_preUserCheckList(t *testing.T) {
	t.Run("user lookup error observed", func(t *testing.T) {
		f := Factory{}
		err := f.preUserCheckList(&authuser.UserContext{Username: "notauser"}, gljobctx.Claims{})
		assert.Error(t, err)
	})
}

func Test_establishGroups(t *testing.T) {
	t.Run("user lookup error observed", func(t *testing.T) {
		err := establishGroups(&authuser.UserContext{Username: "notauser"})
		assert.Error(t, err)
	})
}
