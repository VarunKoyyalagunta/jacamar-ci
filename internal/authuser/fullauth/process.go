package fullauth

import (
	"errors"
	"fmt"
	"os/user"
	"regexp"
	"strconv"

	"gitlab.com/ecp-ci/gljobctx-go"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser/datadir"
	"gitlab.com/ecp-ci/jacamar-ci/internal/gitwebapis"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
)

// processFlow manages all aspects of the user authorization flow, leveraging the supplied
// configuration as well as context from the custom executor. The process supporting this will
// ensure the entire authorization flow is strictly observed, as  such it is recommended to
// leverage stateful variables to store key aspects of the user context after a successful
// authorization. Any issues encountered during this process will result in an error returned.
// Though steps are taken to return a safe error externally defined scripts/syscall are made
// that may return more information than required. It is strongly advised that the error message
// presented to a CI user only inform them of the general failures and more details are logged
// by the privileged user.
func (f Factory) processFlow(jwt gljobctx.Claims) (authuser.UserContext, error) {
	u := &authuser.UserContext{}

	if err := f.validateUser(u, jwt); err != nil {
		return *u, fmt.Errorf("invalid authorization target user: %w", err)
	}

	// Root can be a valid GitLab account and should not be allowed to run while auth is
	// enabled and a non-standard downscope has been identified we enforce not using it
	// of the current service account.
	if err := checkUserContext(*u, f.Opt.Auth); err != nil {
		return *u, err
	}

	if err := datadir.IdentifyDirectories(u, f.Opt, f.Env.RequiredEnv, jwt, f.Msg); err != nil {
		return *u, fmt.Errorf("unable to identify target directories: %w", err)
	}

	if err := f.checkTokenScope(gitwebapis.DefaultClient()); err != nil {
		return *u, fmt.Errorf("failed to enforce ci_job_token_scope: %w", err)
	}

	return *u, nil
}

// processFromState updates a UserContext by leveraging the stateful variables previously established
// and provided to subsequent stages by the custom executor. No auth data is reconfirmed as part of this process.
// Errors can occur during user/group lookup.
func (f Factory) processFromState() (authuser.UserContext, error) {
	usr, err := user.Lookup(f.Env.StatefulEnv.Username)
	if err != nil {
		return authuser.UserContext{}, err
	}

	u, err := authuser.ProcessFromState(usr, f.Env.StatefulEnv)
	if err != nil {
		return authuser.UserContext{}, err
	}

	// Groups can only be established with authorization binary.
	groups, err := groupIDs(usr)
	if err != nil {
		return authuser.UserContext{}, err
	}
	u.Groups = groups

	return u, nil
}

func groupIDs(usr *user.User) (groups []uint32, err error) {
	groupids, err := usr.GroupIds()
	if err != nil {
		return
	}

	for _, sGID := range groupids {
		var iGID int
		iGID, err = strconv.Atoi(sGID)
		if err != nil {
			return
		}
		groups = append(groups, uint32(iGID))
	}

	return groups, err
}

func (f Factory) validateUser(u *authuser.UserContext, jwt gljobctx.Claims) error {
	if err := f.checkBotAccount(jwt); err != nil {
		return err
	}

	// Establishing starting/known values. These can change over the course of authorization!
	u.Username = jwt.UserLogin

	if f.Opt.Auth.ListsPreValidation {
		if err := f.preUserCheckList(u, jwt); err != nil {
			return err
		}
	}

	if f.Valid.RunAs != nil {
		if err := f.processRunAs(u, jwt); err != nil {
			return fmt.Errorf("RunAs validation: %w", err)
		}
	}

	// Check defined allowlist or blocklist (final uid before job execution).
	if err := f.preUserCheckList(u, jwt); err != nil {
		return err
	}

	return establishGroups(u)
}

// checkBotAccount verifies the JWT does not belong to a bot account, only if a
// downscoping target is configured. Users will be warned of error if encountered.
func (f Factory) checkBotAccount(jwt gljobctx.Claims) (err error) {
	if f.Opt.Auth.AllowBotAccounts || f.Opt.Auth.Downscope == "none" {
		// In these cases the use of a bot account does not present an issue
		// in any authorization level workflows.
		return
	}

	// Based upon GitLab documentation rules:
	// https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html#project-bot-users
	re := regexp.MustCompile(`(project|group)_([0-9][0-9]*)_bot([0-9]*)`)
	if re.MatchString(jwt.UserLogin) {
		err = errors.New("bot account token used for job, unsupported by runner")
		f.Msg.Error(err.Error())
	}

	return
}

func (f Factory) processRunAs(u *authuser.UserContext, jwt gljobctx.Claims) error {
	override, err := f.Valid.RunAs.Execute(jwt, u.Username, f.SysLog)
	if err != nil {
		return err
	}

	f.SysLog.Info(fmt.Sprintf("RunAs (%s) parsed successfully", f.Opt.Auth.RunAs.ValidationScript))
	f.SysLog.Debug(fmt.Sprintf("RunAs potential override values: %+v", override))

	u.Username = override.Username
	u.DataDirOverride = override.DataDir

	return nil
}

func (f Factory) preUserCheckList(u *authuser.UserContext, jwt gljobctx.Claims) error {
	target, err := user.Lookup(u.Username)
	if err != nil {
		return fmt.Errorf("failed user.Lookup, verify system expectations: %w", err)
	}

	return f.checkLists(u, target, jwt)
}

// checkLists verifies all allowlist/blocklist logic is observed. It in turn ensures
// aspects that the UserContext has been updated (username, uid, gid, and homedir only).
func (f Factory) checkLists(
	u *authuser.UserContext,
	target *user.User,
	jwt gljobctx.Claims,
) error {
	groups, err := lookupGroups(target, f.Opt.Auth)
	if err != nil {
		return err
	}

	if err = validateShell(f.Opt.Auth.ShellAllowlist, target.Uid, target.Username); err != nil {
		return err
	}

	if err = f.validatePipelineSource(jwt.PipelineSource); err != nil {
		return err
	}

	if err = validateABLists(f.Opt.Auth, target, groups); err != nil {
		return err
	}

	u.HomeDir = target.HomeDir

	return authuser.SetIntIDs(target.Uid, target.Gid, u)
}

// establishGroups within the validation works uses the authorized target user to create
// a list of groups they belong to and update the UserContext accordingly.
func establishGroups(u *authuser.UserContext) error {
	// Need to look up the currently identified username.
	usr, err := user.Lookup(u.Username)
	if err != nil {
		return fmt.Errorf("failed user.Lookup, verify system expectations: %w", err)
	}

	groups, err := groupIDs(usr)
	if err != nil {
		return err
	}
	u.Groups = groups

	return nil
}

// lookupGroups checks the local system for the declared user and group memberships.
func lookupGroups(target *user.User, auth configure.Auth) (groups []string, err error) {
	if len(auth.GroupAllowlist) > 0 || len(auth.GroupBlocklist) > 0 {
		var groupids []string
		var groupptr *user.Group

		groupids, err = target.GroupIds()
		if err != nil {
			return []string{}, fmt.Errorf(
				"unable to identify groups (usr.GroupIds) that user %s is a member of",
				target.Username,
			)
		}

		for _, gid := range groupids {
			groupptr, err = user.LookupGroupId(gid)
			if err != nil {
				return []string{}, fmt.Errorf("unable to lookup group id %s: %w", gid, err)
			}
			group := *groupptr
			groups = append(groups, group.Name)
		}
	}

	return groups, nil
}

func checkUserContext(u authuser.UserContext, auth configure.Auth) error {
	if auth.Downscope != "" {
		usr, err := curUsr()
		if err != nil {
			return fmt.Errorf("unable to identify service user: %w", err)
		}

		// We only need to enforce limitations on the user when a supported
		// downscoping mechanism is configured.
		if (u.Username == "root" || u.UID == 0 || u.GID == 0) ||
			(u.Username == usr.Username && auth.Downscope != "none") {
			return fmt.Errorf("cannot execute as %s while authorization is enforced", u.Username)
		}
	}

	return nil
}
