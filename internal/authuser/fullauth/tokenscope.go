package fullauth

import (
	"errors"
	"fmt"
	"net/url"
	"strings"

	"gitlab.com/ecp-ci/jacamar-ci/internal/gitwebapis"
)

const (
	// https://docs.gitlab.com/ee/api/jobs.html#get-job-tokens-job
	jobPath = "/api/v4/job"
)

type JobProject struct {
	JobTokenScopeEnabled bool `json:"ci_job_token_scope_enabled"`
}

type Job struct {
	Project JobProject `json:"project"`
}

func (f Factory) checkTokenScope(api gitwebapis.Client) error {
	if !f.Opt.Auth.TokenScopeEnforced {
		return nil
	}

	urlStr, err := f.jobURL()
	if err != nil {
		// In all likelihood this is difficult to encounter are early step should fail
		// on an invalid URL.
		return err
	}

	j := Job{}
	err = api.GetJSON(urlStr, map[string]string{"JOB-TOKEN": f.Env.RequiredEnv.JobToken}, &j)
	if err == nil && !j.Project.JobTokenScopeEnabled {
		// Warn the user directly, so they can make the required correction.
		f.Msg.Warn(fmt.Sprintf(
			"Job Token access must be restricted to use runner. See: %s%s",
			"https://docs.gitlab.com/",
			"ee/ci/jobs/ci_job_token.html#limit-gitlab-cicd-job-token-access",
		))
		err = errors.New("ci_job_token_scope_enabled not enabled for the project")
	}

	return err
}

func (f Factory) jobURL() (string, error) {
	u, err := url.Parse(f.Env.RequiredEnv.ServerURL)
	if err != nil {
		return "", err
	}

	u.Path = strings.TrimRightFunc(u.Path, func(r rune) bool {
		return r == '/'
	}) + jobPath

	return u.String(), nil
}
