package fullauth

import (
	"errors"
	"io"
	"os/user"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/gljobctx-go"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser/validation"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_gljobctx"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_validation"
	tst "gitlab.com/ecp-ci/jacamar-ci/tools/jacamar-testing"
)

var (
	currentUsr *user.User
)

type authTests struct {
	auth    configure.Auth
	cfg     configure.Configurer
	env     envparser.ExecutorEnv
	factory Factory
	jwt     gljobctx.Claims
	u       *authuser.UserContext
	valid   authuser.Validators

	mockEstValid func(configure.Configurer, envparser.ExecutorEnv) (authuser.Validators, error)
	mockCurUsr   func() (*user.User, error)

	assertError      func(*testing.T, error)
	assertContext    func(*testing.T, *authuser.UserContext)
	assertAuthorized func(*testing.T, authuser.Authorized)
}

func buildMockFullOpt() configure.Options {
	return configure.Options{
		Auth: configure.Auth{
			UserAllowlist:  []string{"u1", "u4", "u8"},
			UserBlocklist:  []string{"u3", "u4", "u8"},
			GroupAllowlist: []string{"g2", "g5", "g8"},
			GroupBlocklist: []string{"g7", "g8"},
		},
		General: configure.General{
			DataDir: "/ci",
		},
	}
}

func buildMockMinOpt() configure.Options {
	return configure.Options{
		Auth: configure.Auth{
			Downscope: "setuid",
		},
		General: configure.General{
			DataDir: "/ci",
		},
	}
}

func TestFactory_EstablishUser(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	ll := logrus.New()
	ll.Out = io.Discard
	sysLog := logrus.NewEntry(ll)

	jwtTest := mock_gljobctx.NewMockValidater(ctrl)
	jwtTest.EXPECT().ValidateJWT(
		gomock.Eq("jwt.failed.example"),
		gomock.Eq("failed.example"),
	).Return(gljobctx.Claims{}, errors.New("error message")).AnyTimes()
	jwtTest.EXPECT().ValidateJWT(
		gomock.Eq("jwt.success.example"),
		gomock.Eq("success.example"),
	).Return(gljobctx.Claims{
		JobID:     "123",
		UserLogin: currentUsr.Username,
	}, nil).AnyTimes()

	runasErr := mock_validation.NewMockRunAsValidator(ctrl)
	runasErr.EXPECT().Execute(gomock.Any(), gomock.Any(), gomock.Any()).Return(validation.RunAsOverride{}, errors.New("error message")).AnyTimes()

	runasOverride := mock_validation.NewMockRunAsValidator(ctrl)
	runasOverride.EXPECT().Execute(gomock.Any(), gomock.Any(), gomock.Any()).Return(validation.RunAsOverride{
		Username: currentUsr.Username,
		DataDir:  "/override/dir",
	}, nil).AnyTimes()

	tests := map[string]authTests{
		"invalid stateful username": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						Username: "invalid-user",
					},
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						ServerURL: "success.example",
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					JobJWT: jwtTest,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "failed to process state for invalid-user: user: unknown user invalid-user")
			},
		},
		"invalid jwt": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.failed.example",
						ServerURL: "failed.example",
					},
				},
				Valid: authuser.Validators{
					JobJWT: jwtTest,
				},
				SysLog: sysLog,
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unable to parse supplied CI_JOB_JWT: unable to parse supplied CI_JOB_JWT: error message")
			},
		},
		"completed process from state, no downscope": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						Username: currentUsr.Username,
					},
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						ServerURL: "success.example",
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					JobJWT: jwtTest,
				},
			},
			assertError: tst.AssertNoError,
		},
		"context failed from state, setuid downscope": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						Username: currentUsr.Username,
					},
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						ServerURL: "success.example",
					},
				},
				Opt: configure.Options{
					Auth: configure.Auth{
						Downscope: "setuid",
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					JobJWT: jwtTest,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), "cannot execute as "+currentUsr.Username)
			},
		},
		"processes failed from jwt, setuid downscope": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						Username: currentUsr.Username,
					},
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						ServerURL: "success.example",
					},
				},
				Opt: configure.Options{
					Auth: configure.Auth{
						Downscope: "setuid",
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					JobJWT: jwtTest,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), "cannot execute as "+currentUsr.Username)
			},
		},
		"processes from jwt, no downscope": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						ServerURL: "success.example",
					},
				},
				Opt: configure.Options{
					General: configure.General{
						DataDir: "/ci",
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					JobJWT: jwtTest,
				},
			},
			assertError: tst.AssertNoError,
			assertAuthorized: func(t *testing.T, auth authuser.Authorized) {
				assert.Equal(t, currentUsr.Username, auth.CIUser().Username)
			},
		},
		"processes from jwt, invalid directory configurations": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						ServerURL: "success.example",
					},
				},
				Opt: configure.Options{
					General: configure.General{
						DataDir:        "/ci",
						CustomBuildDir: true,
					},
					Auth: configure.Auth{
						RootDirCreation: true,
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					JobJWT: jwtTest,
				},
			},
			assertError: tst.AssertError,
		},
		"processes from jwt, user block list pre-validation": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						ServerURL: "success.example",
					},
				},
				Opt: configure.Options{
					General: configure.General{
						DataDir: "/ci",
					},
					Auth: configure.Auth{
						ListsPreValidation: true,
						UserBlocklist: []string{
							currentUsr.Username,
						},
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					JobJWT: jwtTest,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), "is not in the user allowlist and is in the user blocklist")
			},
		},
		"processes from jwt, user block list": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						ServerURL: "success.example",
					},
				},
				Opt: configure.Options{
					General: configure.General{
						DataDir: "/ci",
					},
					Auth: configure.Auth{
						UserBlocklist: []string{
							currentUsr.Username,
						},
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					JobJWT: jwtTest,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), "is not in the user allowlist and is in the user blocklist")
			},
		},
		"processes from jwt, runas failure": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						ServerURL: "success.example",
					},
				},
				Opt: configure.Options{
					General: configure.General{
						DataDir: "/ci",
					},
					Auth: configure.Auth{
						UserBlocklist: []string{
							currentUsr.Username,
						},
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					RunAs:  runasErr,
					JobJWT: jwtTest,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "failed to authorize user for CI job: invalid authorization target user: RunAs validation: error message")
			},
		},
		"processes from jwt, runas override": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						ServerURL: "success.example",
					},
				},
				Opt: configure.Options{
					General: configure.General{
						DataDir: "/ci",
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					RunAs:  runasOverride,
					JobJWT: jwtTest,
				},
			},
			assertError: tst.AssertNoError,
			assertAuthorized: func(t *testing.T, authorized authuser.Authorized) {
				ctx := authorized.CIUser()
				assert.Equal(t, "/override/dir", ctx.DataDirOverride)
			},
		},
		"processes from jwt, failed check token scope": {
			factory: Factory{
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						CIJobJWT:  "jwt.success.example",
						JobToken:  "T0k3n",
						ServerURL: "success.example",
					},
				},
				Opt: configure.Options{
					Auth: configure.Auth{
						TokenScopeEnforced: true,
					},
					General: configure.General{
						DataDir: "/ci",
					},
				},
				SysLog: sysLog,
				Valid: authuser.Validators{
					JobJWT: jwtTest,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				if err != nil {
					assert.Contains(t, err.Error(), "failed to enforce ci_job_token_scope")
				}
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := tt.factory.EstablishUser()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertAuthorized != nil {
				tt.assertAuthorized(t, got)
			}
		})
	}
}

func Test_Authorized_Interface(t *testing.T) {
	working := targetCtx{
		usrCtx: authuser.UserContext{
			Username:  "username",
			HomeDir:   "/home/username",
			UID:       1000,
			GID:       2000,
			Groups:    []uint32{1, 2, 3},
			BaseDir:   "/base",
			BuildsDir: "/builds",
			CacheDir:  "/cache",
			ScriptDir: "/script",
		},
		jwt: gljobctx.Claims{
			JobID: "123",
		},
	}

	t.Run("verify basic interface functionality implemented", func(tt *testing.T) {
		working.BuildState()
		assert.Equal(tt, "Running as username UID: 1000 GID: 2000\n", working.PrepareNotification())
		assert.Equal(tt, gljobctx.Claims{
			JobID: "123",
		}, working.JobJWT())
	})
}

func init() {
	// Test involving user accounts must target a valid local account.
	currentUsr, _ = user.Current()
}
