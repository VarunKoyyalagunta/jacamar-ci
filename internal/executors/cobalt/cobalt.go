package cobalt

import (
	"fmt"
	"sync"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/augmenter"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/batch"
)

const (
	summaryVar = envkeys.UserEnvPrefix + "COBALT_SUMMARIZE_ERRORS"
)

type executor struct {
	absExec *abstracts.Executor

	// batch submission only variables
	mng         batch.Manager
	outLog      string
	errLog      string
	logFile     string
	jobID       string
	completeMsg string
	sleepTime   time.Duration // Sleep between scheduler integrations.
}

func (e *executor) Run() error {
	if !(e.absExec.Stage == "step_script" || e.absExec.Stage == "build_script") {
		return e.absExec.Runner.JobScriptOutput(e.absExec.ScriptPath)
	}

	// Modify command and create login script for submission to qsub,
	// to account for limited environment.
	e.absExec.Runner.ModifyCmd(command.IdentifyShell(e.absExec.Cfg.General()))
	step := augmenter.OpenStepScript(e.absExec.ScriptPath)
	step.LoginShell()
	step.MessageExit(e.completeMsg)
	if err := step.VerifyAndWrite(); err != nil {
		return fmt.Errorf("failed to updated Cobalt job script: %w", err)
	}

	return e.runCobalt()
}

func (e *executor) runCobalt() error {
	if err := e.submitJob(); err != nil {
		return fmt.Errorf(
			"error while attempting to submit job to Cobalt: %w", err,
		)
	}

	if err := e.monitorJob(); err != nil {
		// Ignore job monitoring error specifics, only surface the fact that there has
		// been an error during execution. This may not be entirely true depending on
		// the source of the failure, but we cannot actually identify in all potential cases.
		return fmt.Errorf("cobalt job (%s) encountered an error during execution", e.jobID)
	}

	return nil
}

func (e *executor) submitJob() error {
	qsubStdin := e.mng.BatchCmd(e.mng.UserArgs())
	e.absExec.Msg.Notify("Cobalt job command: %s %s", qsubStdin, e.absExec.ScriptPath)

	out, err := e.absExec.Runner.JobScriptReturn(e.absExec.ScriptPath, qsubStdin)
	if err != nil {
		return fmt.Errorf("failed to request allocation (%s): %s", qsubStdin, out)
	}

	jobID, err := qsubJobID(out)
	if err != nil {
		return fmt.Errorf("failed to identify jobid from qsub stdout: %s", out)
	}
	e.jobID = jobID
	fmt.Printf("Submitted batch job %s\n", e.jobID)

	scriptDir := e.absExec.Env.StatefulEnv.ScriptDir
	e.outLog = fmt.Sprintf("%s/%s.output", scriptDir, e.jobID)
	e.errLog = fmt.Sprintf("%s/%s.error", scriptDir, e.jobID)
	e.logFile = fmt.Sprintf("%s/%s.cobaltlog", scriptDir, e.jobID)

	// generate expected output/error files
	batch.CreateFiles([]string{e.outLog, e.errLog}, e.absExec.Msg)

	return nil
}

// monitorJob examines the job's output logs and status throughout duration.
func (e *executor) monitorJob() error {
	var wg sync.WaitGroup
	wg.Add(2) // output files

	jobDone := make(chan struct{})
	go func() {
		defer wg.Done()
		err := e.mng.TailFiles(
			[]string{e.outLog, e.errLog}, jobDone, 45*time.Second, e.absExec.Msg,
		)
		if err != nil {
			e.absExec.Msg.Warn(
				"Unable to monitor files, job output distrusted: %s", err.Error(),
			)
		}
	}()

	go func() {
		defer wg.Done()
		e.mng.MonitorTermination(e.absExec.Runner, jobDone, e.jobID, e.absExec.Env.StatefulEnv.ScriptDir)
	}()

	qstatCmd := fmt.Sprintf("%s %s", e.mng.StateCmd(), e.jobID)
	for {
		_, err := e.absExec.Runner.ReturnOutput(qstatCmd)
		if err != nil {
			break
		}
		time.Sleep(e.sleepTime)
	}

	close(jobDone)
	wg.Wait()

	// Wait for configured NFS timeout window.
	e.mng.NFSTimeout((e.absExec.Cfg.Batch()).NFSTimeout, e.absExec.Msg)

	summarizeErrors(e.errLog, e.jobID, e.absExec.Msg)

	err := e.exitStatus(e.logFile)
	if err != nil {
		return fmt.Errorf("unable to identify job's final exit status: %w", err)
	}

	return nil
}

// NewExecutor generates a valid Slurm executor that fulfills the executors.Executor interface.
func NewExecutor(ae *abstracts.Executor) (*executor, error) {
	var err error
	e := &executor{
		absExec: ae,
	}

	if e.absExec.Stage == "step_script" || ae.Stage == "build_script" {
		set := batch.Settings{
			BatchCmd:    "qsub",
			StateCmd:    "qstat",
			StopCmd:     "qdel",
			IllegalArgs: []string{"-o", "--output", "-e", "--error", "--debuglog"},
		}
		e.mng, err = batch.NewBatchJob(set, ae.Cfg.Batch(), ae.Msg)
		if err != nil {
			return nil, err
		}

		e.completeMsg = fmt.Sprintf("Cobalt CI Job %s completed.", ae.Env.RequiredEnv.JobID)
		e.sleepTime = batch.CommandDelay(ae.Cfg.Batch().CommandDelay, ae.Msg)
	}

	return e, nil
}
