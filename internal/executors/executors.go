// Package executors manages global aspects of the CI job before final responsibility
// for execution is handed over to the specified interface.
package executors

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/augmenter"
	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/internal/usertools"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
)

// Executor manages the runnable operations associated with the custom-executor and 'jacamar'
// application. Realization of the interface requires execution of the implemented stage,
// further documented online (https://docs.gitlab.com/runner/executors/custom.html).
// The Run stage specifically will be called multiple time for each generated script.
type Executor interface {
	// Run implements the runner's associated stage (run_exec). It is invoked multiple times, once
	// for  each stage. The stage name and target script for execution can be provided in
	// the executor.JobConfig structure.
	Run() error
}

// Prepare realizes generic aspects of the prepare_exec custom executor stage.
func Prepare(a *abstracts.Executor) error {
	return usertools.CreateDirectories(a.Env.StatefulEnv, a.Cfg.General())
}

// Run realizes generic aspects of the run_exec custom executor stage prior to calling the
// provided Executor.
func Run(a *abstracts.Executor, e Executor, auth authuser.Authorized) error {
	tar := fmt.Sprintf("%s/%s.bash", a.Env.StatefulEnv.ScriptDir, a.Stage)
	err := relocateJobScript(a, tar)
	if err != nil {
		return fmt.Errorf("unable to relocateJobScript (%s): %w", a.ScriptPath, err)
	}
	a.ScriptPath = tar

	// Remove job script to avoid storing tokens locally.
	defer removeAll(a.ScriptPath)

	if a.Stage == "prepare_script" {
		// We use our own more detailed message, thus can ignore the GitLab generated one.
		printPrepareMessage(a, auth)
	} else if a.Stage == "get_sources" {
		err = augmenter.CreateGitAskpass(a.Env, a.Msg)
		if err != nil {
			return fmt.Errorf("unable to CreateGitAskpass: %w", err)
		}
	}

	return e.Run()
}

// Cleanup realizes generic aspects of the cleanup_exec custom executor stage and will log
// related errors to stderr or handle silently.
func Cleanup(a *abstracts.Executor) {
	defer cleanupScriptDir(a)

	// Remove GitAskPass file if it had been created.
	augmenter.CleanupGitAskpass(a.Env, a.Msg, a.Cfg.General())
}

func printPrepareMessage(a *abstracts.Executor, auth authuser.Authorized) {
	var sb strings.Builder

	sb.WriteString(a.Cfg.PrepareNotification(a.Env.StatefulEnv.ScriptDir))
	sb.WriteString(auth.PrepareNotification())

	stamp := time.Now().Format("2006-01-02 15:04:05")
	sb.WriteString("Local time: " + stamp + "\n")

	a.Msg.Stdout(sb.String())
}

// cleanupScriptDir ensure post job logic for the scheduler created script directory and all files
// found within are properly executed. User defined COPY_SCHEDULER_LOGS is used to determine a new
// target for the directory.
func cleanupScriptDir(a *abstracts.Executor) {
	if !a.Cfg.General().RetainLogs {
		defer removeAll(a.Env.StatefulEnv.ScriptDir)
	}

	// Copying the log file will be managed by the user's shell, all variables are realized.
	copyDest, found, err := envparser.SchedulerLogDir()
	if err != nil {
		a.Msg.Warn(fmt.Sprintf(
			"Invalid %s, verify fully qualified path declared.",
			envkeys.CopySchedulerLogs,
		))
	} else if found {
		if err = os.Chmod(a.Env.StatefulEnv.ScriptDir, usertools.OwnerPermissions); err != nil {
			a.Msg.Warn("unable to ensure permissions script directory: %s", err.Error())
		}

		copyDest = filepath.Clean(strings.TrimSpace(copyDest))

		// We rely on executing the command via this method as it allows users
		// to embed environment variables which will then be properly resolved.
		out, err := a.Runner.ReturnOutput(
			fmt.Sprintf(
				"mkdir -p %s && cp -aR %s %s/",
				copyDest,
				a.Env.StatefulEnv.ScriptDir,
				copyDest,
			),
		)
		if err != nil {
			a.Msg.Warn("error encountered while attempting to copy log directory: %s", out)
		}
	}
}

// relocateJobScript ensures the proper relocations of a job script from the source path
// provided via command line arguments to the target. The current run_exec stage along with
// the established builds_dir must be provided.
func relocateJobScript(a *abstracts.Executor, tarPath string) (err error) {
	var contents string

	if a.ScriptPath == "env-script" {
		contents, err = envparser.RetrieveScriptEnv()
		if err != nil {
			return fmt.Errorf("unable to RetrieveScriptEnv: %w", err)
		}
	} else {
		rules := augmenter.Rules{
			UnrestrictedCmdline: a.Cfg.General().UnrestrictedCmdline,
		} // Default rules only
		contents, err = rules.JobScript(a.ScriptPath, a.Stage, a.Env)
		if err != nil {
			return fmt.Errorf("unable to augment CI JobScript: %w", err)
		}
	}

	return finalizeJobScript(contents, tarPath)
}

// finalizeJobScript prepares a runner generated script for usage with any executor, ensuring the
// script will result in the appropriate shell settings. Script is then written to target file.
func finalizeJobScript(contents, target string) error {
	return os.WriteFile(target, []byte(contents), usertools.OwnerPermissions)
}

func removeAll(tar string) {
	_ = os.RemoveAll(tar)
}
