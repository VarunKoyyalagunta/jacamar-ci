package slurm

import (
	"errors"
	"fmt"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/augmenter"
	"gitlab.com/ecp-ci/jacamar-ci/internal/command"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/batch"
)

type executor struct {
	absExec *abstracts.Executor

	// batch submission only variables
	mng     batch.Manager
	jobName string
	outFile string
}

func (e *executor) Run() error {
	if !(e.absExec.Stage == "step_script" || e.absExec.Stage == "build_script") {
		return e.absExec.Runner.JobScriptOutput(e.absExec.ScriptPath)
	}

	// Job scripts to be submitted via sbatch, needs to be updated.
	step := augmenter.OpenStepScript(e.absExec.ScriptPath)
	step.LoginShell()
	if err := step.VerifyAndWrite(); err != nil {
		return fmt.Errorf("failed to updated Slurm job script: %w", err)
	}

	return e.runSlurm()
}

func (e *executor) runSlurm() error {
	// Pre-create output file to avoid issues with jobs stuck in queue.
	batch.CreateFiles([]string{e.outFile}, e.absExec.Msg)

	var wg sync.WaitGroup
	wg.Add(2)

	cmdErr := make(chan error, 1)
	defer close(cmdErr)
	go func() {
		defer wg.Done()
		e.submitJob(cmdErr)
	}()

	jobDone := make(chan struct{}, 1)
	defer close(jobDone)
	go func() {
		defer wg.Done()
		e.monitorJob(jobDone)
	}()

	err := <-cmdErr
	jobDone <- struct{}{}

	wg.Wait()

	if e.absExec.Cfg.Batch().FFSlurmSacct && err == nil {
		err = e.verifyJobState()
	}

	return err
}

func (e *executor) submitJob(cmdErr chan error) {
	sbatchStdin := e.mng.BatchCmd(fmt.Sprintf(
		"--wait --job-name=%s --output=%s %s",
		e.jobName,
		e.outFile,
		e.mng.UserArgs(),
	))
	e.absExec.Msg.Notify("Slurm job command: %s %s", sbatchStdin, e.absExec.ScriptPath)

	err := e.absExec.Runner.JobScriptOutput(e.absExec.ScriptPath, sbatchStdin)
	cmdErr <- err
}

func (e *executor) monitorJob(jobDone chan struct{}) {
	stopArg := fmt.Sprintf("--name=%s", e.jobName)

	var wg sync.WaitGroup
	wg.Add(1) // outfile

	stopTail := make(chan struct{}, 1)
	defer close(stopTail)
	go func() {
		defer wg.Done()
		// We generate output file in previous step from local host, add arbitrary short timeout.
		err := e.mng.TailFiles([]string{e.outFile}, stopTail, 10*time.Second, e.absExec.Msg)
		if err != nil {
			e.absExec.Msg.Warn("Unable to monitor output file (%s): %s", e.outFile, err.Error())
			e.absExec.Msg.Stdout("Jacamar will attempt to cancel job (%s)", e.jobName)

			command.NoOutputCmd(e.mng.StopCmd(), stopArg)
		}
	}()

	e.mng.MonitorTermination(e.absExec.Runner, jobDone, stopArg, e.absExec.Env.StatefulEnv.ScriptDir)
	e.mng.NFSTimeout((e.absExec.Cfg.Batch()).NFSTimeout, e.absExec.Msg)

	stopTail <- struct{}{}
	wg.Wait()
}

// verifyJobState establish a secondary check using 'sacct' to ensure a jobs has "COMPLETED"
// successfully, else an error will be returned. Cases encountered where a job is still running
// will result in 'scancel' being invoked.
func (e *executor) verifyJobState() error {
	yesterday := time.Now().AddDate(0, 0, -1)
	startDate := yesterday.Format("01/02/06")
	sacctStdin := fmt.Sprintf("%s -n --name %s --format=state -S %s", e.mng.StateCmd(), e.jobName, startDate)

	out, err := e.absExec.Runner.ReturnOutput(sacctStdin)
	if err != nil {
		return fmt.Errorf("unable to obtained output via sacct: %s", out)
	}

	jobState := sacctState(out)
	e.absExec.Msg.Notify("Slurm job %s state: %s", e.jobName, jobState)

	switch jobState {
	case "PENDING", "RUNNING", "REQUEUED", "RESIZING":
		command.NoOutputCmd(e.mng.StopCmd(), fmt.Sprintf("--name=%s", e.jobName))
		return errors.New("job still in progress, attempting to stop")
	case "COMPLETED":
		return nil
	default:
		return errors.New("job did not complete successfully")
	}
}

// sacctState parses stdout from the sacct cmd to retrieve the job's state
// expected: $ sacct -n -j # --format=state
func sacctState(out string) string {
	lines := strings.Split(out, "\n")
	return strings.ToUpper(strings.TrimSpace(lines[0]))
}

// NewExecutor generates a valid Slurm executor that fulfills the executors.Executor interface.
func NewExecutor(ae *abstracts.Executor) (*executor, error) {
	var err error
	e := &executor{
		absExec: ae,
	}

	if e.absExec.Stage == "step_script" || ae.Stage == "build_script" {
		set := batch.Settings{
			BatchCmd: "sbatch",
			StopCmd:  "scancel",
			StateCmd: "sacct",
			IllegalArgs: []string{
				"-o",
				"--output",
				"-J",
				"--job-name",
				"--wait",
			},
		}

		e.jobName = fmt.Sprintf(
			"ci-%s_%d",
			ae.Env.RequiredEnv.JobID,
			time.Now().Unix(),
		)

		e.mng, err = batch.NewBatchJob(set, ae.Cfg.Batch(), ae.Msg)
		if err != nil {
			return nil, err
		}

		e.outFile = filepath.Clean(fmt.Sprintf(
			"%s/slurm-ci-%s.out",
			ae.Env.StatefulEnv.ScriptDir,
			ae.Env.RequiredEnv.JobID,
		))
	}

	return e, nil
}
