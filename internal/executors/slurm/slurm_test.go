package slurm

import (
	"errors"
	"os"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/batch"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_batch"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

// We should mainly rely on Pavilion + Facility/Container to realize more comprehensive
// testing against Slurm. Use these tests to ensure we properly handle errors, goroutines,
// and channel communications where possible.

func genericSlurmRun(ctrl *gomock.Controller) *mock_runmechanisms.MockRunner {
	m := mock_runmechanisms.NewMockRunner(ctrl)
	m.EXPECT().JobScriptOutput(gomock.Any(), gomock.Any()).
		DoAndReturn(func(script interface{}, stdin ...interface{}) error {
			time.Sleep(1 * time.Second)
			return nil
		})
	m.EXPECT().RequestContext().Return(nil).AnyTimes()
	return m
}

func genericBatchMng(ctrl *gomock.Controller) *mock_batch.MockManager {
	m := mock_batch.NewMockManager(ctrl)
	m.EXPECT().BatchCmd(gomock.Any()).Return("sbatch").AnyTimes()
	m.EXPECT().StateCmd().Return("sacct").AnyTimes()
	m.EXPECT().StopCmd().Return("scancel").AnyTimes()
	m.EXPECT().UserArgs().Return("args").AnyTimes()
	m.EXPECT().MonitorTermination(gomock.Any(), gomock.Any(), "--name=ci-test_job", gomock.Any()).Return().AnyTimes()
	m.EXPECT().TailFiles(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(nil)
	m.EXPECT().NFSTimeout(gomock.Any(), gomock.Any()).Return()
	return m
}

func genericMsg(ctrl *gomock.Controller) *mock_logging.MockMessenger {
	m := mock_logging.NewMockMessenger(ctrl)
	m.EXPECT().Notify("Slurm job command: %s %s", "sbatch", gomock.Any()).AnyTimes()
	return m
}

func genericCfg(ctrl *gomock.Controller, cb configure.Batch) *mock_configure.MockConfigurer {
	m := mock_configure.NewMockConfigurer(ctrl)
	m.EXPECT().Batch().Return(cb).AnyTimes()
	return m
}

func Test_executor_Run(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	jobScript := `#!/usr/bin/env bash

set -eo pipefail
set +o noclobber
: | eval $'echo "Running a test..."\n'
exit 0
`
	tmpJobScript := t.TempDir() + "/job.bash"
	_ = os.WriteFile(tmpJobScript, []byte(jobScript), 0700)

	type fields struct {
		absExec *abstracts.Executor
		mng     batch.Manager
		outFile string
	}
	tests := map[string]struct {
		fields      fields
		assertError func(*testing.T, error)
	}{
		"working after_script launched locally": {
			fields: fields{
				absExec: &abstracts.Executor{
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().JobScriptOutput("/working/after_script.bash").Return(nil)
						return m
					}(),
					ScriptPath: "/working/after_script.bash",
					Stage:      "after_script",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"failing get_sources launched locally": {
			fields: fields{
				absExec: &abstracts.Executor{
					Runner: func() *mock_runmechanisms.MockRunner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().JobScriptOutput("/failing/get_sources.bash").Return(errors.New("error message"))
						return m
					}(),
					ScriptPath: "/failing/get_sources.bash",
					Stage:      "get_sources",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
			},
		},
		"non-existent script path defined": {
			fields: fields{
				absExec: &abstracts.Executor{
					Msg:        genericMsg(ctrl),
					ScriptPath: "/file/does/not/exist.bash",
					Stage:      "step_script",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "failed to updated Slurm job script: unexpect script detected, only runner generated currently supported")
			},
		},
		"successfully run job": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg:        genericCfg(ctrl, configure.Batch{}),
					Runner:     genericSlurmRun(ctrl),
					Msg:        genericMsg(ctrl),
					ScriptPath: tmpJobScript,
					Stage:      "step_script",
				},
				mng:     genericBatchMng(ctrl),
				outFile: t.TempDir() + "/slurm.out",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"identify cancelled job with sacct": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: genericCfg(ctrl, configure.Batch{
						FFSlurmSacct: true,
					}),
					Runner: func() *mock_runmechanisms.MockRunner {
						m := genericSlurmRun(ctrl)
						m.EXPECT().ReturnOutput(gomock.Any()).Return("CANCELLED", nil)
						return m
					}(),
					Msg: func() *mock_logging.MockMessenger {
						m := genericMsg(ctrl)
						m.EXPECT().Notify("Slurm job %s state: %s", "ci-test_job", "CANCELLED")
						return m
					}(),
					ScriptPath: tmpJobScript,
					Stage:      "step_script",
				},
				mng:     genericBatchMng(ctrl),
				outFile: t.TempDir() + "/slurm.out",
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "job did not complete successfully")
			},
		},
		"failed sacct command": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: genericCfg(ctrl, configure.Batch{
						FFSlurmSacct: true,
					}),
					Runner: func() *mock_runmechanisms.MockRunner {
						m := genericSlurmRun(ctrl)
						m.EXPECT().ReturnOutput(gomock.Any()).Return("output...", errors.New("error_message"))
						return m
					}(),
					Msg:        genericMsg(ctrl),
					ScriptPath: tmpJobScript,
					Stage:      "step_script",
				},
				mng:     genericBatchMng(ctrl),
				outFile: t.TempDir() + "/slurm.out",
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unable to obtained output via sacct: output...")
			},
		},
		"attempt to cancel pending job": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: genericCfg(ctrl, configure.Batch{
						FFSlurmSacct: true,
					}),
					Runner: func() *mock_runmechanisms.MockRunner {
						m := genericSlurmRun(ctrl)
						m.EXPECT().ReturnOutput(gomock.Any()).Return("PENDING", nil)
						return m
					}(),
					Msg: func() *mock_logging.MockMessenger {
						m := genericMsg(ctrl)
						m.EXPECT().Notify("Slurm job %s state: %s", "ci-test_job", "PENDING")
						return m
					}(),
					ScriptPath: tmpJobScript,
					Stage:      "step_script",
				},
				mng:     genericBatchMng(ctrl),
				outFile: t.TempDir() + "/slurm.out",
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "job still in progress, attempting to stop")
			},
		},
		"job completed w/sacct": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg: genericCfg(ctrl, configure.Batch{
						FFSlurmSacct: true,
					}),
					Runner: func() *mock_runmechanisms.MockRunner {
						m := genericSlurmRun(ctrl)
						m.EXPECT().ReturnOutput(gomock.Any()).Return("COMPLETED", nil)
						return m
					}(),
					Msg: func() *mock_logging.MockMessenger {
						m := genericMsg(ctrl)
						m.EXPECT().Notify("Slurm job %s state: %s", "ci-test_job", "COMPLETED")
						return m
					}(),
					ScriptPath: tmpJobScript,
					Stage:      "step_script",
				},
				mng:     genericBatchMng(ctrl),
				outFile: t.TempDir() + "/slurm.out",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec: tt.fields.absExec,
				mng:     tt.fields.mng,
				jobName: "ci-test_job",
				outFile: tt.fields.outFile,
			}

			err := e.Run()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestNewExecutor(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tempDir := t.TempDir()

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().Batch().Return(configure.Batch{
		NFSTimeout:       "5s",
		AllowIllegalArgs: false,
	}).AnyTimes()

	binCfg := mock_configure.NewMockConfigurer(ctrl)
	binCfg.EXPECT().Batch().Return(configure.Batch{
		SchedulerBin:     "/ci/test/bin",
		NFSTimeout:       "5s",
		AllowIllegalArgs: false,
	}).AnyTimes()

	allowCfg := mock_configure.NewMockConfigurer(ctrl)
	allowCfg.EXPECT().Batch().Return(configure.Batch{
		NFSTimeout:       "5s",
		AllowIllegalArgs: true,
	}).AnyTimes()

	msg := mock_logging.NewMockMessenger(ctrl)
	msg.EXPECT().Warn("No %s variable detected, please check your CI job if this is unexpected.", "SCHEDULER_PARAMETERS").Times(2)

	illegalMsg := mock_logging.NewMockMessenger(ctrl)
	illegalMsg.EXPECT().Warn(
		gomock.Eq("Illegal argument detected. Please remove: -J"),
	).Times(2)

	tests := map[string]struct {
		ae             *abstracts.Executor
		prepare        func(*testing.T)
		assertError    func(*testing.T, error)
		assertExecutor func(*testing.T, *executor)
	}{
		// We don't need to test for many failure states as we can
		// rely on that the data provided has already been parsed.
		"basic Slurm executor created for non step_script stage": {
			ae: &abstracts.Executor{
				Stage: "after_script",
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e)
				assert.Equal(t, e.absExec.Stage, "after_script")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"build Slurm executor for step_script": {
			ae: &abstracts.Executor{
				Cfg: cfg,
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID: "101",
					},
					StatefulEnv: envparser.StatefulEnv{
						ScriptDir: tempDir,
					},
				},
				Msg:   msg,
				Stage: "step_script",
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e)
				assert.NotNil(t, e.mng)
				assert.Contains(t, e.jobName, "ci-101_")
				assert.Equal(t, tempDir+"/slurm-ci-101.out", e.outFile)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"scheduler bin observed": {
			ae: &abstracts.Executor{
				Cfg: binCfg,
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID: "101",
					},
					StatefulEnv: envparser.StatefulEnv{
						ScriptDir: tempDir,
					},
				},
				Msg:   msg,
				Stage: "step_script",
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e)
				assert.Equal(t, "/ci/test/bin/sbatch", e.mng.BatchCmd(""))
				assert.Equal(t, "/ci/test/bin/scancel", e.mng.StopCmd())
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"illegal argument results in error": {
			ae: &abstracts.Executor{
				Cfg: cfg,
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID: "101",
					},
					StatefulEnv: envparser.StatefulEnv{
						ScriptDir: tempDir,
					},
				},
				Msg:   illegalMsg,
				Stage: "step_script",
			},
			prepare: func(t *testing.T) {
				t.Setenv(envkeys.UserEnvPrefix+"SCHEDULER_PARAMETERS", "-J custom_name")
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.Nil(t, e)
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "illegal arguments must be addressed before continuing")
			},
		},
		"illegal argument allowed": {
			ae: &abstracts.Executor{
				Cfg: allowCfg,
				Env: envparser.ExecutorEnv{
					RequiredEnv: envparser.RequiredEnv{
						JobID: "101",
					},
					StatefulEnv: envparser.StatefulEnv{
						ScriptDir: tempDir,
					},
				},
				Msg:   illegalMsg,
				Stage: "step_script",
			},
			prepare: func(t *testing.T) {
				t.Setenv(envkeys.UserEnvPrefix+"SCHEDULER_PARAMETERS", "-J custom_name")
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e)
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.prepare != nil {
				tt.prepare(t)
			}

			got, err := NewExecutor(tt.ae)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertExecutor != nil {
				tt.assertExecutor(t, got)
			}
		})
	}
}

func Test_executor_submitJob(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	run := mock_runmechanisms.NewMockRunner(ctrl)
	run.EXPECT().JobScriptOutput(
		"pass.bash",
		"sbatch --wait --job-name=pass --output=/tmp/pass.log --test",
	).Return(nil)
	run.EXPECT().JobScriptOutput(
		"fail.bash",
		gomock.Any(),
	).Return(errors.New("sbatch error"))

	mng := mock_batch.NewMockManager(ctrl)
	mng.EXPECT().BatchCmd(gomock.Any()).Return("sbatch --wait --job-name=pass --output=/tmp/pass.log --test").Times(2)
	mng.EXPECT().UserArgs().Return("--test").Times(2)

	msg := mock_logging.NewMockMessenger(ctrl)
	msg.EXPECT().Notify("Slurm job command: %s %s", "sbatch --wait --job-name=pass --output=/tmp/pass.log --test", gomock.Any()).Times(2)

	type fields struct {
		absExec *abstracts.Executor
		mng     batch.Manager
		jobName string
		outFile string
	}
	tests := map[string]struct {
		fields      fields
		assertError func(*testing.T, error)
	}{
		"sbatch ran without error": {
			fields: fields{
				absExec: &abstracts.Executor{
					Runner:     run,
					Msg:        msg,
					ScriptPath: "pass.bash",
				},
				mng:     mng,
				jobName: "pass",
				outFile: "/tmp/pass.log",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"sbatch encountered error": {
			fields: fields{
				absExec: &abstracts.Executor{
					Runner:     run,
					Msg:        msg,
					ScriptPath: "fail.bash",
				},
				mng: mng,
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "sbatch error")
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec: tt.fields.absExec,
				mng:     tt.fields.mng,
				jobName: tt.fields.jobName,
				outFile: tt.fields.outFile,
			}

			cmdErr := make(chan error, 1)
			defer close(cmdErr)

			e.submitJob(cmdErr)

			if tt.assertError != nil {
				tt.assertError(t, <-cmdErr)
			}
		})
	}
}

func Test_executor_monitorJob(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().Batch().Return(configure.Batch{
		NFSTimeout: "5s",
	})

	run := mock_runmechanisms.NewMockRunner(ctrl)
	run.EXPECT().RequestContext().Return(nil).AnyTimes()

	msg := mock_logging.NewMockMessenger(ctrl)
	msg.EXPECT().Warn("Unable to monitor output file (%s): %s", gomock.Any(), gomock.Any())
	msg.EXPECT().Stdout("Jacamar will attempt to cancel job (%s)", gomock.Any())

	taiError := mock_batch.NewMockManager(ctrl)
	taiError.EXPECT().TailFiles(gomock.Any(), gomock.Any(), gomock.Any(), gomock.Any()).Return(errors.New("tail error"))
	taiError.EXPECT().MonitorTermination(gomock.Any(), nil, "--name=", gomock.Any()).Return()
	taiError.EXPECT().NFSTimeout(gomock.Eq("5s"), gomock.Any())
	taiError.EXPECT().StopCmd().Return("scancel")

	t.Run("job completed, channel closed", func(t *testing.T) {
		e := &executor{
			absExec: &abstracts.Executor{
				Cfg:    cfg,
				Msg:    msg,
				Runner: run,
			},
			mng: taiError,
		}

		e.monitorJob(nil)
	})
}
