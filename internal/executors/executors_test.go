package executors

import (
	"crypto/sha256"
	"encoding/hex"
	"io"
	"os"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/authuser"
	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/envparser"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_authuser"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_executors"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

type executorsTests struct {
	scriptPath  string
	execOptions string
	targetPath  string
	stage       string
	buildsDir   string
	targetEnv   map[string]string
	env         envparser.ExecutorEnv

	auth   *mock_authuser.MockAuthorized
	cfg    *mock_configure.MockConfigurer
	exec   *mock_executors.MockExecutor
	msg    *mock_logging.MockMessenger
	runner *mock_runmechanisms.MockRunner

	assertError  func(t *testing.T, err error)
	assertMap    func(t *testing.T, m map[string]string)
	assertString func(t *testing.T, s string)
}

func validAuth(ctrl *gomock.Controller) *mock_authuser.MockAuthorized {
	m := mock_authuser.NewMockAuthorized(ctrl)
	m.EXPECT().BuildState().Return(envparser.StatefulEnv{
		Username:  "user",
		BaseDir:   "/base",
		BuildsDir: "/builds",
		CacheDir:  "/cache",
		ScriptDir: "/script",
	}).AnyTimes()
	m.EXPECT().CIUser().Return(authuser.UserContext{
		BuildsDir: "/builds",
		CacheDir:  "/cache",
		ScriptDir: "/script",
	}).AnyTimes()
	m.EXPECT().PrepareNotification().Return("Running as ...\n").AnyTimes()
	return m
}

func Test_printPrepareMessage(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	t.Run("expected message interfaces invoked", func(t *testing.T) {
		cfg := mock_configure.NewMockConfigurer(ctrl)
		cfg.EXPECT().PrepareNotification(gomock.Eq("/script")).Return("Targeting test executor\n").Times(1)

		msg := mock_logging.NewMockMessenger(ctrl)
		msg.EXPECT().Stdout(gomock.Any()).Times(1)

		a := &abstracts.Executor{
			Cfg: cfg,
			Msg: msg,
			Env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ScriptDir: "/script",
				},
			},
		}
		printPrepareMessage(a, validAuth(ctrl))
	})
}

func sha256File(file string) (string, error) {
	f, err := os.Open(file)
	if err != nil {
		return "", err
	}
	defer func() { _ = f.Close() }()

	sha := sha256.New()
	if _, err := io.Copy(sha, f); err != nil {
		return "", err
	}
	return hex.EncodeToString(sha.Sum(nil)), nil
}

func Test_relocateJobScript(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	dir, _ := os.MkdirTemp(t.TempDir(), "relocateJobScript")

	tests := map[string]executorsTests{
		"prepare_script provided via command line, no changes required": {
			scriptPath: "../../test/testdata/jobscript.bash",
			targetPath: dir + "/prepare_nochange.bash",
			stage:      "prepare_script",
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					BuildsDir: dir,
				},
			},
			targetEnv: map[string]string{
				envkeys.UserEnvPrefix + "JACAMAR_NO_BASH_PROFILE": "true",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				sum, err := sha256File(s)
				assert.NoError(t, err, "failed to obtain sha256 sum")
				assert.Equal(
					t,
					"7d83836141882826a5417fc3d06da0907d763538b10aa0869eec43c7cff9d39c",
					sum,
				)
			},
		},
		"prepare_script provided via command line, login shell": {
			scriptPath: "../../test/testdata/jobscript.bash",
			targetPath: dir + "/prepare_login.bash",
			stage:      "prepare_script",
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					BuildsDir: dir,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertString: func(t *testing.T, s string) {
				sum, err := sha256File(s)
				assert.NoError(t, err, "failed to obtain sha256 sum")
				assert.Equal(
					t,
					"7d83836141882826a5417fc3d06da0907d763538b10aa0869eec43c7cff9d39c",
					sum,
				)
			},
		},
		"invalid script contents provided env": {
			scriptPath: "env-script",
			targetPath: dir + "/prepare_profile.bash",
			stage:      "prepare_script",
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					BuildsDir: dir,
				},
			},
			targetEnv: map[string]string{
				envkeys.ScriptContentsPrefix + "0": "BASE%%64",
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"unable to RetrieveScriptEnv: error decoding script contents, illegal base64 data at input byte 4",
				)
			},
		},
		"invalid source file path provided": {
			scriptPath: "/file/doesnt/exist.bash",
			targetPath: dir + "/prepare_error.bash",
			stage:      "prepare_script",
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					BuildsDir: dir,
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t, err,
					"unable to augment CI JobScript: open /file/doesnt/exist.bash: no such file or directory",
				)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			for k, v := range tt.targetEnv {
				t.Setenv(k, v)
			}

			cfg := mock_configure.NewMockConfigurer(ctrl)
			cfg.EXPECT().General().Return(configure.General{}).AnyTimes()

			a := &abstracts.Executor{
				ScriptPath: tt.scriptPath,
				Stage:      tt.stage,
				Env:        tt.env,
				Cfg:        cfg,
			}

			err := relocateJobScript(a, tt.targetPath)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertString != nil {
				tt.assertString(t, tt.targetPath)
			}
		})
	}
}

func Test_Prepare(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	tests := map[string]struct {
		abs         *abstracts.Executor
		assertError func(*testing.T, error)
	}{
		"error encountered creating dirs": {
			abs: &abstracts.Executor{
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().General().Return(configure.General{}).AnyTimes()
					return m
				}(),
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{},
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "no base directory defined, verify runner configuration")
			},
		},
		"valid directory structure created": {
			abs: &abstracts.Executor{
				Cfg: func() *mock_configure.MockConfigurer {
					m := mock_configure.NewMockConfigurer(ctrl)
					m.EXPECT().General().Return(configure.General{}).AnyTimes()
					return m
				}(),
				Env: envparser.ExecutorEnv{
					StatefulEnv: envparser.StatefulEnv{
						BaseDir:   t.TempDir() + "/user",
						BuildsDir: t.TempDir() + "/user/builds",
						CacheDir:  t.TempDir() + "/user/cache",
						ScriptDir: t.TempDir() + "/user/script",
					},
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := Prepare(tt.abs)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_Run(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	dir, _ := os.MkdirTemp(t.TempDir(), "scriptDir")

	msg := logging.NewMessenger()

	tests := map[string]executorsTests{
		"failed to relocate job script": {
			scriptPath: "/file/doesnt/exist.bash",
			stage:      "prepare_script",
			cfg: func() *mock_configure.MockConfigurer {
				m := mock_configure.NewMockConfigurer(ctrl)
				m.EXPECT().General().Return(configure.General{}).AnyTimes()
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.EqualError(
					t,
					err,
					"unable to relocateJobScript (/file/doesnt/exist.bash): unable to augment CI JobScript: open /file/doesnt/exist.bash: no such file or directory",
				)
			},
		},
		"prepare_script job successful": {
			scriptPath: "../../test/testdata/jobscript.bash",
			stage:      "prepare_script",
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ScriptDir: dir,
				},
			},
			auth: func() *mock_authuser.MockAuthorized {
				m := mock_authuser.NewMockAuthorized(ctrl)
				m.EXPECT().PrepareNotification().Return("").Times(1)
				return m
			}(),
			cfg: func() *mock_configure.MockConfigurer {
				m := mock_configure.NewMockConfigurer(ctrl)
				m.EXPECT().General().Return(configure.General{}).AnyTimes()
				m.EXPECT().PrepareNotification(gomock.Eq(dir)).Times(1)
				return m
			}(),
			exec: func() *mock_executors.MockExecutor {
				m := mock_executors.NewMockExecutor(ctrl)
				m.EXPECT().Run().Return(nil).Times(1)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"get_sources job successful": {
			scriptPath: "../../test/testdata/job-scripts/get_sources_default",
			stage:      "get_sources",
			env: envparser.ExecutorEnv{
				RequiredEnv: envparser.RequiredEnv{
					JobToken: "T0k3n",
				},
				StatefulEnv: envparser.StatefulEnv{
					BuildsDir: dir,
					ScriptDir: dir,
				},
			},
			cfg: func() *mock_configure.MockConfigurer {
				m := mock_configure.NewMockConfigurer(ctrl)
				m.EXPECT().General().Return(configure.General{}).AnyTimes()
				return m
			}(),
			exec: func() *mock_executors.MockExecutor {
				m := mock_executors.NewMockExecutor(ctrl)
				m.EXPECT().Run().Return(nil).Times(1)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"get_sources failed augmentation": {
			scriptPath: "../../test/testdata/jobscript.bash",
			stage:      "get_sources",
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					BuildsDir: dir,
					ScriptDir: dir,
				},
			},
			cfg: func() *mock_configure.MockConfigurer {
				m := mock_configure.NewMockConfigurer(ctrl)
				m.EXPECT().General().Return(configure.General{}).AnyTimes()
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "unable to CreateGitAskpass: missing job token for GIT_ASKPASS")
			},
		},
		"step_script job failed": {
			scriptPath: "../../test/testdata/jobscript.bash",
			stage:      "step_script",
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ScriptDir: dir,
				},
				RequiredEnv: envparser.RequiredEnv{
					JobToken: "T0k3n",
				},
			},
			cfg: func() *mock_configure.MockConfigurer {
				m := mock_configure.NewMockConfigurer(ctrl)
				m.EXPECT().General().Return(configure.General{}).AnyTimes()
				return m
			}(),
			exec: func() *mock_executors.MockExecutor {
				m := mock_executors.NewMockExecutor(ctrl)
				m.EXPECT().Run().Return(nil).Times(1)
				return m
			}(),
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			a := &abstracts.Executor{
				Cfg:         tt.cfg,
				Env:         tt.env,
				Msg:         msg,
				Runner:      tt.runner,
				ExecOptions: tt.execOptions,
				ScriptPath:  tt.scriptPath,
				Stage:       tt.stage,
			}

			err := Run(a, tt.exec, tt.auth)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_Cleanup(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	dir, _ := os.MkdirTemp(t.TempDir(), "scriptDir")

	tests := map[string]executorsTests{
		"standard cleanup script directory workflow": {
			env: envparser.ExecutorEnv{
				StatefulEnv: envparser.StatefulEnv{
					ScriptDir: dir,
				},
			},
			msg: func() *mock_logging.MockMessenger {
				m := mock_logging.NewMockMessenger(ctrl)
				return m
			}(),
			cfg: func() *mock_configure.MockConfigurer {
				m := mock_configure.NewMockConfigurer(ctrl)
				m.EXPECT().General().Return(configure.General{}).AnyTimes()
				return m
			}(),
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			a := &abstracts.Executor{
				Cfg:         tt.cfg,
				Env:         tt.env,
				Msg:         tt.msg,
				Runner:      tt.runner,
				ExecOptions: tt.execOptions,
				ScriptPath:  tt.scriptPath,
				Stage:       tt.stage,
			}

			_ = os.MkdirAll(tt.env.StatefulEnv.ScriptDir, 0700)

			Cleanup(a)
		})
	}
}
