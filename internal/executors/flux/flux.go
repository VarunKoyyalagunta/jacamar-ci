package flux

import (
	"fmt"
	"sync"
	"time"

	"gitlab.com/ecp-ci/jacamar-ci/internal/augmenter"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/batch"
)

type executor struct {
	absExec *abstracts.Executor

	// batch submission only variables
	mng     batch.Manager
	jobName string
}

func (e *executor) Run() error {
	if !(e.absExec.Stage == "step_script" || e.absExec.Stage == "build_script") {
		return e.absExec.Runner.JobScriptOutput(e.absExec.ScriptPath)
	}

	// Verify that job scripts start with a script declaration line.
	step := augmenter.OpenStepScript(e.absExec.ScriptPath)
	step.LoginShell()
	if err := step.VerifyAndWrite(); err != nil {
		return fmt.Errorf("failed to updated Flux job script: %w", err)
	}

	return e.runFlux()
}

func (e *executor) runFlux() error {
	var wg sync.WaitGroup
	wg.Add(2)

	cmdErr := make(chan error, 1)
	defer close(cmdErr)
	go func() {
		defer wg.Done()

		// Build flux mini alloc submission command.
		fluxStdin := e.mng.BatchCmd(fmt.Sprintf(
			"--job-name=%s %s",
			e.jobName,
			e.mng.UserArgs(),
		))
		e.absExec.Msg.Notify("Flux job command: %s %s", fluxStdin, e.absExec.ScriptPath)

		err := e.absExec.Runner.JobScriptOutput(e.absExec.ScriptPath, fluxStdin)
		cmdErr <- err
	}()

	jobDone := make(chan struct{}, 1)
	defer close(jobDone)
	go func() {
		defer wg.Done()
		stopArg := fmt.Sprintf("$(flux jobs --format {id} -n --name %s)", e.jobName)
		e.mng.MonitorTermination(e.absExec.Runner, jobDone, stopArg, e.absExec.Env.StatefulEnv.ScriptDir)
	}()

	err := <-cmdErr
	jobDone <- struct{}{}

	wg.Wait()

	// Though a delay will not directly affect output it can still restrict artifacts and
	// should be observed.
	e.mng.NFSTimeout((e.absExec.Cfg.Batch()).NFSTimeout, e.absExec.Msg)

	return err
}

// NewExecutor generates a valid flux executor that fulfills the executors.Executor interface.
func NewExecutor(ae *abstracts.Executor) (*executor, error) {
	var err error
	e := &executor{
		absExec: ae,
	}

	if e.absExec.Stage == "step_script" || ae.Stage == "build_script" {
		set := batch.Settings{
			BatchCmd: "flux mini alloc",
			StopCmd:  "flux job cancel",
			IllegalArgs: []string{
				"--job-name",
				"--output",
				"--error",
			},
		}

		e.jobName = fmt.Sprintf(
			"ci-%s_%d",
			ae.Env.RequiredEnv.JobID,
			time.Now().Unix(),
		)

		e.mng, err = batch.NewBatchJob(set, ae.Cfg.Batch(), ae.Msg)
		if err != nil {
			return nil, err
		}
	}

	return e, nil
}
