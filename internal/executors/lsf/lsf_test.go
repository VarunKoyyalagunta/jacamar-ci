package lsf

import (
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ecp-ci/jacamar-ci/internal/envkeys"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/batch"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_batch"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_configure"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_logging"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

func Test_executor_Run(t *testing.T) {
	script := "/some/job/script.bash"
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	run := mock_runmechanisms.NewMockRunner(ctrl)
	run.EXPECT().JobScriptOutput(script).Return(nil)
	run.EXPECT().JobScriptOutput(script + ".error").Return(errors.New("error message"))
	run.EXPECT().JobScriptOutput(script, "bsub -I -N1")
	run.EXPECT().JobScriptOutput(script+".error", "bsub -I -N1").Return(errors.New("error message"))

	mng := mock_batch.NewMockManager(ctrl)
	mng.EXPECT().NFSTimeout(gomock.Eq("30s"), gomock.Any()).Times(2)
	mng.EXPECT().BatchCmd("-I -N1").Return("bsub -I -N1").Times(2)
	mng.EXPECT().UserArgs().Return("-N1").Times(2)

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().Batch().Return(configure.Batch{
		NFSTimeout: "30s",
	}).AnyTimes()

	msg := mock_logging.NewMockMessenger(ctrl)
	msg.EXPECT().Notify("LSF job command: %s %s", "bsub -I -N1", gomock.Any()).AnyTimes()

	type fields struct {
		absExec *abstracts.Executor
		mng     batch.Manager
	}
	tests := map[string]struct {
		fields      fields
		assertError func(*testing.T, error)
	}{
		"non-build job script execute successfully": {
			fields: fields{
				absExec: &abstracts.Executor{
					Runner:     run,
					ScriptPath: script,
					Stage:      "prepare_script",
				},
				mng: mng,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"non-build job script execute with error": {
			fields: fields{
				absExec: &abstracts.Executor{
					Runner:     run,
					ScriptPath: script + ".error",
					Stage:      "after_script",
				},
				mng: mng,
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "error message")
			},
		},
		"step_script executed successfully": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg:        cfg,
					Runner:     run,
					Msg:        msg,
					ScriptPath: script,
					Stage:      "step_script",
				},
				mng: mng,
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"build_script executed with error": {
			fields: fields{
				absExec: &abstracts.Executor{
					Cfg:        cfg,
					Runner:     run,
					Msg:        msg,
					ScriptPath: script + ".error",
					Stage:      "build_script",
				},
				mng: mng,
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "error message")
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			e := &executor{
				absExec: tt.fields.absExec,
				mng:     tt.fields.mng,
			}
			err := e.Run()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func TestNewExecutor(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	msg := mock_logging.NewMockMessenger(ctrl)
	msg.EXPECT().Warn(
		gomock.Eq("Illegal argument detected. Please remove: -e"),
	).Times(1)

	cfg := mock_configure.NewMockConfigurer(ctrl)
	cfg.EXPECT().Batch().Return(configure.Batch{
		NFSTimeout:        "30s",
		ArgumentsVariable: []string{"TEST_BSUB_PARAMETERS"},
	}).AnyTimes()

	type args struct {
		ae *abstracts.Executor
	}
	tests := map[string]struct {
		args           args
		prepare        func(*testing.T)
		assertError    func(*testing.T, error)
		assertExecutor func(*testing.T, *executor)
	}{
		"new executor for step_script": {
			args: args{
				ae: &abstracts.Executor{
					Cfg:   cfg,
					Msg:   msg,
					Stage: "step_script",
				},
			},
			prepare: func(t *testing.T) {
				t.Setenv(envkeys.UserEnvPrefix+"TEST_BSUB_PARAMETERS", "-A account123")
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"illegal argument results in error": {
			args: args{
				ae: &abstracts.Executor{
					Cfg:   cfg,
					Msg:   msg,
					Stage: "step_script",
				},
			},
			prepare: func(t *testing.T) {
				t.Setenv(envkeys.UserEnvPrefix+"TEST_BSUB_PARAMETERS", "-e test.txt")
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "illegal arguments must be addressed before continuing")
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.Nil(t, e)
			},
		},
		"new executor for after_script": {
			args: args{
				ae: &abstracts.Executor{
					Stage: "after_script",
				},
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
			assertExecutor: func(t *testing.T, e *executor) {
				assert.NotNil(t, e)
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			if tt.prepare != nil {
				tt.prepare(t)
			}

			got, err := NewExecutor(tt.args.ae)

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
			if tt.assertExecutor != nil {
				tt.assertExecutor(t, got)
			}
		})
	}
}
