package lsf

import (
	"fmt"

	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/batch"
)

type executor struct {
	absExec *abstracts.Executor

	// batch submission only variables
	mng batch.Manager
}

func (e *executor) Run() error {
	if !(e.absExec.Stage == "step_script" || e.absExec.Stage == "build_script") {
		return e.absExec.Runner.JobScriptOutput(e.absExec.ScriptPath)
	}

	bsubStdin := e.mng.BatchCmd(fmt.Sprintf("-I %s", e.mng.UserArgs()))
	e.absExec.Msg.Notify("LSF job command: %s %s", bsubStdin, e.absExec.ScriptPath)

	err := e.absExec.Runner.JobScriptOutput(e.absExec.ScriptPath, bsubStdin)

	// Wait for configured NFS timeout window (regardless of error).
	e.mng.NFSTimeout((e.absExec.Cfg.Batch()).NFSTimeout, e.absExec.Msg)

	return err
}

// NewExecutor generates a valid LSF executor that fulfills the executors.Executor interface.
func NewExecutor(ae *abstracts.Executor) (*executor, error) {
	var err error
	e := &executor{
		absExec: ae,
	}

	if e.absExec.Stage == "step_script" || ae.Stage == "build_script" {
		set := batch.Settings{
			BatchCmd:    "bsub",
			IllegalArgs: []string{"-e", "-o", "-eo", "-I"},
		}

		e.mng, err = batch.NewBatchJob(set, ae.Cfg.Batch(), ae.Msg)
		if err != nil {
			return nil, err
		}
	}

	return e, nil
}
