package pbs

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"
)

type qstatJob struct {
	JobName    string `json:"Job_Name"`
	JobState   string `json:"job_state"`
	ExitStatus int    `json:"Exit_status"`
}

type qstatJSON struct {
	PBSServer string              `json:"pbs_server"`
	Jobs      map[string]qstatJob `json:"Jobs"`
}

// qsubJobID parses the output from a successful qsub command in order to identify the jobID.
func qsubJobID(out string) string {
	out = strings.TrimSpace(out)
	lines := strings.Split(out, "\n")

	return lines[0]
}

// completed polls qstat until job in no longer obtainable without historical
// job information arguments.
func (e *executor) completed() {
	qstat := fmt.Sprintf("%s %s", e.mng.StateCmd(), e.jobID)
	for {
		_, err := e.absExec.Runner.ReturnOutput(qstat)
		if err != nil {
			return
		}
		time.Sleep(e.sleepTime)
	}
}

// exitStatus checks historic qstat results, returns an error if job failure detected
// or unable to successfully retrieve.
func (e *executor) exitStatus() error {
	qstat := fmt.Sprintf("%s -x -f -F json %s", e.mng.StateCmd(), e.jobID)
	out, err := e.absExec.Runner.ReturnOutput(qstat)
	if err != nil {
		return fmt.Errorf("failed qstat: %s", out)
	}

	res := qstatJSON{}
	// Empty results will still result in job failure.
	err = json.Unmarshal([]byte(out), &res)
	if err != nil || len(res.Jobs) == 0 {
		return fmt.Errorf("no supported qstat output found (%s)", qstat)
	}

	for k, v := range res.Jobs {
		if v.ExitStatus != 0 {
			return fmt.Errorf("non-zero exit status encountered in %s", k)
		}
	}

	return nil
}
