package pbs

import (
	"errors"
	"testing"
	"time"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/abstracts"

	"gitlab.com/ecp-ci/jacamar-ci/internal/runmechanisms"
	"gitlab.com/ecp-ci/jacamar-ci/pkg/batch"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_batch"
	"gitlab.com/ecp-ci/jacamar-ci/test/mocks/mock_runmechanisms"
)

const (
	qstatExitZero = `
{
    "timestamp":1651017427,
    "pbs_version":"20.0.1",
    "pbs_server":"hostname",
    "Jobs":{
        "0.hostname":{
            "Job_Name":"ci-123_1651017322",
            "Job_Owner":"pbsadmin@hostname",
            "job_state":"F",
            "Exit_status":0
        }
    }
}`
	qstatExitOne = `
{
    "timestamp":1651017427,
    "pbs_version":"20.0.1",
    "pbs_server":"hostname",
    "Jobs":{
        "1.hostname":{
            "Job_Name":"ci-123_1651017322",
            "Job_Owner":"pbsadmin@hostname",
            "job_state":"F",
            "Exit_status":0
        },
        "2.hostname":{
            "Job_Name":"ci-123_1651017322",
            "Job_Owner":"pbsadmin@hostname",
            "job_state":"F",
            "Exit_status":1
        }
    }
}`
)

func Test_qsubJobID(t *testing.T) {
	tests := map[string]struct {
		out  string
		want string
	}{
		"name only": {
			out:  "2.hostname",
			want: "2.hostname",
		},
		"multi-line": {
			out:  "3000.hostname\nadditional\noutput",
			want: "3000.hostname",
		},
		"empty output": {
			out:  "",
			want: "",
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			assert.Equalf(t, tt.want, qsubJobID(tt.out), "qsubJobID(%v)", tt.out)
		})
	}
}

func Test_executor_exitStatus(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mng := mock_batch.NewMockManager(ctrl)
	mng.EXPECT().StateCmd().Return("qstat").AnyTimes()

	tests := map[string]pbsTests{
		"qstat command failure": {
			e: &executor{
				absExec: &abstracts.Executor{
					Runner: func() runmechanisms.Runner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().ReturnOutput(gomock.Any()).Return("no job?", errors.New("error message"))
						return m
					}(),
				},
				mng:   mng,
				jobID: "1000.hostname",
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "failed qstat: no job?")
			},
		},
		"job exit_status 0": {
			e: &executor{
				absExec: &abstracts.Executor{
					Runner: func() runmechanisms.Runner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().ReturnOutput(gomock.Any()).Return(qstatExitZero, nil)
						return m
					}(),
				},
				mng:   mng,
				jobID: "0.hostname",
			},
			assertError: func(t *testing.T, err error) {
				assert.NoError(t, err)
			},
		},
		"job exit_status 1": {
			e: &executor{
				absExec: &abstracts.Executor{
					Runner: func() runmechanisms.Runner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().ReturnOutput(gomock.Any()).Return(qstatExitOne, nil)
						return m
					}(),
				},
				mng:   mng,
				jobID: "2.hostname",
			},
			assertError: func(t *testing.T, err error) {
				assert.EqualError(t, err, "non-zero exit status encountered in 2.hostname")
			},
		},
		"no valid qstat output": {
			e: &executor{
				absExec: &abstracts.Executor{
					Runner: func() runmechanisms.Runner {
						m := mock_runmechanisms.NewMockRunner(ctrl)
						m.EXPECT().ReturnOutput(gomock.Any()).Return("", nil)
						return m
					}(),
				},
				mng:   mng,
				jobID: "2000.hostname",
			},
			assertError: func(t *testing.T, err error) {
				assert.Error(t, err)
				if err != nil {
					assert.Contains(t, err.Error(), "no supported qstat output found")
				}
			},
		},
	}
	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			err := tt.e.exitStatus()

			if tt.assertError != nil {
				tt.assertError(t, err)
			}
		})
	}
}

func Test_executor_completed(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	t.Run("functional workflow", func(t *testing.T) {
		e := &executor{
			absExec: &abstracts.Executor{
				Runner: func() runmechanisms.Runner {
					m := mock_runmechanisms.NewMockRunner(ctrl)
					m.EXPECT().ReturnOutput(gomock.Any()).Return("", errors.New("no job found")).After(
						m.EXPECT().ReturnOutput(gomock.Any()).Return("", nil),
					)
					return m
				}(),
			},
			mng: func() batch.Manager {
				m := mock_batch.NewMockManager(ctrl)
				m.EXPECT().StateCmd().Return("qstat").AnyTimes()
				return m
			}(),
			sleepTime: 5 * time.Second,
			jobID:     "2000.hostname",
		}

		e.completed()
	})
}
