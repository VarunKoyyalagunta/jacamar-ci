// Package envkeys maintains constants for all potentially required environment
// variable keys.
package envkeys

const (
	// UserEnvPrefix is the expected runner defined prefix for all environment variables
	// provided to the custom executor driver.
	UserEnvPrefix = "CUSTOM_ENV_"
	// StatefulEnvPrefix is the required application defined prefix for stateful (e.g. job)
	// environment variables.
	StatefulEnvPrefix = "JACAMAR_CI_"
	// ScriptContentsPrefix is the prefix for all environment variables set with encoded
	// script contents.
	ScriptContentsPrefix = "JACAMAR_SCRIPT_CONTENTS_"
	// SysExitCode is the key for the preferred system failure exit status.
	SysExitCode = "SYSTEM_FAILURE_EXIT_CODE"
	// BuildExitCode is the key for the preferred build failure exit status.
	BuildExitCode = "BUILD_FAILURE_EXIT_CODE"
	// JobResponse location for the CI job response file with full payload contents. This
	// is only accessible by the auth user.
	JobResponse = "JOB_RESPONSE_FILE"
	// CustomBuildsDir key for user proposed build directory location.
	CustomBuildsDir = "CUSTOM_CI_BUILDS_DIR"
	// CopySchedulerLogs key for user proposed location to copy batch scheduling related logs.
	CopySchedulerLogs = "COPY_SCHEDULER_LOGS"
	// RunnerTimeout key for stateful runner timeout, string duration.
	RunnerTimeout = StatefulEnvPrefix + "RUNNER_TIMEOUT"
	// JacamarShell key to indicate to user profile Jacamar CI is in use.
	JacamarShell = "JACAMAR_CI_SHELL"

	CIJobJWT    = "CI_JOB_JWT"
	CIJobJWTV2  = "CI_JOB_JWT_V2"
	CIRunnerVer = "CI_RUNNER_VERSION"
	GitTrace    = "GIT_TRACE"
)
