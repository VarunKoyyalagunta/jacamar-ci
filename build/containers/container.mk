GO_VERSION ?= 1.19.3
GO_ARCH ?= amd64
GO_SHA ?= 74b9640724fd4e6bb0ed2a1bc44ae813a03f1e72a4c76253e2d5c015494430ba
GO_URL ?= https://golang.org/dl/go${GO_VERSION}.linux-${GO_ARCH}.tar.gz

GOLANGCI_VERSION ?= 1.50.1
GOSEC_VERSION ?= 2.14.0

PAV_TAG ?= 60d55280bed2fb060626dea284068fca791fbed4
SLURM_VERSION ?= 21.08.6
PBS_VERSION ?= 20.0.1
FLUX_VERSION ?= 0.45.0

CI_REGISTRY=registry.gitlab.com/ecp-ci/jacamar-ci
export BUILD_IMG=${CI_REGISTRY}/centos7-builder:${GO_VERSION}
export PAVILION_IMG=${CI_REGISTRY}/pav-tester:${GO_VERSION}
export SLURM_IMG=${CI_REGISTRY}/slurm-tester:${SLURM_VERSION}
export PBS_IMG=${CI_REGISTRY}/pbs-tester:${PBS_VERSION}
export GO_IMG=${CI_REGISTRY}/go-tester:${GO_VERSION}
export SUSE_IMG=${CI_REGISTRY}/opensuse-builder:${GO_VERSION}
export FLUX_IMG=${CI_REGISTRY}/flux-tester:${FLUX_VERSION}

ci_dir=/builds/ecp-ci/jacamar-ci

#
# rebuild/release images using Go
.PHONY: build-go-images
build-go-images:
	$(MAKE) build-centos7-builder
	$(MAKE) build-fedora-builder
	$(MAKE) build-opensuse-builder
	$(MAKE) build-go-tester
	$(MAKE) build-pav-tester
	$(MAKE) build-slurm-tester
	$(MAKE) build-flux-tester

.PHONY: push-go-images
push-go-images:
	$(MAKE) push-centos7-builder
	$(MAKE) push-fedora-builder
	$(MAKE) push-opensuse-builder
	$(MAKE) push-go-tester
	$(MAKE) push-pav-tester
	$(MAKE) push-slurm-tester
	$(MAKE) push-flux-tester

.PHONY: latest-go-images
latest-go-images:
	$(MAKE) latest-centos7-builder
	$(MAKE) latest-fedora-builder
	$(MAKE) latest-opensuse-builder
	$(MAKE) latest-go-tester
	$(MAKE) latest-pav-tester
	$(MAKE) latest-slurm-tester
	$(MAKE) latest-flux-tester

#
# build/containers/centos7-builder
.PHONY: build-centos7-builder
build-centos7-builder:
	${CONTAINER_RUNTIME} build \
		--build-arg GO_VERSION=${GO_VERSION} \
		--build-arg GO_ARCH=${GO_ARCH} \
		--build-arg GO_SHA=${GO_SHA} \
		--build-arg GO_URL=${GO_URL} \
		-t ${CI_REGISTRY}/centos7-builder:${GO_VERSION} \
		build/containers/centos7-builder

.PHONY: push-centos7-builder
push-centos7-builder:
	${CONTAINER_RUNTIME} push ${CI_REGISTRY}/centos7-builder:${GO_VERSION}

.PHONY: latest-centos7-builder
latest-centos7-builder:
	${CONTAINER_RUNTIME} tag \
		${CI_REGISTRY}/centos7-builder:${GO_VERSION} \
		${CI_REGISTRY}/centos7-builder:latest
	${CONTAINER_RUNTIME} push ${CI_REGISTRY}/centos7-builder:latest

.PHONY: run-centos7-builder
run-centos7-builder:
	${CONTAINER_RUNTIME} run \
		--rm -v ${ROOT_DIR}:${ci_dir} \
		-e "CI_PROJECT_DIR=${ci_dir}" \
		-w ${ci_dir}  \
		-it ${CI_REGISTRY}/centos7-builder:${GO_VERSION} /bin/bash


#
# build/containers/fedora-builder
.PHONY: build-fedora-builder
build-fedora-builder:
	${CONTAINER_RUNTIME} build \
		--build-arg GO_VERSION=${GO_VERSION} \
		--build-arg GO_ARCH=${GO_ARCH} \
		--build-arg GO_SHA=${GO_SHA} \
		--build-arg GO_URL=${GO_URL} \
		-t ${CI_REGISTRY}/fedora-builder:${GO_VERSION} \
		build/containers/fedora-builder

.PHONY: push-fedora-builder
push-fedora-builder:
	${CONTAINER_RUNTIME} push ${CI_REGISTRY}/fedora-builder:${GO_VERSION}

.PHONY: latest-fedora-builder
latest-fedora-builder:
	${CONTAINER_RUNTIME} tag \
		${CI_REGISTRY}/fedora-builder:${GO_VERSION} \
		${CI_REGISTRY}/fedora-builder:latest
	${CONTAINER_RUNTIME} push ${CI_REGISTRY}/fedora-builder:latest

.PHONY: run-fedora-builder
run-fedora-builder:
	${CONTAINER_RUNTIME} run \
		--rm -v ${ROOT_DIR}:${ci_dir} \
		-e "CI_PROJECT_DIR=${ci_dir}" \
		-w ${ci_dir}  \
		-it ${CI_REGISTRY}/fedora-builder:${GO_VERSION} /bin/bash


#
# build/containers/go-tester
.PHONY: build-go-tester
build-go-tester:
	${CONTAINER_RUNTIME} build \
		--build-arg GO_TAG=${GO_VERSION} \
		--build-arg GOSEC_VERSION=${GOSEC_VERSION} \
		--build-arg GOLANGCI_VERSION=${GOLANGCI_VERSION} \
		-t ${CI_REGISTRY}/go-tester:${GO_VERSION} \
		build/containers/go-tester

.PHONY: push-go-tester
push-go-tester:
	${CONTAINER_RUNTIME} push ${CI_REGISTRY}/go-tester:${GO_VERSION}

.PHONY: latest-go-tester
latest-go-tester:
	${CONTAINER_RUNTIME} tag \
		${CI_REGISTRY}/go-tester:${GO_VERSION} \
		${CI_REGISTRY}/go-tester:latest
	${CONTAINER_RUNTIME} push ${CI_REGISTRY}/go-tester:latest

.PHONY: run-go-tester
run-go-tester:
	${CONTAINER_RUNTIME} run \
		--rm -v ${ROOT_DIR}:${ci_dir} \
		-e "CI_PROJECT_DIR=${ci_dir}" \
		-w ${ci_dir}  \
		-it ${CI_REGISTRY}/go-tester:${GO_VERSION} /bin/bash


#
# build/containers/opensuse-builder
.PHONY: build-opensuse-builder
build-opensuse-builder:
	${CONTAINER_RUNTIME} build \
		--build-arg GO_VERSION=${GO_VERSION} \
		--build-arg GO_ARCH=${GO_ARCH} \
		--build-arg GO_SHA=${GO_SHA} \
		--build-arg GO_URL=${GO_URL} \
		-t ${CI_REGISTRY}/opensuse-builder:${GO_VERSION} \
		build/containers/opensuse-builder

.PHONY: push-opensuse-builder
push-opensuse-builder:
	${CONTAINER_RUNTIME} push ${CI_REGISTRY}/opensuse-builder:${GO_VERSION}

.PHONY: latest-opensuse-builder
latest-opensuse-builder:
	${CONTAINER_RUNTIME} tag \
		${CI_REGISTRY}/opensuse-builder:${GO_VERSION} \
		${CI_REGISTRY}/opensuse-builder:latest
	${CONTAINER_RUNTIME} push ${CI_REGISTRY}/opensuse-builder:latest

.PHONY: run-opensuse-builder
run-opensuse-builder:
	${CONTAINER_RUNTIME} run \
		--rm -v ${ROOT_DIR}:${ci_dir} \
		-e "CI_PROJECT_DIR=${ci_dir}" \
		-w ${ci_dir}  \
		-it ${CI_REGISTRY}/opensuse-builder:${GO_VERSION} /bin/bash


#
# build/containers/pbs-tester
.PHONY: build-pbs-tester
build-pbs-tester:
	${CONTAINER_RUNTIME} build \
		--build-arg PBS_VER=${PBS_VERSION} \
		--build-arg PAV_TAG=${PAV_TAG} \
		--build-arg GO_VERSION=${GO_VERSION} \
		--build-arg GO_ARCH=${GO_ARCH} \
		--build-arg GO_SHA=${GO_SHA} \
		--build-arg GO_URL=${GO_URL} \
		-t ${PBS_IMG} \
		build/containers/pbs-tester

.PHONY: push-pbs-tester
push-pbs-tester:
	${CONTAINER_RUNTIME} push ${PBS_IMG}

.PHONY: latest-pbs-tester
latest-pbs-tester:
	${CONTAINER_RUNTIME} tag \
		${CI_REGISTRY}/pbs-tester:${PBS_VERSION} \
		${CI_REGISTRY}/pbs-tester:latest
	${CONTAINER_RUNTIME} push ${CI_REGISTRY}/pbs-tester:latest

.PHONY: run-pbs-tester
run-pbs-tester:
	${CONTAINER_RUNTIME} run -h buildkitsandbox \
		--rm -v ${ROOT_DIR}:${ci_dir} \
		--cap-add=SYS_PTRACE \
		--cap-add=CAP_SYS_RESOURCE \
		-e "CI_PROJECT_DIR=${ci_dir}" \
		-w ${ci_dir} \
		-it ${PBS_IMG}

#
# build/containers/pav-tester
.PHONY: build-pav-tester
build-pav-tester:
	${CONTAINER_RUNTIME} build \
		--build-arg GO_VERSION=${GO_VERSION} \
		--build-arg GO_ARCH=${GO_ARCH} \
		--build-arg GO_SHA=${GO_SHA} \
		--build-arg GO_URL=${GO_URL} \
		--build-arg PAV_TAG=${PAV_TAG} \
		-t ${CI_REGISTRY}/pav-tester:${GO_VERSION} \
		build/containers/pav-tester

.PHONY: push-pav-tester
push-pav-tester:
	${CONTAINER_RUNTIME} push ${CI_REGISTRY}/pav-tester:${GO_VERSION}

.PHONY: latest-pav-tester
latest-pav-tester:
	${CONTAINER_RUNTIME} tag \
		${CI_REGISTRY}/pav-tester:${GO_VERSION} \
		${CI_REGISTRY}/pav-tester:latest
	${CONTAINER_RUNTIME} push ${CI_REGISTRY}/pav-tester:latest

.PHONY: run-pav-tester
run-pav-tester:
	${CONTAINER_RUNTIME} run \
		--rm -v ${ROOT_DIR}:${ci_dir} \
		-e "CI_PROJECT_DIR=${ci_dir}" \
		-w ${ci_dir}  \
		-it ${CI_REGISTRY}/pav-tester:${GO_VERSION} /bin/bash

#
# build/containers/slurm-tester
.PHONY: build-slurm-tester
build-slurm-tester:
	${CONTAINER_RUNTIME} build \
		--build-arg PAV_TAG=${PAV_TAG} \
		--build-arg GO_VERSION=${GO_VERSION} \
		--build-arg GO_ARCH=${GO_ARCH} \
		--build-arg GO_SHA=${GO_SHA} \
		--build-arg GO_URL=${GO_URL} \
		--build-arg SLURM_VERSION=${SLURM_VERSION} \
		-t ${CI_REGISTRY}/slurm-tester:${SLURM_VERSION} \
		build/containers/slurm-tester

.PHONY: push-slurm-tester
push-slurm-tester:
	${CONTAINER_RUNTIME} push ${CI_REGISTRY}/slurm-tester:${SLURM_VERSION}

.PHONY: latest-slurm-tester
latest-slurm-tester:
	${CONTAINER_RUNTIME} tag \
		${CI_REGISTRY}/slurm-tester:${SLURM_VERSION} \
		${CI_REGISTRY}/slurm-tester:latest
	${CONTAINER_RUNTIME} push ${CI_REGISTRY}/slurm-tester:latest

.PHONY: run-slurm-tester
run-slurm-tester:
	${CONTAINER_RUNTIME} run -h slurmctl \
		--rm -v ${ROOT_DIR}:${ci_dir} \
		-e "CI_PROJECT_DIR=${ci_dir}" \
		--cap-add=SYS_ADMIN \
		-w ${ci_dir}  \
		-it ${CI_REGISTRY}/slurm-tester:${SLURM_VERSION}

#
# build/containers/flux-tester
.PHONY: build-flux-tester
build-flux-tester:
	${CONTAINER_RUNTIME} build \
		--build-arg PAV_TAG=${PAV_TAG} \
		--build-arg GO_VERSION=${GO_VERSION} \
		--build-arg GO_ARCH=${GO_ARCH} \
		--build-arg GO_SHA=${GO_SHA} \
		--build-arg GO_URL=${GO_URL} \
		--build-arg FLUX_VERSION=${FLUX_VERSION} \
		-t ${CI_REGISTRY}/flux-tester:${FLUX_VERSION} \
		build/containers/flux-tester

.PHONY: push-flux-tester
push-flux-tester:
	${CONTAINER_RUNTIME} push ${CI_REGISTRY}/flux-tester:${FLUX_VERSION}

.PHONY: latest-flux-tester
latest-flux-tester:
	${CONTAINER_RUNTIME} tag \
		${CI_REGISTRY}/flux-tester:${FLUX_VERSION} \
		${CI_REGISTRY}/flux-tester:latest
	${CONTAINER_RUNTIME} push ${CI_REGISTRY}/flux-tester:latest

.PHONY: run-flux-tester
run-flux-tester:
	${CONTAINER_RUNTIME} run \
		--rm -v ${ROOT_DIR}:${ci_dir} \
		-e "CI_PROJECT_DIR=${ci_dir}" \
		-w ${ci_dir}  \
		-it ${CI_REGISTRY}/flux-tester:${FLUX_VERSION}
