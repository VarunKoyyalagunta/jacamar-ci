.PHONY: all
all: #B Build and install all applications locally.
	$(MAKE) build
	$(MAKE) install

.PHONY: build
build: #B  Build all applications locally in project builds directory.
	$(MAKE) build-dir
	$(MAKE) build-jacamar
	$(MAKE) build-auth

.PHONY: build-jacamar
build-jacamar: #B Build jacamar application only.
	CGO_ENABLED=0 ${GOCMD} build \
		-trimpath \
		-ldflags "${LDFLAGS}" \
		-tags netgo,osusergo,getent \
		-o ${BUILDDIR}jacamar \
		cmd/jacamar/main.go

.PHONY: build-auth
build-auth: #B Build jacamar-auth application only.
	${GOCMD} build \
		-trimpath \
		-ldflags '${LDFLAGS}' \
		-tags netgo \
		-o ${BUILDDIR}jacamar-auth \
		cmd/jacamar-auth/main.go

.PHONY: build-dir
build-dir: #B Create target build directory if not present.
	mkdir -p ${BUILDDIR}

.PHONY: install
install: #B Install all applications locally at PREFIX.
	install -d $(INSTALL_PREFIX)/bin
	install -m 755 ${BUILDDIR}jacamar $(INSTALL_PREFIX)/bin
	install -m 755 ${BUILDDIR}jacamar-auth $(INSTALL_PREFIX)/bin

.PHONY: bom
bom: #B General software bill of materials using CycloneDX format.
	mkdir -p bom/
	cyclonedx-gomod mod -json -output "bom/jacamar-ci_${VERSION}_${GOOS}_${GOARCH}.bom.json" .
