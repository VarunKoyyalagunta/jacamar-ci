# Jacamar CI

[![Latest Release](https://gitlab.com/ecp-ci/jacamar-ci/-/badges/release.svg)](https://gitlab.com/ecp-ci/jacamar-ci/-/releases)
[![pipeline status](https://gitlab.com/ecp-ci/jacamar-ci/badges/develop/pipeline.svg)](https://gitlab.com/ecp-ci/jacamar-ci/-/commits/develop)
[![coverage report](https://gitlab.com/ecp-ci/jacamar-ci/badges/develop/coverage.svg)](https://gitlab.com/ecp-ci/jacamar-ci/-/commits/develop)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/ecp-ci/jacamar-ci)](https://goreportcard.com/report/gitlab.com/ecp-ci/jacamar-ci)

Jacamar CI is the HPC focused CI/CD driver using GitLab’s
[custom executor](https://docs.gitlab.com/runner/executors/custom.html) model.
The core goal of this project is to establish a maintainable, yet extensively configurable
tool that will allow for the use of [GitLab’s](https://gitlab.com) robust testing
on unique resources. Allowing code teams to integrate potentially existing
pipelines on powerful scientific development environments.

![Jacamar CI Logo](build/images/jacamar_ci_logo_md.png)

## Documentation

For complete documentation regarding all aspects of deployment and administration of
Jacamar CI please see [ecp-ci.gitlab.io](https://ecp-ci.gitlab.io/docs/admin.html#jacamar-ci).

## Quick Start

If new to Jacamar CI we recommend viewing the
[administrator tutorial](https://ecp-ci.gitlab.io/docs/admin/jacamar/tutorial.html)
before continuing. This hands-on tutorial will take ~15 minutes. It will
provide a basic introduction to deployment, configuration, and a simple
process for testing the majority of functionality within a
container/virtual-environment.

### Installation

See our [documentation](https://ecp-ci.gitlab.io/docs/admin/jacamar/deployment.html#packages)
on installing the release RPM. If you wish to build from source you will need:

* Bash
* Git versions 2.9+
* GitLab Runner 14.3+
* Go versions 1.18+ (see [official instructions](https://golang.org/doc/install))
* libc
* [libseccomp](https://github.com/seccomp/libseccomp) 2.3.1+
* Make

```Console
git clone https://gitlab.com/ecp-ci/jacamar-ci.git
cd jacamar-ci
make build
make install PREFIX=/usr/local
```

Though the custom executor has been officially supported through the GitLab
Runner for several major releases, due to required functionality Jacamar CI
necessitates the use of upstream runner version `14.3+`. Please see the
[official releases](https://packages.gitlab.com/runner/gitlab-runner) to
obtain your required version.

### Configuration

If you've administered a GitLab Runner before, the
[configuration](https://docs.gitlab.com/runner/configuration/advanced-configuration.html)
will be familiar. With Jacamar CI, it not only relies upon specific
management of the upstream runner but also its own configuration. Both
applications leverage [TOML](https://github.com/toml-lang/toml).

#### GitLab Runner

When using the [custom executor](https://docs.gitlab.com/runner/executors/custom.html)
there are additional one time configuration (e.g., `/etc/gitlab-runner/config.toml`)
requirements. This example targets the full path to our `jacamar-auth`
application as we wish to leverage the supported
[authorization and downscoping mechanisms](https://ecp-ci.gitlab.io/docs/admin/jacamar/auth.html).
Please note that the `*_args` are required as shown.

```TOML
[[runners]]
  name = "Custom Executor Example"
  url = "https://gitlab.example.com/"
  token = "T0k3n"
  executor = "custom"
  [runners.custom]
    config_exec = "/opt/jacamar/bin/jacamar-auth"
    config_args = ["config", "--configuration", "/etc/gitlab-runner/custom-config.toml"]

    prepare_exec = "/opt/jacamar/bin/jacamar-auth"
    prepare_args = ["prepare"]

    run_exec = "/opt/jacamar/bin/jacamar-auth"
    run_args = ["run"]

    cleanup_exec = "/opt/jacamar/bin/jacamar-auth"
    cleanup_args = ["cleanup", "--configuration", "/etc/gitlab-runner/custom-config.toml"]
```

#### Jacamar

You will notice a Jacamar CI specific configuration is required
(`--configuration /etc/gitlab-runner/custom-config.toml`). This is to
support the plethora of
[configuration options](https://ecp-ci.gitlab.io/docs/admin/jacamar/configuration.html#jacamar-ci-config):

```TOML
[general]
executor = "slurm"
data_dir = "/ecp"

[auth]
downscope = "setuid"
```

The above demonstrates the minimally required configuration in order
to run. We highly encourage you to explore our documentation for a
comprehensive look at options available and recommended practices.

# Attributions

* The logo was created by @admgc and is a remix of the
  [Rufous-tailed jacamar](https://en.wikipedia.org/wiki/Jacamar#/media/File:Rufous-tailed_jacamar_(Galbula_ruficauda)_male_2.JPG)
  by [Charles J. Sharp](https://www.wikidata.org/wiki/Q54800218)
  from [Sharp Photography](http://www.sharpphotography.co.uk/),
  licensed under [CC BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0).
